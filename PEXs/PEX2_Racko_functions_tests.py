#!/usr/bin/env python

"""
These functions are used to verify that the functions used to make a RACKO game are not erronous

Documentation Statement:
Compared my methods to C3C Van Epps, ended up using mine because I liked them better

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
import PEX2_Racko_functions
from PEX2_Racko_functions import create_deck, shuffle_deck, pass_out_cards, get_number_of_players,\
    remove_top_card,append_card_to_deck,player_hand_is_racko,player_hand_value
import random
# Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "Sep 22, 2016"


# ---------------------------------------------------------------------
def correct_cards_in_deck(deck):
    """
    Verify that a deck of cards contains the correct card values
    :param deck: [int] a list of integers
    :return: Boolean True if each card appears exactly once in the deck
    """
    # There must be one card of each number in the deck. Count the
    # number of times each card is found.
    counters = [0] * (len(deck)+1)
    for card_value in deck:
        counters[card_value] += 1

    # Now verify that each card was found only once
    valid_deck = True
    for card_value in range(1, len(deck)+1):
        if counters[card_value] == 0:
            print("Test failed: card {} is missing from the deck"
                  .format(card_value))
            valid_deck = False
        elif counters[card_value] > 1:
            print("Test failed: card {} is in the deck {} times".
                  format(card_value, counters[card_value]))
            valid_deck = False
    return valid_deck


# ---------------------------------------------------------------------
def verify_create_deck():
    """
    Verify the correctness of the create_deck() function
    """
    # Test for valid inputs
    deck = create_deck(2)

    if not isinstance(deck, list):
        print("Test failed: the return value of create_deck must be a list")

    if len(deck) != 40 or not correct_cards_in_deck(deck):
        print("Test failed: 2 player deck is incorrect.")
        print("The invalid deck is ", deck)

    deck = create_deck(3)
    if len(deck) != 50 or not correct_cards_in_deck(deck):
        print("Test failed: 3 player deck is incorrect.")
        print("The invalid deck is ", deck)

    deck = create_deck(4)
    if len(deck) != 60 or not correct_cards_in_deck(deck):
        print("Test failed: 4 player deck is incorrect.")
        print("The invalid deck is ", deck)

    # Test for invalid inputs
    for number_players in [0, 1, 5, 6, 7]:
        deck = create_deck(number_players)
        if deck is not None:
            print("Test failed: {} players did not return as an error"
                  .format(number_players))

def verify_shuffled_deck():
    """
    Verify the decks aren't the same
    """
    for i in range(2, 5):
        deck = create_deck(i)
        olddeck = deck[:]
        deck = shuffle_deck(deck)
        different = False
        for j in range(len(deck) - 1):
            if (olddeck[j] != deck[j]):
                different = True
        if (different == False):
            print("shuffle test has failed: decks are identical")
        if (len(deck) != len(olddeck)):
            print("shuffle test failed: length has changed")

def verify_cards_passedout():
    """
    Verify the hands have been passed out correctly
    """
    for i in range(2, 5):
        deck = create_deck(i)
        deck = shuffle_deck(deck)
        hands = pass_out_cards(deck,i)
        for j in range(i):
            if (len(hands[j])!=10):
                print("pass out test has failed, incorrect number of cards passed out")


def verify_remove_top_card():
    """
    Verify the top card is out
    """
    for i in range(2, 5):
        deck = create_deck(i)
        deck = shuffle_deck(deck)
        olddeck = deck[:]
        remove_top_card(deck)
        if (deck[0]==olddeck[0]):
            print("remove top card test has failed, top card is the same")
        if (len(deck) == len(olddeck)):
            print("remove top card test has failed, length is the same")

def verify_append_card_todeck():
    """
    Verify the top card has been added to bottom
    """
    for i in range(2, 5):
        deck = create_deck(i)
        deck = shuffle_deck(deck)
        olddeck = deck[:]
        append_card_to_deck(deck,random.randint(1,40))
        if (deck[len(deck)-1]==olddeck[len(olddeck)-1]):
            print("add card test has failed, last card is the same")
        if (len(deck) == len(olddeck)):
            print("add card card test has failed, length is the same")

def verify_playerhand_in_racko():
    """
    Verify the hand passed is in racko
    """
    for i in range(2, 5):
        deck = create_deck(i)
        if (player_hand_is_racko(deck)==False):
            print("racko test failed: default deck is NOT in racko at the start")
        deck = shuffle_deck(deck)
        if (player_hand_is_racko(deck)==True):
            print("racko test failed: deck is in racko after shuffle")
        hands = pass_out_cards(deck,i)
        for j in range(i):
            if (player_hand_is_racko(hands[j])):
                print("racko test failed: player hand is in racko by default")

def verify_playerhand_value():
    """
    Verifies the value of the current player's hand
    """
    for i in range(2, 5):
        deck = create_deck(i)
        if (player_hand_value(deck, False) != (200 + (i-2)*50)):
            print("value test failed: default deck is NOT correct respective value")
        if (player_hand_value(deck, True) != (225 + (i-2)*50)):
            print("value test failed: default deck is NOT correct respective value")
        deck = shuffle_deck(deck)
        hands = pass_out_cards(deck, i)
        for j in range(i):
            if (player_hand_value(hands[j], False)%5 !=0 ):
                print("value test failed: value is not multiple of 5")

# ---------------------------------------------------------------------
def main():
    """
    Execute the validation tests for the RACK-O functions
    """
    verify_create_deck()
    verify_playerhand_value()
    verify_playerhand_in_racko()
    verify_append_card_todeck()
    verify_remove_top_card()
    verify_create_deck()
    verify_shuffled_deck()
if __name__ == "__main__":
    main()
