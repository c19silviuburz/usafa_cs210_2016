#!/usr/bin/env python

"""
Play the game RACK-O.

Documentation: C3C Van Epps explained the scope of some of the functions to me because it was unclear what the document was asking for
"""
# =====================================================================

from PEX4_RACKO_interface import get_number_of_players, get_how_to_start_game, \
    get_draw_card_choice, get_discard_choice
from PEX4_RACKO_interface import QUIT_GAME, NEW_GAME, LOAD_PREVIOUS_GAME, SAVE_GAME, \
    DRAW_FROM_THE_DISCARD_PILE, DRAW_FROM_THE_DRAW_PILE
from PEX4_Cards import Cards
from PEX4_Player import Player
import pickle

# Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "17 Nov 16"

# Constants for the state of the game
CONTINUE_PLAYING = 0
GAME_OVER = 1


class RackoGame:

    # -----------------------------------------------------------------
    def __init__(self, number_of_players):
        """
        Initializes the variables needed to play a RACKO game
        :return: None
        """
        self.number_of_players = number_of_players
        self.list_of_players = []
        for i in range(self.number_of_players):
            name = str("Player ") + str(i)
            self.list_of_players.append(Player(name))
        self.deck_of_cards = Cards(1, (10*self.number_of_players)+20)
        self.dealer = 0
        self.active_player = 1
        self.draw_pile = Cards()
        self.discard_pile = Cards()
        self.status = CONTINUE_PLAYING
        self.pass_out_cards()

    # -----------------------------------------------------------------
    def __str__(self):
        """
        Loads a string with information about the current racko game and returns it
        :return: String
        """
        string = ""
        string += str("Number of players: ") + str(self.number_of_players)
        string += str("\nPlayers: ")
        for i in range(self.number_of_players):
            string += str(self.list_of_players[i])
            string += str(", ")
        string += str("\nDeck of Cards: ")
        string += str(self.deck_of_cards)
        string += str("\nDealer: ")
        string += str(self.dealer)
        string += str("\nActive player: ")
        string += str(self.active_player)
        string += str("\nDraw pile: ")
        string += str(self.draw_pile)
        string += str("\nDiscard pile: ")
        string += str(self.discard_pile)
        string += str("\nStatus: ")
        string += str(self.status)
        return string

    # -----------------------------------------------------------------
    def pass_out_cards(self):
        """
        Assigns the players their necessary cards to play the game
        :return: None
        """
        for i in range(self.number_of_players):
            hand = Cards()
            for j in range(10):
                hand.add_to_end(self.deck_of_cards.get_top_card())
                self.deck_of_cards.remove_top_card()
            self.list_of_players[i].set_hand(hand)
        self.draw_pile = self.deck_of_cards

        self.discard_pile.add_to_top(self.draw_pile.get_top_card())
        self.draw_pile.remove_top_card()

    # -----------------------------------------------------------------
    def save_game(self):
        """
        Saves the current game's information to be loaded later
        :return: None
        """
        filename = input("Save the RACK-O game to what file: ")#ask about this
        with open(filename, "wb") as outfile:
            pickle.dump(self, outfile)

    # -----------------------------------------------------------------
    def update_draw_pile(self):
        """
        If the draw pile is 0, the first card from the discard pile is taken and made as the draw pile
        :return: None
        """
        if len(self.draw_pile) == 0:
            for i in range(len(self.discard_pile)-1):
                self.draw_pile.add_to_end(self.discard_pile.remove_top_card())

    # -----------------------------------------------------------------
    def active_player_takes_their_turn(self):
        """
        Runs the logic that handles the players' input in the game
        :return: None
        """
        topcard = self.discard_pile.get_top_card()
        playerhand = self.list_of_players[self.active_player].get_hand()

        choice = get_draw_card_choice()


    # -----------------------------------------------------------------
    def update_player_scores(self):
        """
        Increments the players' scores appropriately at the end of the round
        :return: None
        """
        for i in range(self.number_of_players):
            self.list_of_players[i].increment_score(self.player_hand_value(i))

    # -----------------------------------------------------------------
    def active_players_hand_is_racko(self):
        """
        Determines if the active player's hand is RACKO
        :return: True is yes, false if no
        """
        oldvalue = -1
        for i in range(len(self.list_of_players[self.active_player].hand)):
            if (oldvalue < self.list_of_players[self.active_player].hand[i]):
                oldvalue = self.list_of_players[self.active_player].hand[i]
            else:
                return False
        return True

    def players_hand_is_racko(self, which_player):
        """
        Determines if the requested players' hand (which_player) is in racko
        :return: True if yes, false if no
        """
        oldvalue = -1
        for i in range(len(self.list_of_players[which_player].hand)):
            if (oldvalue < self.list_of_players[which_player].hand.get_card_at(i)):
                oldvalue = self.list_of_players[which_player].hand.get_card_at(i)
            else:
                return False
        return True

    # -----------------------------------------------------------------
    def player_hand_value(self, which_player):
        """
        Calculates and returns the requested player's hand (which_player)
        :return: player hand value
        """
        value = 0
        if (self.players_hand_is_racko(which_player)):
            value += 25
        oldvalue = -1
        for i in range(len(self.list_of_players[which_player].hand)):
            if (oldvalue < self.list_of_players[which_player].hand.get_card_at(i)):
                oldvalue = self.list_of_players[which_player].hand.get_card_at(i)
                value += 5
                break
        return value

    # -----------------------------------------------------------------
    def round_is_finished(self):
        """
        Updates and displays scores, prepares for next round. If someone has reached 500, game is over
        :return: None
        """
        self.update_player_scores()
        self.display_round_winner()
        self.reset_for_next_round()

        for i in self.list_of_players:
            if (500 >= self.list_of_players[i].get_score()):
                self.status = GAME_OVER
        self.display_final_results()
        self.display_final_results()


    # -----------------------------------------------------------------
    def reset_for_next_round(self):
        """
        Resets game states to prepare for next round. Increments dealer/activeplayers, resets all decks
        :return: None
        """
        self.update_player_scores()
        self.dealer +=1
        if self.dealer > self.number_of_players:
            self.dealer = 0
        self.status = CONTINUE_PLAYING
        self.active_player +=1
        if self.active_player > self.number_of_players:
            self.active_player = 0
        self.draw_pile = Cards()
        self.discard_pile = Cards()
        self.deck_of_cards = Cards(1, (10 * self.number_of_players) + 20)
        self.pass_out_cards()

    # -----------------------------------------------------------------
    def display_all_current_scores(self):
        """
        Prints all players' current scores
        :return: None
        """
        for i in range(len(self.list_of_players)):
            print("Player: ", self.list_of_players[i].name)
            print("\nScore: ", self.list_of_players[i].score)

    # -----------------------------------------------------------------
    def display_round_winner(self):
        """
        Determines and prints the current round's winner
        :return: None
        """
        highest = -1
        for i in range(len(self.list_of_players)):
            if (highest < self.list_of_players[i].score):
                highest = i
        print("Round winner: " , self.list_of_players[i].name)
        print("\nScore: " , self.list_of_players[i].score)


    # -----------------------------------------------------------------
    def display_final_results(self):
        """
        Prints the players' final scores
        :return: None
        """
        print("Game Over! Final scores:\n")
        for i in range(len(self.list_of_players)):
            print("Player: " , self.list_of_players[i].name)
            print("Score: ", self.list_of_players[i].score)
            print("\n")

    # -----------------------------------------------------------------
    def play(self):
        """  Play the game until the user quits or someone wins. """
        while self.status == CONTINUE_PLAYING:

            self.active_player_takes_their_turn()
            if self.status == CONTINUE_PLAYING:

                # Check for a winner
                if self.active_players_hand_is_racko():
                    self.round_is_finished()
                else:
                    # Switch active player
                    self.active_player = (self.active_player + 1) % self.number_of_players

        # The game is over; display the status of the game when it ended.
        self.display_final_results()
# ---------------------------------------------------------------------


    def load_saved_racko_game():
        filename = input("Please enter the name of the RACK-O game file: ")
        with open(filename, "rb") as infile:
            racko_game = pickle.load(infile)

        return racko_game
# =====================================================================


def main():
    """ Play RACK-O """
    start = get_how_to_start_game()

    if start == NEW_GAME:
        game = RackoGame()
        game.play()
    elif start == LOAD_PREVIOUS_GAME:
        game = load_saved_racko_game()
        game.play()
    else:  # start == QUIT_GAME
        exit()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()