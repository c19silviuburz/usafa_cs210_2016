#!/usr/bin/env python

"""
An ordered list of cards in a RACK-O game.

Documentation: C3C Van Epps explained the scope of some of the functions to me because it was unclear what the document was asking for
"""
# =====================================================================
import random

# Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "17 Nov 16"


class Cards:
    """ Cards represents an ordered list of cards """

    def __init__(self, first_card=0, last_card=0):
        """
        Creates blank list of cards and then initilizes them if necessary
        :return: None
        """
        self.cards_list = []
        if (first_card != 0 ) & (last_card != 0):
            self.initialize(first_card,last_card)
        self.shuffle()


    def initialize(self, first_card, last_card):
        """
        initilizes the cardslist with sequential numbers
        :return: None
        """
        last_card += 1
        for i in range(first_card, last_card):
            self.cards_list.append(i)

    def get_top_card(self):
        """
        Returns top card but doesnt remove it
        :return: top card in list
        """
        if (len(self.cards_list) != 0):
            return self.cards_list[0]
        else:
            return -1

    def get_card_at(self, position):
        """
        Returns card at specified position but doesnt remove it
        :return: specified card in list
        """
        if ((len(self.cards_list) > position) & (position >= 0)):
            return self.cards_list[position]
        else:
            return -1

    def add_to_end(self, new_card):
        """
        adds passed card to the end of list
        :return: None
        """
        self.cards_list.append(new_card)

    def add_to_top(self, new_card):
        """
        adds passed card to the start of list
        :return: None
        """
        self.cards_list.insert(0, new_card)

    def replace(self, position, new_card):
        """
        Replaces the specified position in the list with the specified card
        :return: None
        """
        if ((len(self.cards_list) > position) & (position >= 0)):
            self.cards_list[position] = new_card

    def remove_top_card(self):
        """
        Returns top card and removes it
        :return: top card
        """
        if (len(self.cards_list) != 0):
            card = self.cards_list[0]
            self.cards_list.pop(0)
            return card
        else:
            return -1

    def find(self, card):
        """
        Returns index of first occurance of specified card
        :return: index of card or -1 if not found
        """
        if card in self.cards_list:
            return self.cards_list.index(card)
        else:
            return -1

    def shuffle(self):
        """
        Shuffles the cards in the deck
        :return: None
        """
        newlist = []
        length = len(self.cards_list)
        for j in range(len(self.cards_list)):
            newlist.append(self.cards_list.pop(random.randint(0, length - 1 - j)))
        self.cards_list = newlist[:]

    def __str__(self):
        """
        Loads a string with the values of the cards in the deck and returns it
        :return: String
        """
        string = ""
        for i in range(len(self.cards_list)):
            string += str(self.cards_list[i])
            string += ", "
        return string

    def __contains__(self, card):
        """
        Checks if specific card is in the deck
        :return: True if found, false if no
        """
        if (card in self.cards_list):
            return True
        else:
            return False

    def __len__(self):
        """
        Returns length of deck (number of cards)
        :return: length of deck
        """
        return len(self.cards_list)

    def __iter__(self):
        """
        Called when iterating through the deck list
        :return: self
        """
        self.counter = 0
        return self

    def __next__(self):
        """
        interates to the next card in the list
        :return: next card in list, raises exception if not found
        """
        maximum_counter = len(self.cards_list)
        if self.counter < maximum_counter:
            result = self.cards_list[self.counter]  # the next value you want the loop to process
            self.counter += 1
            return result
        else:
            raise StopIteration