#!/usr/bin/env python

"""
These are the functions necessary to building a RACKO game.

Documentation Statement:
Compared my methods to C3C Van Epps, ended up using mine because I liked them better

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules

# Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "Sep 22, 2016"
# ---------------------------------------------------------------------

import random

def create_deck(number_players):
    """
    Create a deck of cards for a RACK-O game.
    :param number_players: the number of players in a game.
    :return: a list containing sequential numbers 1 to 40, 50, or 60. Returns
    None if the number_players is invalid.
    """
    if number_players == 2:
        number_cards = 40
    elif number_players == 3:
        number_cards = 50
    elif number_players == 4:
        number_cards = 60
    else:
        print("Error: {} is an invalid number of players".format(number_players))
        return None

    card_deck = []
    for card in range(1, number_cards+1):
        card_deck.append(card)

    return card_deck
# ---------------------------------------------------------------------


def get_number_of_players():
    """
    Ask the user how many players will be playing the game
    :return:  Returns the number of desired players, returns -1 if quit
    """
    input1 = 0
    while((input1 >4) or (input1 < 1)):
        input1 = int(input("Number of Players?: "))
        if (input1 == "quit"):
            return -1
    return input1
# ---------------------------------------------------------------------


def shuffle_deck(deck):
    """
    Shuffles the deck for the game
    :param deck: deck to be shuffled
    :return: returns a shuffled deck
    """
    newlist = []
    length = len(deck)
    for j in range(len(deck)):
        newlist.append(deck.pop(random.randint(0,length-1-j)))
    deck = newlist[:]
    return deck

# ---------------------------------------------------------------------


def pass_out_cards(deck, number_players): #where is the "new" operator in python????????
    """
    Distributes cards to the players as per the rules
    :param deck: the deck to be used
    number_players: the number of players in a game.
    :return: returns a list of lists with the players' hands
    """
    player1_hand = []
    player2_hand = []
    player3_hand = []
    player4_hand = []
    if (number_players == 2):
        for i in range(10):
            player1_hand.append(deck.pop())
            player2_hand.append(deck.pop())
        return [player1_hand,player2_hand]
    elif (number_players == 3):
        for i in range(10):
            player1_hand.append(deck.pop())
            player2_hand.append(deck.pop())
            player3_hand.append(deck.pop())
        return [player1_hand, player2_hand, player3_hand]
    elif (number_players == 4):
        for i in range(10):
            player1_hand.append(deck.pop())
            player2_hand.append(deck.pop())
            player3_hand.append(deck.pop())
            player4_hand.append(deck.pop())
        return [player1_hand,player2_hand,player3_hand,player4_hand]
    else:
        print("num players out of bounds")
    return 0
# ---------------------------------------------------------------------


def remove_top_card(deck):
    """
    Returns the top cards in the deck and removes it from the deck
    :param deck: deck to be used
    :return: returns the top card from the card, while removing it from the deck
    """
    return deck.pop(0)
# ---------------------------------------------------------------------


def append_card_to_deck(deck, addition):
    """
    Adds a card to the bottom of the deck
    :param deck: deck that will be added onto
    addition: card to be added
    :return: none
    """
    deck.append(addition)
# ---------------------------------------------------------------------


def player_hand_is_racko(player_hand):
    """
    Checks if the player's hand is in RACKO
    :param player_hand: the current player's hand that is being checked
    :return: Returns true if hand is in racko, false otherwise
    """
    oldvalue = -1
    for i in range(len(player_hand)):
        if (oldvalue<player_hand[i]):
            oldvalue=player_hand[i]
        else:
            return False
    return True
# ---------------------------------------------------------------------


def player_hand_value(player_hand, winner):
    """
    Calculates the value of the player's current hand
    :param player_hand: the player's current hand
    winner: boolean of whether or not the player has won first (is in RACKO)
    :return: returns a point value associated with the player's hand, as per the rules
    """
    value = 0
    if (winner):
        value += 25
    oldvalue = -1
    for i in range(len(player_hand)):
        if (oldvalue<player_hand[i]):
            oldvalue=player_hand[i]
            value += 5
    return value

def main():
    """
    Execute the validation tests for the RACK-O functions
    """

if __name__ == "__main__":
    main()
