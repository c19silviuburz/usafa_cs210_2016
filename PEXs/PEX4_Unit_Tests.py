#!/usr/bin/env python3
"""
These unit tests are for the Fall 16 PEX4 Racko code.

Run this file in PyCharm, and PyCharm will give you nicely-formatted test results.
"""

import math
import unittest

__author__ = "Robert Harder"
__email__ = "Robert.Harder@usafa.edu"
__date__ = "Nov 9, 2016"

import PEX4_Player as player_module
import PEX4_Cards as cards_module


class PEX4Test(unittest.TestCase):
    def test_player_get_name(self):
        """Tests setting, getting player name"""

        # Test initial name set and get
        tests = ["John", "Jane", "", None]
        for expected_val in tests:
            p1 = player_module.Player(expected_val)
            actual_val = p1.get_name()  # TEST THIS: Name should be what we just set it to
            self.assertEqual(expected_val, actual_val,
                             "Wrong name was returned. Expected: {}.  Got: {}."
                             .format(expected_val, actual_val))

    def test_player_hand(self):
        """Tests setting, getting player hand."""

        tests = [
            (1, 10),
            (23, 42),
            (1, 60),
            (0, 0)
        ]
        for low_card, high_card in tests:
            c1 = cards_module.Cards(low_card, high_card)
            p1 = player_module.Player("Falcon")
            p1.set_hand(c1)
            returned_hand = p1.get_hand()

            # Contents of hand modified?
            self.assertEqual(c1, returned_hand,
                             "Returned hand is not the same one that was set." +
                             "\nExpected hand: {}\nGot hand: {}"
                             .format(c1, returned_hand))

    def test_player_score(self):
        """Tests setting, getting, resetting, incrementing player score."""

        # Create players and verify score is set
        tests = [0, -1, 1, 500]
        for expected_value in tests:
            p1 = player_module.Player("Falcon", expected_value)  # Positional argument
            actual_value = p1.get_score()
            self.assertEqual(expected_value, actual_value,
                             "Wrong score was returned. Expected: {}.  Got: {}."
                             .format(expected_value, actual_value))

            p2 = player_module.Player("Falcon", score=expected_value)  # Named argument
            actual_value = p2.get_score()
            self.assertEqual(expected_value, actual_value,
                             "Wrong score was returned. Expected: {}.  Got: {}."
                             .format(expected_value, actual_value))

        # Reset score
        tests = [0, 23, 100]
        for original_score in tests:
            p1 = player_module.Player("Falcon", original_score)
            p1.reset_score()
            actual_value = p1.get_score()
            self.assertEqual(0, actual_value,
                             "Wrong score was returned after being reset. Expected: {}.  Got: {}."
                             .format(0, actual_value))

        # Increment scores
        tests = [
            (0, (0,), 0),  # Initial score, scores to add, final score
            (0, (1,), 1),
            (0, (1, 3), 4),
            (10, (23, 42), 75)
        ]
        for initial_score, additional_scores, final_score in tests:
            p1 = player_module.Player("Falcon", initial_score)
            for x in additional_scores:
                p1.increment_score(x)
            actual_value = p1.get_score()
            self.assertEqual(final_score, actual_value,
                             "Wrong score was returned after incrementing." +
                             "\nInitial score: {}. Incremented score with: {}. Expected: {}.  Got: {}."
                             .format(initial_score,
                                     ', '.join([str(x) for x in additional_scores]),
                                     final_score, actual_value))

    def test_cards_len(self):
        """Tests Cards.__len__ function"""

        tests = [
            (1, 10, 10),
            (23, 33, 11),
            (1, 60, 60),
            (0, 0, 0)
        ]
        for low_card, high_card, expected_value in tests:
            c1 = cards_module.Cards(low_card, high_card)
            actual_value = len(c1)  # TEST THIS
            self.assertEqual(expected_value, actual_value,
                             "Wrong value returned for size of deck." +
                             "\nDeck: {}".format(c1) +
                             "\n\tExpected length: {}. Got: {}.".format(expected_value, actual_value))

    def test_cards_contains(self):
        """Tests the Cards.__contains__ function"""

        tests = [
            (1, 10, 3, True),
            (1, 10, 10, True),
            (1, 10, 11, False),
            (1, 10, 23, False),
            (23, 33, 30, True),
            (23, 33, 34, False),
            (0, 0, 23, False)
        ]
        for low_card, high_card, find_me, expected_value in tests:
            c1 = cards_module.Cards(low_card, high_card)
            actual_value = find_me in c1  # TEST THIS
            if expected_value:
                self.assertTrue(actual_value, "Card {} not found in deck: {}".format(find_me, c1))
            else:
                self.assertFalse(actual_value, "Card {} incorrectly reported as found in deck: {}"
                                 .format(find_me, c1))

    def test_cards_iter_next(self):
        """Tests Cards.__iter__ and Cards.__next__ functions"""

        tests = [
            (1, 10),
            (23, 42),
            (1, 60),
            (0, 0)
        ]
        for low_card, high_card in tests:
            c1 = cards_module.Cards(low_card, high_card)

            if low_card == 0 and high_card == 0:
                expected_cards = []
            else:
                expected_cards = [x for x in range(low_card, high_card + 1)]

            actual_cards = [c for c in c1]  # TEST THIS

            self.assertEqual(expected_cards, actual_cards,
                             "Wrong cards returned by iterating over deck." +
                             "\nExpected cards: {}\nGot cards: {}"
                             .format(expected_cards, actual_cards))

    def test_cards_str(self):
        """Tests Cards.__str__ function"""

        tests = [
            (1, 10),
            (23, 42),
            (1, 60)
        ]
        for low_card, high_card in tests:
            c1 = cards_module.Cards(low_card, high_card)
            expected_cards = [x for x in range(low_card, high_card + 1)]
            str_c1 = str(c1)  # TEST THIS
            for c in expected_cards:
                self.assertIn(str(c), str_c1, "Card {} not found in string representation: {}"
                              .format(c, str_c1))

    def test_cards_get_top_card(self):
        """Tests Cards.get_top_card function"""

        # Test get_top_card
        tests = [
            # Low card, high card, expected top card response
            (1, 10, 1),
            (1, 60, 1),
            (23, 33, 23),
            (0, 0, -1),
            (1, 1, 1),
            (3, 3, 3)
        ]
        for low_card, high_card, expected_top_card in tests:
            c1 = cards_module.Cards(low_card, high_card)  # No cards
            actual_value = c1.get_top_card()  # TEST THIS
            self.assertEqual(expected_top_card, actual_value,
                             "Wrong value was returned from get_top_card with deck: {}.  Expected: {}. Got: {}"
                             .format(str(c1), expected_top_card, actual_value))

    def test_cards_get_card_at(self):
        """Tests Cards.get_card_at function"""

        # Test empty deck first
        c_empty = cards_module.Cards()
        expected_value = -1
        for i in [-1, 0, 1, 2]:
            actual_value = c_empty.get_card_at(i)  # TEST THIS
            self.assertEqual(expected_value, actual_value,
                             "Wrong value was returned from get_card_at with deck: {}, position: {}.  Expected: {}. Got: {}"
                             .format(str(c_empty), i, expected_value, actual_value))

        # Test deck with many values
        tests = [
            (1, 10),  # Low card, high card
            (1, 60),
            (23, 33)
        ]
        for low_card, high_card in tests:
            c1 = cards_module.Cards(low_card, high_card)
            expected_cards = [c for c in range(low_card, high_card + 1)]  # Should be like this list

            # Check range of valid positions
            for i in range(high_card - low_card + 1):
                expected_value = expected_cards[i]
                actual_value = c1.get_card_at(i)  # TEST THIS
                self.assertEqual(expected_value, actual_value,
                                 "Wrong value was returned from get_card_at with deck: {}, position: {}.  Expected: {}. Got: {}"
                                 .format(str(c1), i, expected_value, actual_value))

            # Check invalid positions
            expected_value = -1
            actual_value = c1.get_card_at(-1)  # TEST THIS: Index too small
            self.assertEqual(expected_value, actual_value,
                             "Wrong value was returned from get_card_at with deck: {}, position: {}.  Expected: {}. Got: {}"
                             .format(str(c1), i, expected_value, actual_value))
            actual_value = c1.get_card_at(len(expected_cards))  # TEST THIS: Index too big
            self.assertEqual(expected_value, actual_value,
                             "Wrong value was returned from get_card_at with deck: {}, position: {}.  Expected: {}. Got: {}"
                             .format(str(c1), i, expected_value, actual_value))

    def test_cards_add_to_end(self):
        """Tests Cards.add_to_end function"""

        tests = [
            # Low card, high card, tuple of cards to add to end
            (1, 10, (23,)),
            (1, 10, (23, 42)),
            (23, 33, (17, 13, 11)),
            (0, 0, (1,)),
            (3, 3, (7,))
        ]
        for low_card, high_card, additional_cards in tests:
            c_initial = cards_module.Cards(low_card, high_card)  # Copy of initial deck
            c1 = cards_module.Cards(low_card, high_card)  # Initial deck to work with

            for c in additional_cards:  # Add cards
                c1.add_to_end(c)  # TEST THIS
            expected_value = additional_cards[-1]  # What we think the last card should be

            end_pos = high_card - low_card + len(additional_cards)  # Position for get_card_at
            if low_card == 0 and high_card == 0:  # Special case when we start with an empty deck
                end_pos -= 1

            actual_value = c1.get_card_at(end_pos)  # Get the card where we think it should be

            self.assertEqual(expected_value, actual_value,
                             "Wrong value was returned after add_to_end." +
                             "\nInitial deck: {}. \nFinal Deck: {}. \n\tAdded cards: {}. Expected: {}. Got: {}"
                             .format(
                                 c_initial,
                                 c1,
                                 ', '.join([str(x) for x in additional_cards]),
                                 expected_value,
                                 actual_value))

    def test_cards_add_to_top(self):
        """Tests Cards.add_to_top function"""

        tests = [
            # Low card, high card, tuple of cards to add to top
            (1, 10, (23,)),
            (1, 10, (23, 42)),
            (23, 33, (17, 13, 11)),
            (0, 0, (1,)),
            (3, 3, (7,))
        ]
        for low_card, high_card, additional_cards in tests:
            c_initial = cards_module.Cards(low_card, high_card)  # Copy of initial deck
            c1 = cards_module.Cards(low_card, high_card)  # Initial deck to work with

            for c in additional_cards:  # Add cards
                c1.add_to_top(c)  # TEST THIS
            expected_value = additional_cards[-1]  # What we think the top card should be

            actual_value = c1.get_card_at(0)  # Get the card where we think it should be
            self.assertEqual(expected_value, actual_value,
                             "Wrong value was returned by get_card_at(0) after add_to_top." +
                             "\nInitial deck: {}. \n\tAdded cards: {}.\nFinal Deck: {}."
                             .format(
                                 c_initial,
                                 ', '.join([str(x) for x in additional_cards]),
                                 c1) +
                             "\n\tExpected card: {}. Got card: {}".format(expected_value, actual_value))

            # Also double check get_top_card
            actual_value = c1.get_top_card()  # Get the card where we think it should be
            self.assertEqual(expected_value, actual_value,
                             "Wrong value was returned by get_top_card after add_to_top." +
                             "\nInitial deck: {}. \n\tAdded cards: {}.\nFinal Deck: {}."
                             .format(
                                 c_initial,
                                 ', '.join([str(x) for x in additional_cards]),
                                 c1) +
                             "\n\tExpected card: {}. Got card: {}".format(expected_value, actual_value))

    def test_cards_replace(self):
        """Test Cards.replace function"""

        tests = [
            # Low card, high card, list of replacements, final deck
            (1, 10, [(0, 23)], [23, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
            (1, 10, [(0, 23), (3, 42)], [23, 2, 3, 42, 5, 6, 7, 8, 9, 10]),
            (1, 10, [(0, 23), (3, 42), (3, 99)], [23, 2, 3, 99, 5, 6, 7, 8, 9, 10]),
            (1, 60, [(10, 99), (10, 11)], [c for c in range(1, 61)]),
            (0, 0, [(0, 23)], [])
        ]
        for low_card, high_card, replacements, final_deck in tests:
            c_initial = cards_module.Cards(low_card, high_card)  # Copy of initial deck
            c1 = cards_module.Cards(low_card, high_card)

            for pos, new_card in replacements:
                c1.replace(pos, new_card)  # TEST THIS

            for i in range(len(final_deck)):
                expected_value = final_deck[i]
                actual_value = c1.get_card_at(i)
                self.assertEqual(expected_value, actual_value,
                                 "Wrong value returned by get_card_at({}) after replace called."
                                 .format(i) +
                                 "\nInitial deck: {}.\n\tReplaced cards (pos, card): {}\nGot deck: {}"
                                 .format(c_initial, replacements, c1) +
                                 "\n\tExpected card: {}. Got card: {}.".format(expected_value, actual_value))

            # Verify there is nothing beyond the end
            expected_value = -1
            actual_value = c1.get_card_at(len(final_deck))
            self.assertEqual(expected_value, actual_value,
                             "Too many cards in deck after replace called." +
                             "\nInitial deck: {}.\n\tReplaced cards (pos, card): {}"
                             .format(c_initial, replacements) +
                             "\nExpected deck: {}. \nGot deck: {}.".format(final_deck, c1))

    def test_cards_remove_top_card(self):
        """Tests Cards.remove_top_card function"""

        # Decks with cards
        tests = [
            (1, 10, 1),
            (1, 10, 2),
            (23, 33, 2),
            (1, 60, 60)
        ]
        for low_card, high_card, num_removals in tests:
            c_initial = cards_module.Cards(low_card, high_card)  # Copy of initial deck
            c1 = cards_module.Cards(low_card, high_card)
            expected_cards = [c for c in range(low_card, high_card + 1)]

            for i in range(num_removals):
                expected_value = expected_cards.pop(0)
                actual_vaule = c1.remove_top_card()
                self.assertEqual(expected_value, actual_vaule,
                                 "Wrong value returned after remove_top_card called {} times."
                                 .format(i + 1) +
                                 "\nInitial deck: {}\nExpected deck: {}\nGot deck: {}"
                                 .format(c_initial, expected_cards, c1))

        # Special case, deck starts empty
        c_empty = cards_module.Cards()
        expected_value = -1
        actual_vaule = c_empty.remove_top_card()
        self.assertEqual(expected_value, actual_vaule,
                         "Wrong value returned after remove_top_card called on empty deck. " +
                         "Expected: {}. Got: {}".format(expected_value, actual_vaule))

        # Special case, deck goes to empty
        c_2_initial = cards_module.Cards(1, 2)
        c_2 = cards_module.Cards(1, 2)
        c_2.remove_top_card()
        c_2.remove_top_card()
        expected_value = -1
        actual_vaule = c_2.remove_top_card()
        self.assertEqual(expected_value, actual_vaule,
                         "Wrong value returned after remove_top_card called on what should have been an empyt deck. " +
                         "\nInitial deck: {}".format(c_2_initial) +
                         "\nCalled remove_top_card 2 times." +
                         "\nExpected empty deck." +
                         "\nGot deck: {}".format(c_2))

    def test_cards_find(self):
        """Tests Cards.find function"""

        tests = [
            (1, 10),
            (1, 60),
            (23, 33)
        ]
        for low_card, high_card in tests:
            c1 = cards_module.Cards(low_card, high_card)
            expected_cards = [c for c in range(low_card, high_card + 1)]
            for i in range(len(expected_cards)):
                expected_value = i
                actual_value = c1.find(expected_cards[i])
                self.assertEqual(expected_value, actual_value,
                                 "Wrong value returned for find. " +
                                 "\nDeck: {}".format(c1) +
                                 "\n\tLooking for card: {}. Expected position: {}.  Got position: {}"
                                 .format(expected_cards[i], expected_value, actual_value))

        # Special case, empty deck
        c_empty = cards_module.Cards()
        tests = [1, 2, 3, -2, -1, 0]
        for find_me in tests:
            expected_value = -1
            actual_value = c_empty.find(find_me)
            self.assertEqual(expected_value, actual_value,
                             "Wrong value returned for find with empty deck. " +
                             "\nDeck: {}".format(c_empty) +
                             "\n\tLooking for card: {}. Expected position: {}.  Got position: {}"
                             .format(find_me, expected_value, actual_value))

    def test_cards_shuffle(self):
        """Tests Cards.shuffle function"""

        tests = [
            (1, 40),
            (1, 50),
            (1, 60)
        ]
        for low_card, high_card in tests:
            c_initial = cards_module.Cards(low_card, high_card)
            c1 = cards_module.Cards(low_card, high_card)

            c1.shuffle()  # TEST THIS

            # Check that we have right number of cards
            expected_value = high_card - low_card + 1
            actual_value = len(c1)
            self.assertEqual(expected_value, actual_value,
                             "Wrong number of cards in deck after shuffle called." +
                             "\nInitial deck ({} cards): {}.".format(expected_value, c_initial) +
                             "\nGot deck ({} cards): {}.".format(actual_value, c1))

            # Need to extract the cards and make a list for the helper function
            cards = []
            for i in range(high_card - low_card + 1):
                cards.append(c1.get_card_at(i))
            self.assertTrue(self.is_random_according_to_runs_test(cards),
                            "Shuffled deck not sufficiently random: " + str(cards))

            # Make sure shuffle doesn't die because deck is already shuffled
            c1.shuffle()  # Could throw exception of shuffle() relied on cards being in order

    def is_random_according_to_runs_test(self, values: [int]):
        """
        Supporing function to test randomness according to the Runs test (as taught in CS362!)
        """
        runs = 1
        prev = values[0]
        was_going_up = values[0] < values[1]
        was_going_down = values[0] > values[1]
        for curr in values[1:]:  # Start at 2nd value

            if curr > prev:
                # Going up
                if was_going_up:  # Already doing that
                    pass
                else:
                    # Reverse! We _were_ going down
                    runs += 1
                    was_going_up = True
                    was_going_down = False
            elif curr < prev:
                # Going down
                if was_going_down:  # Already doing that
                    pass
                else:
                    # Reverse! We _were_ going up
                    runs += 1
                    was_going_down = True
                    was_going_up = False

            prev = curr

        # Runs Test
        n = len(values)
        mu_r = (2 * n - 1) / 3
        sigma_2_r = (16 * n - 29) / 90
        z_0 = (runs - mu_r) / math.sqrt(sigma_2_r)
        # z_alpha_05 = 1.96
        # z_alpha_02 = 1.282
        # z_alpha_002 = 3.090
        z_alpha_001 = 3.291  # 99.9% -- be cautious in declaring that the deck is not shuffled

        return abs(z_0) < z_alpha_001


if __name__ == "__main__":
    # PyCharm doesn't actually run this line.  It detects that you have test code and does its own internal thing.
    unittest.main()
