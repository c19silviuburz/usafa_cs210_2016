#!/usr/bin/env python

"""
Verify that RACK-O functions work correctly.

Documentation Statement: None

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
from PEX2_Racko_functions import create_deck, \
    get_number_of_players, shuffle_deck, pass_out_cards, \
    remove_top_card, append_card_to_deck, player_hand_is_racko, player_hand_value

# Metadata
__author__ = "Wayne Brown"
__email__ = "Wayne.Brown@usafa.edu"
__date__ = "Oct 12, 2016"
# ---------------------------------------------------------------------


def correct_cards_in_deck(deck):
    """
    Verify that a deck of cards contains the correct card values
    :param deck: a list of integers
    :return: True or False
    """
    # Verify that each card was found only once
    valid_deck = True
    for card_value in range(1, len(deck)+1):
        occurrences = deck.count(card_value)
        if occurrences == 0:
            print("Test failed: card {} is missing from the deck".format(card_value))
            valid_deck = False
        elif occurrences > 1:
            print("Test failed: card {} is in the deck {} times".
                  format(card_value, occurrences))
            valid_deck = False

    # If the deck is invalid, display it
    if not valid_deck:
        print("Bad deck: ", deck)

    return valid_deck
# ---------------------------------------------------------------------


def verify_create_deck():
    """
    Verify the correctness of the create_deck() function
    """
    number_of_players = [2, 3, 4]
    expected_deck_size = [40, 50, 60]

    # Test for valid inputs
    for test in range(len(number_of_players)):
        deck = create_deck(number_of_players[test])

        if not isinstance(deck, list):
            print("Test failed: the return value of create_deck must be a list")

        if len(deck) != expected_deck_size[test] or not correct_cards_in_deck(deck):
            print("Test failed: {} player deck is incorrect.".format(number_of_players[test]))
            print("The invalid deck is ", deck)

    # Test for invalid inputs
    for number_players in [0, 1, 5, 6, 7]:
        deck = create_deck(number_players)
        if deck is not None:
            print("Test failed: {} players did not return as an error".format(number_players))
# ---------------------------------------------------------------------


def verify_get_number_of_players():
    """
    Verify that get_number_of_players() works correctly
    """

    # Test for valid input
    values_expected = [2, 3, 4, -1]
    for n in values_expected:
        print("Make get_number_of_players() return a {}".format(n))
        value_returned = get_number_of_players()
        if value_returned != n:
            print("Test failed on get_number_of_players(): expected {} but got back {}"
                  .format(n, value_returned))

    # Test for invalid input
    print("Try to input invalid input into get_number_of_players()")
    value_returned = get_number_of_players()
    if value_returned not in [-1, 2, 3, 4]:
        print("Test failed on get_number_of_players(): got back {} which is invalid"
              .format(value_returned))
# ---------------------------------------------------------------------


def number_of_cards_in_original_position(deck):
    """
    Count how many cards are in their original position in the deck. For
    example, if card 1 is in position 1, count this card.
    :param deck: a list of integers
    :return: The number of cards that are in their original position.
    """
    count = 0
    for index in range(0, len(deck)):
        if deck[index] == index + 1:
            count += 1

    return count
# ---------------------------------------------------------------------


def verify_shuffle_deck():
    """
    Verify that shuffle_deck() works correctly
    """
    for num_players in [2, 3, 4]:
        deck = create_deck(num_players)
        shuffle_deck(deck)
        if number_of_cards_in_original_position(deck) > 3:
            print("Test failed on shuffle_deck(): The card sequence is not sufficiently shuffled.")
            print("Failed deck", deck)

        if not correct_cards_in_deck(deck):
            print("Test failed on shuffle_deck(): The shuffle lost or gained cards.")
# ---------------------------------------------------------------------


def combine_decks(hands, deck):
    """
    Given a list of hands and all the other cards in a deck, recombine all
    of the cards into a single list
    :param hands: a list of 2, 3, or 4 hands, each with 10 cards
    :param deck: a list of cards
    :return: one list containing all cards
    """
    all_cards = []
    for one_hand in hands:
        all_cards += one_hand

    all_cards += deck

    return all_cards
# ---------------------------------------------------------------------


def verify_pass_out_cards():
    """
    Verify that the pass_out_cards() function works correctly.
    """
    for num_players in [2, 3, 4]:
        deck = create_deck(num_players)
        hands = pass_out_cards(deck, num_players)

        # Test to make sure there are the correct number of hands.
        if len(hands) != num_players:
            print("Test failed on pass_out_cards(): Expected {} hands but got {} hands."
                  .format(num_players, len(hands)))
        else:
            # We have the correct number of hands.
            # Now test for 10 cards in each hand.
            for one_hand in hands:
                if len(one_hand) != 10:
                    print("Test failed on pass_out_cards(): hand does not have 10 cards.")
                    print("  failed hand is ", one_hand)

                # Make sure the original deck does not contain the cards
                # in the hand.
                for card in one_hand:
                    if card in deck:
                        print("Test failed on pass_out_cards(): {} is in both the hand and "
                              "the deck".format(card))

        # Make sure the hands and the remaining deck did not lose any cards
        all_cards = combine_decks(hands, deck)
        if not correct_cards_in_deck(all_cards):
            print("Test failed on pass_out_cards(): the hands and deck are missing cards.")
# ---------------------------------------------------------------------


def verify_remove_top_card():
    """
    Verify that the remove_top_card function works correctly.
    """
    test_data_front = [[1, 2, 3], 1, [2, 3],
                       [10, 20], 10, [20],
                       [], -1, []]

    test_data_end = [[1, 2, 3], 3, [1, 2],
                     [10, 20], 20, [10],
                     [], -1, []]

    # Determine whether the card is removed from the start or the end
    card = remove_top_card([1, 2, 3])
    if card == 1:
        tests = test_data_front
        changed_index_0 = True
    elif card == 3:
        tests = test_data_end
        changed_index_0 = False
    else:
        print("Fatal error for remove_top_card(), neither end worked")
        return False

    for which_test in range(0, len(tests), 3):
        deck = tests[which_test]
        expected_card = tests[which_test+1]
        expected_list = tests[which_test+2]

        card = remove_top_card(deck)
        if card != expected_card:
            print("Test failed on remove_top_card: expected {} but got {}"
                  .format(expected_card, card))
            print("The test deck was: ", deck)

        if deck != expected_list:
            print("Test failed on remove_top_card: expected {} but deck is {}"
                  .format(expected_list, deck))

    return changed_index_0
# ---------------------------------------------------------------------


def verify_append_card_to_deck():
    """
    Verify that the append_card_to_deck function works correctly.
    """
    test_data_front = [[1, 2, 3], 23, [23, 1, 2, 3],
                       [10, 20], 5, [5, 10, 20],
                       [], 3, [3]]

    test_data_end = [[1, 2, 3], 23, [1, 2, 3, 23],
                     [10, 20], 5, [10, 20, 5],
                     [], 3, [3]]

    # Determine whether the card is removed from the start or the end
    deck = [1, 2, 3]
    append_card_to_deck(deck, 4)
    if deck == [4, 1, 2, 3]:
        tests = test_data_front
        changed_index_0 = True
    elif deck == [1, 2, 3, 4]:
        tests = test_data_end
        changed_index_0 = False
    else:
        print("Fatal error for append_card_to_deck(), neither end worked")
        return False

    for which_test in range(0, len(tests), 3):
        deck = tests[which_test]
        card_to_add = tests[which_test + 1]
        expected_list = tests[which_test + 2]

        append_card_to_deck(deck, card_to_add)
        if deck != expected_list:
            print("Test failed on append_card_to_deck: expected {} but deck is {}"
                  .format(expected_list, deck))

    return changed_index_0
# ---------------------------------------------------------------------


def verify_player_hand_is_racko():
    """
    Verify that the player_hand_is_racko function works correctly.
    """
    test_data = [[1, 3, 6, 12, 20, 21, 25, 37, 42, 43], True,
                 [60, 3, 6, 12, 20, 21, 25, 37, 42, 43], False,
                 [1, 60, 6, 12, 20, 21, 25, 37, 42, 43], False,
                 [1, 3, 60, 60, 20, 21, 25, 37, 42, 43], False,
                 [1, 3, 6, 12, 60, 21, 25, 37, 42, 43], False,
                 [1, 3, 6, 12, 20, 60, 25, 37, 42, 43], False,
                 [1, 3, 6, 12, 20, 21, 60, 37, 42, 43], False,
                 [1, 3, 6, 12, 20, 21, 25, 60, 42, 43], False,
                 [1, 3, 6, 12, 20, 21, 25, 37, 60, 43], False,
                 [1, 3, 6, 12, 20, 21, 25, 37, 42, 60], True
                 ]
    # Check to make sure a non-racko hand is recognized
    for which_test in range(0, len(test_data), 2):
        hand = test_data[which_test]
        expected_result = test_data[which_test+1]

        if player_hand_is_racko(hand) != expected_result:
            print("Test failed on player_hand_is_racko:")
            print("The hand was: ", hand)
# ---------------------------------------------------------------------


def verify_player_hand_value():
    """
    Verify that the verify_player_hand_value function works correctly.
    """
    # Test data has 3 values, hand, winner, expected value
    test_data = [[[1, 3, 6, 12, 20, 21, 25, 37, 42, 43], True, 75],
                 [[60, 3, 6, 12, 20, 21, 25, 37, 42, 43], False, 5],
                 [[1, 60, 6, 12, 20, 21, 25, 37, 42, 43], False, 10],
                 [[1, 3, 60, 12, 20, 21, 25, 37, 42, 43], False, 15],
                 [[1, 3, 6, 60, 20, 21, 25, 37, 42, 43], False, 20],
                 [[1, 3, 6, 12, 60, 21, 25, 37, 42, 43], False, 25],
                 [[1, 3, 6, 12, 20, 60, 25, 37, 42, 43], False, 30],
                 [[1, 3, 6, 12, 20, 21, 60, 37, 42, 43], False, 35],
                 [[1, 3, 6, 12, 20, 21, 25, 60, 42, 43], False, 40],
                 [[1, 3, 6, 12, 20, 21, 25, 37, 60, 43], False, 45],
                 [[1, 3, 6, 12, 20, 21, 25, 37, 42, 60], False, 50]
                 ]

    for which_test in range(len(test_data)):
        hand = test_data[which_test][0]
        winner = test_data[which_test][1]
        expected_result = test_data[which_test][2]

        score = player_hand_value(hand, winner)
        if score != expected_result:
            print("Test failed on player_hand_value: expected {} but got {}"
                  .format(expected_result, score))
            print("The hand was: ", hand)
            print("The winner was: ", winner)
# ---------------------------------------------------------------------


def main():
    """
    Execute the validation tests for the RACK-O functions
    """
    verify_create_deck()
    verify_get_number_of_players()
    verify_shuffle_deck()
    verify_pass_out_cards()
    remove_changed_index_0 = verify_remove_top_card()
    append_changed_index_0 = verify_append_card_to_deck()
    verify_player_hand_is_racko()
    verify_player_hand_value()

    if remove_changed_index_0 != append_changed_index_0:
        print("Warning: remove_top_card and append_card_to_deck should change the same"
              "end of the deck, but they do not")

if __name__ == "__main__":
    main()
