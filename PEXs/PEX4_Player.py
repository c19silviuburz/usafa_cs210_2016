#!/usr/bin/env python

"""
A Player in a RACK-O game.

Documentation: C3C Van Epps explained the scope of some of the functions to me because it was unclear what the document was asking for
"""
# =====================================================================

# Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "17 Nov 16"

# ---------------------------------------------------------------------
from PEX4_Cards import Cards


class Player:
    """ Define a player in a card game. """

    HUMAN_PLAYER = 0
    COMPUTER_PLAYER = 1
    NETWORK_PLAYER = 2

    def __init__(self, name, score=0, type=HUMAN_PLAYER):
        """
        Initializes the player's attributes
        :return: None
        """
        self.name = name
        self.score = score
        self.type = type
        self.hand = Cards()


    def __str__(self):
        """
        Loads a string with information about the player and returns it
        :return: String
        """
        string = ""
        string += str("Player's name: ")
        string += str(self.name)
        string += str("\nPlayer's score: ")
        string += str(self.score)
        string += str("\nPlayer's type: ")
        string += str(self.type)
        string += str("\nPlayer's hand: ")
        string += str(self.hand)
        return string

    def get_name(self):
        """

        :return: player's name
        """
        return self.name

    def get_hand(self):
        """

        :return: player's hand
        """
        return self.hand

    def set_hand(self, hand):
        """
        sets the input hand as the player's hand
        :return: None
        """
        self.hand = hand

    def get_score(self):
        """

        :return: player's score
        """
        return self.score

    def reset_score(self):
        """
        Sets player's score to 0
        :return: None
        """
        self.score = 0

    def increment_score(self, amount):
        """
        Increases the players score by the amount passed
        :return: None
        """
        self.score += amount
