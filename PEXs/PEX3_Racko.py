#!/usr/bin/env python

"""
This code will verify the completeness and correctness of a rack-o game through a command line interface

Documentation Statement:
NONE
"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
from PEX2_Racko_functions import create_deck, get_number_of_players, \
    shuffle_deck, pass_out_cards, remove_top_card, append_card_to_deck, \
    player_hand_is_racko, player_hand_value

# Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "OCT 26, 2016"

# CONSTANTS
# Options for initial input from the user
NEW_GAME = 1
LOAD_PREVIOUS_GAME = 2

# Game status
PLAY_GAME = 5
GAME_OVER = 6
QUIT_GAME = -1

# Indexes into the game list to access game data
DEALER = 0
ACTIVE_PLAYER = 1
HANDS = 2
DRAW_PILE = 3
DISCARD_PILE = 4
PLAYER_SCORES = 5
# ---------------------------------------------------------------------


def start_of_game():
    """
    Get the user preferences for starting RACK-O.
    :return: game_option - NEW_GAME, LOAD_PREVIOUS_GAME, or QUIT_GAME
    :return: number_of_players - an integer, 2, 3, or 4
    """
    # Set default return values
    game_type = 0
    number_of_players = 0

    # Does the user want to start a new game or continue a previous game?
    print("{}) Start a new game?".format(NEW_GAME))
    print("{}) Load a previous game?".format(LOAD_PREVIOUS_GAME))
    print("q) Quit?")
    while game_type == 0:
        option = input("Option: (1, 2 or q): ")
        if option.lower() == "q":
            game_type = QUIT_GAME
        elif option == "1" or option == "2":
            game_type = int(option)
        else:
            print("Invalid input. Please re-enter your choice.")

    if game_type == NEW_GAME:
        number_of_players = get_number_of_players()

    return game_type, number_of_players
# ---------------------------------------------------------------------

def initialize_game(game_type, number_of_players):
    """
    initializes the game list with the proper data to run a racko game
    :param game_type: 1 for new game, 2 for load
    number_of_players: how many players are desired in the game
    :return: returns a game list with the proper data to start
    """
    if (game_type==1):
        deck = shuffle_deck(create_deck(number_of_players))
        hands = pass_out_cards(deck, number_of_players)
        return [0,1,hands, deck,[],[],[0]*number_of_players]
    elif (game_type==2):
        return load_game()

def play_hand(game):
    """
    Main engine for running the game, handles all functions associated with the progression of a typical game
    :param game: contains data with the game at hand
    :return: returns if the game is over or not
    """
    #discard pile, draw pile, quit, save
    print("{}) Draw from discard pile?".format("1"))
    print("{}) Draw from draw pile?".format("2"))
    print("q) Quit?")
    print("s) Save?")
    option = input("Option: (1, 2, q, s): ")
    if option.lower() == "q":
        game_type = QUIT_GAME
    elif option.lower() == "s":
        save_game(game)
    elif option == "1":
        STATUS = PLAY_GAME
        card = remove_top_card(game[3])
        print("{}) Replace a card in your current hand.".format("1"))
        print("{}) Return this card to the the discard pile.".format("2"))
        option = input("Option: (1, 2): ")
        if option == "1":
            print("Your cards: ", game[2][ACTIVE_PLAYER])
            print("Index of the card you wish to replace?")
            option = int(input("Index? "))
            while ((option < 0) or (option > 10)):
                print("Invalid input")
                option = input("Index? ")
            append_card_to_deck(game[4],game[2][ACTIVE_PLAYER][option])
            game[2][ACTIVE_PLAYER][option] = card
        elif option == "2":
            append_card_to_deck(game[4], card)
            return PLAY_GAME
        else:
            print("Invalid input. Please re-enter your choice.")
    elif option == "2":
        STATUS = PLAY_GAME
        card = remove_top_card(game[3])
        print("{}) Replace a card in your current hand.".format("1"))
        print("{}) Send this card to the the discard pile.".format("2"))
        option = input("Option: (1, 2): ")
        if option == "1":
            print("Your cards: ", game[2][ACTIVE_PLAYER])
            print("Index of the card you wish to replace?")
            option = int(input("Index? "))
            while ((option < 0) or (option > 10)):
                print("Invalid input")
                option = input("Index? ")
            append_card_to_deck(game[4],game[2][ACTIVE_PLAYER][option])
            game[2][ACTIVE_PLAYER][option] = card
        elif option == "2":
            append_card_to_deck(game[4], card)
            return PLAY_GAME
        else:
            print("Invalid input. Please re-enter your choice.")
    else:
        print("Invalid input. Please re-enter your choice.")
    return PLAY_GAME

def update_player_scores(game):
    """
    updates the scores portion of the list "game"
    :param game: game at hand
    :return: returns an updated "game" list
    """
    game[5][ACTIVE_PLAYER] = player_hand_value(game[3][ACTIVE_PLAYER],player_hand_is_racko(game[3][ACTIVE_PLAYER]))
    return game

def display_round_winner(game):
    """
    displays the winner of the current round
    :param game: game at hand
    :return: returns none because it just displays
    """
    for i in range(len(game[2])):
        if game[5][i] == max(game[5]):
            print("Round winner is player ", i ," with a score of ", game[5][i])

def reset_for_next_round(game):
    """
    Resets the "game" list to being a new round
    :param game: game at hand, will be converted to start a new game
    :return: returns a the new game data, preserving the scores
    """
    game[0] += 1
    if game[0] > len(game[2]):
        game[0] = 0
    game[4] = []
    newdeck = shuffle_deck(create_deck(len(game[2])))
    game[2] = pass_out_cards(newdeck, len(game[2]))
    return game


def display_game_winner(scores):
    """
    Displays who wins the current game
    :param scores: list of the scores of the players in the game
    :return: returns none because it just displays
    """
    for i in range(len(scores)):
        if scores[i] == max(scores[i]):
            print("Round winner is player ", i ," with a score of ", scores[i])

def save_game(game):
    """
    Saves the current "game" list to a file for later recovery
    :param game: game data to be saved
    :return: returns none
    """
    keys = ['DEALER', 'ACTIVE_PLAYER', 'HANDS', 'DRAW_PILE', 'DISCARD_PILE', "SCORE"]
    target = input("Full directory you would like to save to?")
    targetgame = open(target, 'w')
    for i in range(len(game)):
        targetgame.write(str(keys[i], " = ", str(game[i]), "\n"))
    targetgame.close()

def load_game():
    """
    Loads old game data from a file
    :param none
    :return: returns the game data it just loaded in the "game" list
    """
    game = []
    target = input("Full directory you would like to load from?")
    targetgame = open(target, 'r')
    for i in range(6):
        data = targetgame.readlines()
        vals = data.split("=")
        vals2 =  vals[1].strip()
        game[i] = int(vals2)
    return game

def main():
    """
    Play the game of RACK-O.
    :return: None

    The data of a RACK-O game is stored in a nested list as follows:
    game = [ dealer, current_player, hands, draw_pile, discard_pile, player_scores ]
      where,
         dealer is an integer in the range [0,number_players-1]
         current_player is an integer in the range [0,num_players-1]
         hands is a list of lists [ hand0, hand1, hand2, hand3 ]
                Note: each hand[0-3] is a list of 10 integers
         draw_pile is a list of 0 or more integers
         discard_pile is a list of 0 or more integers
         player_scores is a list of integers, one score per player

    Here is full description of a game for 3 players:
    game = [ 0,  # DEALER
             1,  # ACTIVE_PLAYER
             [ [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],         # HANDS
               [11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
               [21, 22, 23, 24, 25, 26, 27, 28, 29, 30] ],
             [ 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,  # DRAW_PILE
               41, 42, 43, 44, 45, 46, 47, 48, 49, 50],
             [],  # DISCARD_PILE
             [ 0, 0, 0]  # PLAYER_SCORES
           ]
    """

    # Get initial user option: start a new game or load a previous game?
    game_type, number_of_players = start_of_game()
    if game_type != QUIT_GAME:

        # Initialize a RACK-O game, either from a file, or start a new game
        game = initialize_game(game_type, number_of_players)

        print(game)

        # Play the game
        status = PLAY_GAME
        while status == PLAY_GAME:

            # Current player takes a turn
            status = play_hand(game)
            if status == PLAY_GAME:

                # Check for a winner
                if player_hand_is_racko(game[HANDS][ACTIVE_PLAYER]):
                    update_player_scores(game)
                    display_round_winner(game)

                    # Is the entire game over?
                    if max(game[PLAYER_SCORES]) >= 500:
                        status = GAME_OVER
                    else:
                        reset_for_next_round(game)
                else:
                    # Switch active player
                    game[ACTIVE_PLAYER] = (game[ACTIVE_PLAYER] + 1) % number_of_players

        # The game is over; display the game winner
        if status == GAME_OVER:
            display_game_winner(game[PLAYER_SCORES])

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()