#!/usr/bin/env python

"""
Interface functions for a RACK-O game. These 4 functions get all
required user input.
"""
# =====================================================================

# Metadata
__author__ = "Dr. Wayne Brown"
__email__ = "Wayne.Brown@usafa.edu"
__date__ = "Nov 8, 2016"


# Constants for the game choices and state. They are all negative to
# guarantee that the values are distinguishable from card values.
QUIT_GAME = -1
NEW_GAME = -2
LOAD_PREVIOUS_GAME = -3
SAVE_GAME = -4
DRAW_FROM_THE_DRAW_PILE = -5
DRAW_FROM_THE_DISCARD_PILE = -6

# ---------------------------------------------------------------------


def get_number_of_players():
    """
    Prompt a user for the number of players in a game.
    :return: QUIT_GAME (-1) if the user entered quit, or 2, 3, or 4 players
    """
    valid_inputs = ["2", "3", "4", "quit", "q"]

    # Prompt the user for their input
    user_input = input("Please enter the number of players (2, 3, or 4): ")
    user_input = user_input.lower().strip()

    # Continue to prompt the user for input if they entered an invalid value.
    while user_input not in valid_inputs:
        print("Invalid input. The valid number of players must be 2, 3, or 4.")
        user_input = input("Please re-enter your choice: ")
        user_input = user_input.lower().strip()

    if user_input == "quit" or user_input == "q":
        return QUIT_GAME
    else:
        number_of_players = int(user_input)
        return number_of_players
# ---------------------------------------------------------------------


def get_how_to_start_game():
    """
    Get the user preferences for starting RACK-O.
    :return: game_option - NEW_GAME, LOAD_PREVIOUS_GAME, or QUIT_GAME
    """

    valid_inputs = ["1", "2", "quit", "q"]

    # Show a user their 3 choices.
    print("1) Start a new game?")
    print("2) Load a previous game?")
    print("q) Quit?")
    user_input = input("Option: (1, 2 or q): ")

    # Continue to prompt the user for input if they entered an invalid value.
    while user_input not in valid_inputs:
        print("Invalid input. Your choices are 1, 2, or q.")
        user_input = input("Please re-enter your choice: ")
        user_input = user_input.lower().strip()

    if user_input == "q":
        return QUIT_GAME
    elif user_input == "1":
        return NEW_GAME
    elif user_input == "2":
        return LOAD_PREVIOUS_GAME

# ---------------------------------------------------------------------


def get_draw_card_choice(top_discard_pile_card):
    """
    Allow a player to chose from drawing a card from the DISCARD pile or the DRAW pile
    :param top_discard_pile_card: the top card on the discard pile
    :return: SAVE_GAME, QUIT_GAME, DRAW_FROM_THE_DRAW_PILE, or DRAW_FROM_THE_DISCARD_PILE
    """
    valid_inputs = ["1", "2", "s", "q"]

    # Show a user their choices.
    print("1) Pick up a draw pile card (the value is unknown).")
    print("2) Pick up the {} on the top of the discard pile."
          .format(top_discard_pile_card))
    print("s) Save the game.")
    print("q) Quit playing.")
    user_input = input("Choice (1,2,s,q): ")

    # Continue to prompt the user for input if they entered an invalid value.
    while user_input not in valid_inputs:
        print("Invalid input. Your choices are 1, 2, s, or q.")
        user_input = input("Please re-enter your choice: ")
        user_input = user_input.lower().strip()

    if user_input == 's':
        return SAVE_GAME
    elif user_input == 'q':
        return QUIT_GAME
    elif user_input == "1":
        return DRAW_FROM_THE_DRAW_PILE
    elif user_input == "2":
        return DRAW_FROM_THE_DISCARD_PILE
    else:
        print("Error in get_draw_card_choice, This should never happen.")
        return DRAW_FROM_THE_DRAW_PILE

# ---------------------------------------------------------------------


def get_discard_choice(player_hand, drawn_card):

    valid_inputs = ['s', 'q', str(drawn_card)]
    for card in player_hand:
        valid_inputs.append(str(card))

    # Show a user their choices.
    print("Which card should be discarded? One of {} or {}".format(player_hand, drawn_card))
    print("If you discard a card in the hand, the drawn card replaces it.")
    print("s) Save the game.")
    print("q) Quit playing.")
    user_input = input("Choice: ")

    # Continue to prompt the user for input if they entered an invalid value.
    while user_input not in valid_inputs:
        print("Invalid input.")
        user_input = input("Please re-enter your choice: ")
        user_input = user_input.lower().strip()

    if user_input == 's':
        return SAVE_GAME
    elif user_input == 'q':
        return QUIT_GAME
    else:
        return int(user_input)  # A card value
# ---------------------------------------------------------------------
