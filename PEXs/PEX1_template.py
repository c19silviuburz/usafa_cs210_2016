#!/usr/bin/env python

"""
This program draws a facsimile of the squadron patch for squadron 37 using turtle graphics.

Documentation Statement: NONE!

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
import math
from turtle import TurtleScreen, RawTurtle, TK

# Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "Aug 31, 2016"

# Constants
CANVAS_WIDTH = 600
CANVAS_HEIGHT = int(CANVAS_WIDTH * 9 / 16)  # Produces the eye-pleasing 16:9 HD aspect ratio.

# Create a Tkinter graphics window
graphics_window = TK.Tk()
graphics_window.title("Squadron 37 Patch")

# Create a "canvas" inside the graphics window to draw on
my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
my_canvas.pack()  # Organizes my_canvas inside the graphics window

# Create a "canvas" made specifically for turtle graphics
turtle_canvas = TurtleScreen(my_canvas)
turtle_canvas.bgcolor("white")

# Create a turtle to draw on the canvas
fred = RawTurtle(turtle_canvas)

# If you want the drawing to be as fast as possible, uncomment these lines
# turtle_canvas.delay(0)
# sammy.hideturtle()
# sammy.speed("fastest")

# Draw squadron NN patch (your code goes here)


scale = float(input("Scale? Suggested is ~250"))
scale /= 229 #this number corresponds to the diameter of the initial circle that is drawn, determined by measuring the radius
#adjusting the scale based on the input allows for the user's input to translate directly to pixels
#scale is used as a multiplier throughout the program whenever a distance or a size is called, in order to keep the proportion

fred.hideturtle()
fred.speed("fastest") #optimizations

fred.up()
fred.left(90)
fred.forward(100*scale)
fred.right(90)
fred.down()
fred.color("yellow")
fred.width(5*scale) #positions itself to draw the outer circle, picks the settings

fred.fillcolor("blue")
fred.begin_fill()
initheading = fred.heading()
for k in range(10000): #loop to draw a circle by stepping and turning
    fred.forward(2*scale)
    fred.right(1)
    if (initheading == fred.heading()): #stop if completed circle
        break
fred.end_fill() #draws outer circle


fred.up()
fred.forward(25*scale)
fred.right(90)
fred.forward(30*scale)
fred.left(90)
fred.down() #positions itself to draw the grey circle (the moon)

initheading = fred.heading()
fred.color("grey")
fred.fillcolor("grey")
fred.begin_fill()
for k in range(10000):
    fred.forward(.9*scale)
    fred.right(1)
    if (initheading == fred.heading()):
        break
fred.end_fill()  #draws the moon similarly to the first circle, smaller scale and different color

fred.color("red")
fred.up()
fred.forward(18*scale)
fred.right(90)
fred.forward(55*scale)
fred.left(90)
fred.write("3",False, align = "center",font = ("Arial", int(24*scale), "bold"))
fred.forward(17*scale)
fred.right(90)
fred.forward(4*scale)
fred.write("7",False, align = "center",font = ("Arial", int(20*scale), "bold")) #positions itself and places the squadron number appropriately


fred.forward(8*scale)
fred.left(90)
fred.forward(3*scale)
fred.right(90)
fred.down() #positions itself to draw the first crater
initheading = fred.heading()
fred.color("white")
fred.width(.5*scale)
for k in range(10000):
    fred.left(65)
    fred.forward(.3*scale)
    fred.right(65) #alternates between +/- 65 degress to create the appearance of jagged edges on the crater
    fred.forward(.13*scale) #carry on with the step procedure
    fred.right(1.5) #moves along the circle as per the previous methods
    if (initheading == fred.heading()): #draws the first crater, similar to previous two circles (lower one)
        break

fred.up()
fred.left(180)
fred.forward(65*scale)
fred.left(90)
fred.forward(35*scale)
fred.right(180) #position to draw second crater

fred.down()
initheading = fred.heading() #similar to above
fred.color("white")
fred.width(.5*scale)
for k in range(10000):
    fred.left(65)
    fred.forward(.3*scale)
    fred.right(65)
    fred.forward(.13*scale)
    fred.right(3.5) #decreased turn radius for smaller circle
    if (initheading == fred.heading()): #draws the second (upper) crater, procedure is similar to above
        break

fred.up()
fred.right(90)
fred.forward(100*scale)
fred.down() #position to draw horse

fred.width(6)
fred.left(45) #haphazard attempt at creating the horse and rider
fred.color("yellow")
fred.fillcolor("red")
fred.begin_fill()
fred.forward(75*scale)
fred.right(90)
fred.forward(20*scale)
fred.right(30)
fred.forward(20*scale)
fred.right(60)
fred.forward(95*scale)
fred.left(95)
fred.forward(50*scale)
fred.right(90)
fred.forward(8*scale)
fred.right(85)
fred.forward(25*scale)
fred.left(30)
fred.forward(35*scale)
fred.left(90)
fred.forward(45*scale)
fred.right(80)
fred.forward(35*scale)
fred.right(80)
fred.forward(55*scale)
fred.right(100)
fred.forward(35*scale)
fred.left(56)
fred.forward(68*scale)
fred.end_fill() #complete the loop and fill in the interior

# Keep the window open until the user closes it.
TK.mainloop()
