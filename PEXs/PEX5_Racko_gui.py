#!/usr/bin/env python

"""
PEX 5 - A Rack-o game using a GUI.
Documentation: C3C Van Epps explained to me the flow structure of the game in terms of GUI,
using the buttons to execute the turns and changing of the player in the game
"""
# =====================================================================

import tkinter as tk
from tkinter import ttk, Menu
from tkinter import filedialog
from tkinter import messagebox
from tkinter import simpledialog
from tkinter import messagebox
import os  # to be able to get the current working directory; os.getcwd()
import pickle
from PEX5_RACKO_game import RackoGame, GAME_OVER

# Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "17 Nov 16"


class RackoGUI:
    """ A GUI interface for the game of RACK-O """

    # Modes of play
    DRAW_A_CARD = -1
    DISCARD_A_CARD = -2

    # The source of a drawn card
    CARD_NOT_DRAWN = -4
    THE_DRAW_PILE = -5
    THE_DISCARD_PILE = -6

    def __init__(self):
        """
        Initializes the windows and necessary variables to run the GUI racko game, and then starts it
        :return: None
        """
        self.window = tk.Tk()
        self.menubar = Menu(self.window)
        self.filemenu = Menu(self.menubar, tearoff=0)
        # Hide the initial window until we get the number of players
        self.window.withdraw()
        self.window.title("RACKO")
        self.frames = []
        self.buttonslist = []



        # To use the tkinter dialog boxes you must have a root window.
        # Therefore, the prompt for the number of players is done after
        # the root window has been created.
        number_of_players = simpledialog.askinteger("RACKO Startup",
                                                    "How many players? (2, 3, or 4)",
                                                    parent=self.window,
                                                    minvalue=2, maxvalue=4)
        if number_of_players is None: # The user selected "Cancel"
            # Cancel this application
            self.window.destroy()
            exit()

        self.game = RackoGame(number_of_players)
        self.create_widgets(number_of_players)
        self.initializeGUI(number_of_players)

        # Create the game information


    def create_widgets(self, number_of_players):
        """
        Creates the widgets necessary for running a gui racko game, including all the card buttons and control buttons
        :return: None
        """
        self.filemenu.add_command(label="Load", command = self.loadgame)
        self.filemenu.add_command(label="Save", command = self.savegame)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Restart", command = self.restartgame)
        self.filemenu.add_command(label="Exit", command=self.exitgame)
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.window.config(menu=self.menubar)

        for i in range(number_of_players):
            self.frames.append(ttk.LabelFrame(self.window, text="Player " + str(i+1), relief=tk.RIDGE))
            self.frames[i].grid(row=1, column=i+1, sticky=tk.E + tk.W + tk.N + tk.S)
            self.buttonslist.append([])
            self.create_player_frame(i)
            self.create_pile_buttons(number_of_players)
        self.window.deiconify()


    def create_player_frame(self, column):
        """
        Creates the individual frames containing the buttons for each player
        :return: None
        """
        for i in range(10):
            self.buttonslist[column].append(tk.Button(self.frames[column], height = 1, width = 15, text=str(i+1)))
            self.buttonslist[column][i].grid(row=i+1, column=column, sticky=tk.W, padx=10, pady = 4)
            #self.buttonslist[column][i].bind("<ButtonRelease-1>", self.discard_card_in_hand)
            self.buttonslist[column][i].configure(state = 'disabled')

    def create_pile_buttons(self, number_of_players):
        """
        Creates the control buttons necessary for drawing card to play RACKO
        :return: None
        """
        self.controlframe = ttk.LabelFrame(self.window, text="Control Options", relief=tk.RIDGE)
        self.controlframe.grid(row=11, column=1, sticky=tk.E + tk.W + tk.N + tk.S)
        self.drawbutton = tk.Button(self.controlframe, height = 1, width = 8, text="Draw Pile")
        self.drawbutton.grid(row= 1, column=1, sticky=tk.W, padx=10, pady=4)
        self.drawbutton.bind("<ButtonRelease-1>", self.draw_button)
        self.discardbutton = tk.Button(self.controlframe, height = 1, width = 5, text="Discard Pile")
        self.discardbutton.grid(row= 1, column=2, sticky=tk.W, padx=10, pady=4)
        self.discardbutton.bind("<ButtonRelease-1>", self.discard_button)

    def discard_card_in_hand(self, event):
        """
        Called when a player clicks on a button corresponding to a card in their hand that they want to replace
        :return: None
        """
        card_to_discard = (int(event.widget['text']))
        position = self.game.list_of_players[self.game.active_player-1].hand.find(card_to_discard)
        event.widget['text'] = self.drawncard
        self.game.discard_pile.add_to_end(card_to_discard)
        self.game.list_of_players[self.game.active_player-1].hand.replace(position,self.drawncard)
        self.iteratenextplayer()
        self.updatecards()

    def discard_button(self, event):
        """
        Called when a player presses the discard button, picking up the card on top of the discard pile
        :return: None
        """
        self.drawncard = self.game.discard_pile.remove_top_card()
        messagebox._show("RACKO", "You drew the " + str(self.drawncard) + "!")
        self.activatebuttons(self.game.active_player -1)
        self.deactivatecontrols()

    def draw_button(self, event):
        """
        Called when a player wants to draw a new card from the top of the draw pile
        :return: None
        """
        self.drawncard = self.game.draw_pile.remove_top_card()
        messagebox._show("RACKO", "You drew a " + str(self.drawncard) + "!")
        self.activatebuttons(self.game.active_player -1)
        self.deactivatecontrols()

    def loadgame(self):
        """
        Called when the player loads a previous game
        :return: None
        """
        self.game.load_saved_racko_game()

    def savegame(self):
        """
        Called when the player saves the current game
        :return: None
        """
        self.game.save_game()

    def initializeGUI(self, number_of_players):
        """
        Calls updatecards to display the proper labels on buttons
        :return: None
        """
        self.updatecards()

    def restartgame(self):
        """
        Restarts the game class and resets the gui to play the next round
        :return: None
        """
        self.game.reset_for_next_round()
        if (self.game.status == 1):
            messagebox._show("RACKO", "Game is over!")
        self.game.active_player = 1
        self.game.dealer = 0
        for i in range(self.game.number_of_players):
            self.game.list_of_players[i].reset_score()
        self.updatecards()
    def exitgame(self):
        """
        Dialogue to confirm exit, then exit
        :return: None
        """
        if messagebox.askokcancel("Quit", "Confirm quit?"):
            self.window.destroy()

    def updatecards(self):
        """
        Updates the labels on all the gui buttons corresponding to cards
        :return: None
        """
        for i in range(self.game.number_of_players):
            for j in range(10):
                self.buttonslist[i][j]['text'] = (str(self.game.list_of_players[i].hand.get_card_at(j)))
        self.discardbutton['text'] = (str(self.game.discard_pile.get_top_card()))

    def activatebuttons(self, player):
        """
        Activates a specified players' buttons for clicking
        :return: None
        """
        for i in range(10):
            self.buttonslist[player][i].configure(state = 'active')
            self.buttonslist[self.game.active_player - 1][i].bind("<ButtonRelease-1>", self.discard_card_in_hand)

    def deactivatebuttons(self, player):
        """
        Disables a specified players' buttons for clicking
        :return: None
        """
        for i in range(10):
            self.buttonslist[player][i].configure(state = 'disabled')
            self.buttonslist[player][i].unbind("<ButtonRelease-1>")

    def activatecontrols(self):
        """
        Activates the racko controls (draw cards buttons) for clicking
        :return: None
        """
        self.drawbutton.configure(state = 'active')
        self.discardbutton.configure(state = 'active')
        self.drawbutton.bind("<ButtonRelease-1>", self.discard_button)
        self.discardbutton.bind("<ButtonRelease-1>", self.discard_button)

    def deactivatecontrols(self):
        """
        Deactivates the racko controls (draw cards buttons) for clicking
        :return: None
        """
        self.drawbutton.configure(state = 'disabled')
        self.discardbutton.configure(state = 'disabled')
        self.drawbutton.unbind("<ButtonRelease-1>")
        self.discardbutton.unbind("<ButtonRelease-1>")

    def iteratenextplayer(self):
        """
        Called when a player has completed their turn to move on to the next player let them take their turn
        :return: None
        """
        self.activatecontrols()
        self.deactivatebuttons(self.game.active_player - 1)
        self.game.active_player += 1
        if (self.game.active_player > self.game.number_of_players):
            self.game.active_player = 1


def main():
    # Create the GUI program
    program = RackoGUI()

    # Start the GUI event loop
    program.window.mainloop()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()