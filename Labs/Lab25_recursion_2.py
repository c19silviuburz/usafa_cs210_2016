#!/usr/bin/env python

"""
Lab25 - Learn more about recursion in problem solving.
"""
# =====================================================================

# Metadata
__author__ = "Silviu Burz"

# ----------------------------------------------------------------------
# Problem 1
# The textbook lesson describes a recursive function that will convert
# any integer number into an equivalent series of digits in a different base
# and return a string containing the appropriate digits. Discuss this
# function with a fellow cadet and if you can't figure out how it works,
# ask your instructor for an explanation.
#
# You can run the test cases in the main() function to see the results
# of this function.
# ----------------------------------------------------------------------


def to_string(n, base):
    convert_string = "0123456789ABCDEF"
    if n < base:   # base case
        return convert_string[n]
    else:
        return to_string(n // base, base) + convert_string[n % base]

# ---------------------------------------------------------------------
# Problem 2
# Write a function that takes a string as a parameter and returns a new
# string that is the reverse of the old string.
#
# Implement two versions of this function using the following hints:
#
# Hint: The recursive relationship could be stated in English like this:
# "The reserve of a string is the last character in the string concatenated
# with the rest of the string reversed."
# or
# "The reverse of a string is the first character tacked onto the end of
# the rest of the string reversed."
# ----------------------------------------------------------------------


def reverse_string(string):
    newstring = ""
    for i in range(len(string)):
        newstring += string[len(string)-i-1]
    return newstring

def reverse_string2(string):
    newstring = ""
    for i in range(len(string)):
        newstring += string[len(string)-i-1]
    return newstring

# ---------------------------------------------------------------------
# Problem 3
# Write a recursive function that takes a string as a parameter and
# returns True if the string is a palindrome, or False otherwise. Remember
# that a string is a palindrome if it is spelled the same both forward
# and backward. For example: "radar" is a palindrome.
#
# Hint: The recursive relationship could be stated in English like this:
# If the first and last characters in a string are equal, and the middle
# characters are a palindrome, then the string is a palindrome.
# ----------------------------------------------------------------------


def is_palindrome(string):
    for i in range(int(len(string)/2)):
        if (string[i] != string[len(string)-1-i]):
            return False
    return True

# ---------------------------------------------------------------------
# Problem 4
# Let's say that a "pair" in a string is two instances of a char separated
# by a char. Therefore, in "AxA" the A's make a pair. Pair's can overlap,
# so "AxAxA" contains 3 pairs -- 2 for A and 1 for x. Recursively compute
# the number of pairs in a given string.
# (This problem comes from http://codingbat.com/prob/p154048)
# ---------------------------------------------------------------------


def count_pairs(string):
    pairs = 0
    for i in range(len(string)-2):
        if (string[i]==string[i+2]):
            pairs += 1
    return pairs

# ---------------------------------------------------------------------
# Please spend any remaining time working on PEX 3.
# ----------------------------------------------------------------------


def main():
    print("Call your functions to verify their correctness.")


    #print(reverse_string("asdfg"))

    #print(is_palindrome(("raccar")))




    # ------------------------------------------------------------------
    # Problem 1 tests:
    # n = 54
    # for base in range(2, 17):
    #     print("{} in base {} is equal to {} ".format(n, base, to_string(n, base)))

    # ------------------------------------------------------------------
    # Problem 2 tests:
    # test_cases = ['abc', 'Now is the time', 'Watch out!']
    # for test in test_cases:
    #     print("'{:s}' reversed is '{:s}'".format(test, reverse_string2(test)))

    # ------------------------------------------------------------------
    # Problem 3 tests:
    # test_cases = ['radar', 'abcdcbb', 'a', "aba", "cc", "weird"]
    # for test in test_cases:
    #     if is_palindrome(test):
    #         print("{} is a palindrome".format(test))
    #     else:
    #         print("{} is not a palindrome".format(test))

    # ------------------------------------------------------------------
    # Problem 4 tests:
    # test_cases = ["a", "axa", "aaaa", "axaxa", "abcdef"]
    # for test in test_cases:
    #     print("{} has {} pairs".format(test, count_pairs(test)))

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
