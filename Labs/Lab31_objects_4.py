#!/usr/bin/env python

"""
Lab31 - Learn more about how to create and use objects.
"""
# =====================================================================

# Metadata
__author__ = "Silviu Burz"

# ---------------------------------------------------------------------
# Problem 1:
# With a fellow cadet, discuss an implementation of a Fraction Class
# that implements the problems you worked on in the previous lab.
# If anything is confusing, discuss it with your instructor.
#
# Note that the Fraction class includes the addition method discussed
# in the textbook reading. What is the difference between the add() and
# the __add__() methods?
# ---------------------------------------------------------------------


def gcd(m, n):
    """
    Find the greatest common divisor of two numbers: m and n
    :param m: an integer
    :param n: an integer
    :return: The greatest common divisor of m and n
    """
    while m % n != 0:
        m, n = n, m % n

    return n

class Fraction:
    """
    Store and manipulate a fraction
    """
    def __init__(self, top, bottom):
        self.num = top        # the numerator is on top
        self.den = bottom     # the denominator is on the bottom

    def __str__(self):
        if (self.num == 0):
            return str(0)
        else:
            self.simplify()
            return str(self.num) + "/" + str(self.den)

    def simplify(self):
        common = gcd(self.num, self.den)

        self.num = self.num // common
        self.den = self.den // common

    def __eq__(self, other_fraction):
        self.simplify()
        other_fraction.simplify()
        return self.num == other_fraction.num and \
               self.den == other_fraction.den

    def __ne__(self, other_fraction):
        self.simplify()
        other_fraction.simplify()
        return self.num != other_fraction.num or \
               self.den != other_fraction.den

    def __lt__(self, other_fraction):
        # A common denominator is required, return which numerator is then less
        return self.num * other_fraction.den < \
               other_fraction.num * self.den

    def __le__(self, other_fraction):
        # A common denominator is required, return which numerator is then less than or equal to
        return self.num * other_fraction.den <= \
               other_fraction.num * self.den

    def __gt__(self, other_fraction):
        # A common denominator is required, return which numerator is then greater
        return self.num * other_fraction.den > \
               other_fraction.num * self.den

    def __ge__(self, other_fraction):
        # A common denominator is required, return which numerator is then greater than or equal to
        return self.num * other_fraction.den >= \
               other_fraction.num * self.den

    def add(self, otherfraction):

        newnum = self.num*otherfraction.den + self.den*otherfraction.num
        newden = self.den * otherfraction.den

        result = Fraction(newnum, newden)
        result.simplify()

        return result

    def __add__(self, otherfraction):
        if (type(otherfraction == int)):
            newnum = (otherfraction*self.den)
            newnum += self.num
            result = Fraction(newnum, self.den)
            return result
        else:
            newnum = self.num * otherfraction.den + self.den * otherfraction.num
            newden = self.den * otherfraction.den

            result = Fraction(newnum, newden)
            result.simplify()

        return result

    def __sub__(self, otherfraction):
        newnum = self.num * otherfraction.den - self.den * otherfraction.num
        newden = self.den * otherfraction.den

        result = Fraction(newnum, newden)
        result.simplify()

        return result

    def __mul__(self, otherfraction):
        newnum = self.num * otherfraction.num
        newden = self.den * otherfraction.den

        result = Fraction(newnum, newden)
        result.simplify()

        return result

    def __truediv__(self, otherfraction):
        newnum = self.num * otherfraction.den
        newden = self.den * otherfraction.num

        result = Fraction(newnum, newden)
        result.simplify()

        return result

    def __floordiv__(self, otherfraction):
        newnum = self.num * otherfraction.den
        newden = self.den * otherfraction.num

        newnum -= newnum % newden

        result = Fraction(newnum, newden)
        result.simplify()

        return result

    def __mod__(self, otherfraction):
        newnum = self.num * otherfraction.den
        newden = self.den * otherfraction.num

        newnum -= newnum % newden

        result = Fraction(newnum, newden)
        result.simplify()

        return result

    def __pow__(self, otherfraction):
        newnum = (self.num ** (otherfraction.num/otherfraction.den))
        newden = (self.den ** (otherfraction.num/otherfraction.den))

        result = Fraction(newnum, newden)
        result.simplify()

        return result

def problem1():
    f1 = Fraction(1, 2)
    f2 = Fraction(1, 4)

    f3 = f1 + f2  # calls the __add__ method of f1
    print("f3 = ", f3)

    f4 = f1.add(f2)
    print("f4 = ", f4)


# ---------------------------------------------------------------------
# Problem 2:
# You can make all of the arithmetic operators perform correct
# calculations on objects of the Fraction class by overwriting the
# following pre-defined methods.
#
# Operator  Expression	  Internally
#    +      a + b         a.__add__(b)
#    -      a - b         a.__sub__(b)
#    *      a * b         a.__mul__(b)
#    /      a / b         a.__truediv__(b)
#    //     a // b        a.__floordiv__(b)
#    %      a % b         a.__mod__(b)
#    **     a ** b        a.__pow__(b)
#
# You can find a complete list of methods that are associated with
# Python operators here:
#     https://docs.python.org/3/reference/datamodel.html#emulating-numeric-types
#
# Add a couple of the arithmetic methods shown above to the Fraction
# class above.
#
# In the problem2() function below, create come Fraction objects and
# experiment with the operators you implemented. Notice that if you try
# any of the operators you have not implemented, Python will crash with
# an error.
# =====================================================================


def problem2():
    a = Fraction(4,7)
    b = Fraction(6, 4)
    print(a**b)

# ---------------------------------------------------------------------
# Problem 3:
# In Problem 3 below experiment with what happens if you try to add
# a Fraction object to an integer. For example, if you add 1/2 to 5
# you should get 11/2. Did you try it? Did it crash? Why did it crash?
# Can you fix the __add__ function in the Fraction class so that it
# works with integers and Fractions? Try to.
#
#  Test your modifications in the problem3() function below.
# ---------------------------------------------------------------------


def problem3():
    a = Fraction(4,5)
    a = a + 5
    print(a)

# ---------------------------------------------------------------------
# Please spend the remainder of your lab time working on PEX 4.
# ---------------------------------------------------------------------


def main():
    #problem1()
    problem2()
    #problem3()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
