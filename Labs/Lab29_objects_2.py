#!/usr/bin/env python

"""
Lab29 - Learn more about how to create and use objects.
"""
# =====================================================================

import math

# Metadata
__author__ = "Silviu Burz"

# ---------------------------------------------------------------------


class Point:
    """ Point class for representing and manipulating x,y coordinates. """

    def __init__(self, init_x, init_y, initcolor):
        """ Create a new point at the given coordinates. """
        self.x = init_x
        self.y = init_y
        self.color = initcolor

    def __str__(self):
        return str("Point")

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def distance_from_origin(self):
        return ((self.x ** 2) + (self.y ** 2)) ** 0.5

    def set_x(self, new_x):
        self.x = new_x

    def set_y(self, new_y):
        self.y = new_y

    def is_at_origin(self):
        return self.x == 0 and self.y == 0

    def move(self, delta_x, delta_y):
        self.x += delta_x
        self.y += delta_y

    def get_color(self):
        return self.color

    def set_color(self, colortoset):
        self.color = colortoset

    def mid_point(self, another_point):
        newx = (self.x-another_point.x)/2
        newy = (self.y-another_point.y)/2
        return Point(newx, newy, self.color)

# ---------------------------------------------------------------------


class Circle:
    """ Define a circle. """

    def __init__(self, point, radius, initcolor):
        self.center = point
        self.radius = radius
        self.color = initcolor

    def __str__(self):
        return str("Circle")

    def radius(self):
        return self.radius

    def area(self):
        return math.pi * self.radius ** 2

    def move(self, delta_x, delta_y):
        self.center.move(delta_x, delta_y)

    def getcircumference(self):
        return 2*math.pi*self.radius

    def getarea(self):
        return math.pi*self.radius**2

    def copy(self):
        return Circle(self.center, self.radius, self.color)

# =====================================================================
# PLEASE NOTE:
# The structure of this lab assignment is different from previous labs.
# The problems you are assigned to implement are described below, but you
# will put your "answers" either in the class definitions above, or the
# main() function below.
# =====================================================================
# Problem 1:
# Study the above class definitions for a Point and Circle class. (These
# are example solutions to the lab 27 problems.) Discuss the solutions
# with a fellow cadet. If you have any questions, please ask your
# instructor for assistance.
#
#  ---------------------------------------------------------------------
# Problem 2:
# Discussion:
# Python "classes" have special methods for common tasks.
#   * The special methods always start and end with two underscores (__).
#   * When a class is defined, Python always adds its special methods to
#     the definition. You can't change this! This is just how Python
#     works!
#   * One method that is always added to a class definition is the
#     __str__(self) method that converts the object to a string. The
#     default __str__() method is typically not very useful.
#   * You can override the default __str__() method by defining your
#     own __str__() method in your class. In this way you can always
#     determine exactly how an object is converted to a string.
#
# Task:
# Implement a __str(self) function for both the Point and Circle classes.
# Note that a __str__(self) method always returns a string.
# Test your work by creating objects (instances of a class) in main()
# and then printing out the objects.
#
# ---------------------------------------------------------------------
# Problem 3:
# Modify the __str(self) methods you implemented for problem 2 to make the
# objects print out differently. Be creative!
#
# Test your work by creating objects (instances of a class) in main()
# and then printing out the objects.
#
#  ---------------------------------------------------------------------
# Problem 4:
# Add a color value to both the Point and Circle class' internal data.
# That is, a Point should be defined by 3 values: x, y, color.
# A Circle should be defined by 3 values: a point, a radius, and a color.
# Modify any methods of the classes that might be effected by this change.
#
# Note: If you give a parameter a default value, you are not required
#       to send a value when you call the function.
#
# Test your work by creating objects (instances of a class) in main()
# and then calling appropriate methods on the objects.
#
#  ---------------------------------------------------------------------
# Problem 5:
# Add at least one new method to both class definitions. Here are a few
# possible suggestions:
# For the Point class: get_color, set_color, which_quadrant, angle,
#                      distance_from(another_point)
# For the Circle class: circumference, shrink(percent), point_inside(a_point)
#
# Test your work by creating objects (instances of a class) in main()
# and then calling appropriate methods on the objects.
#
#  ---------------------------------------------------------------------
# Problem 6:
# The purpose of this problem is to implement a class method that returns
# an object as its return value, which is a common occurrence. For example,
# all string methods return new objects because strings are immutable.
#
# Implement a new method for the Point class that calculates the midpoint
# between itself and another point and returns the midpoint as a new
# Point object. The method definition should look like this:
#     def mid_point(self, another_point):
#       ...
#       return mid_point
#
# Implement a new method for the Circle class that creates an exact copy
# of a Circle object without changing the original in any way. The
# method definition should look like this:
#     def copy(self):
#       ...
#       return new_circle
#
# Test your work by creating objects (instances of a class) in main()
# and then calling appropriate methods on the objects.
#
# ---------------------------------------------------------------------

# =====================================================================


def main():
    """
    Use a Point and Circle class to create Point and Circle objects
    and then manipulate those objects.
    """

    p = Point(7, 6, "red")
    print("p =", p)

    q = Point(3, 5, "red")
    print("q =", q)

    r = Point(7, 6, "red")
    print("r =", r)

    s = Circle(r, 5, "red")
    print("s =", s)

    print(s.__str__())

    t = r.mid_point(q)
    print(t.x)

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
