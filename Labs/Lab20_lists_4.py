#!/usr/bin/env python

"""
Lab20 - Learn more about lists and list manipulation.
        Compare and contrast lists to tuples.
"""
# =====================================================================

from math import pi, sqrt, ceil

# Metadata
__author__ = "Silviu Burz"

# ---------------------------------------------------------------------
# Problems 1-3
# Write an equivalent function to the functions below that create and
# return a list of values using list comprehensions.
# The first problem has been done for you as an example.
# There are corresponding test cases in main().
#
# Note that the following are equivalent:
#
#  new_list = [ expression for item in sequence if condition]
 #
# After you have code that works, discuss your solutions with a fellow cadet.
# ----------------------------------------------------------------------


def problem1():
    a_list = []
    for num in range(10, 20):
        if num % 2 == 0:
            a_list.append(num ** 2)

    return a_list


def problem1a():
    return [num ** 2 for num in range(10, 20) if num % 2 == 0]
# ----------------------------------------------------------------------


def problem2():
    circle_areas = []
    for radius in range(3, 12):
        if pi * radius ** 2 < 100:
            circle_areas.append(pi * radius ** 2)

    return circle_areas


def problem2a():
    return [pi * radius ** 2 for radius in range(3,12) if pi * radius ** 2 < 100]
# ----------------------------------------------------------------------


def is_prime(n):
    for divisor in range(2, ceil(sqrt(n))):
        if n % divisor == 0:
            return False

    return True


def problem3():
    primes_list = []
    for value in range(3, 101):
        if is_prime(value):
            primes_list.append(value)

    return primes_list


def problem3a():
    return [value for value in range(3,101) if is_prime(value)]
# ----------------------------------------------------------------------


def problem4():
    multiples_of_3 = []
    for value in range(0, 101, 3):
        multiples_of_3.append(value)

    return multiples_of_3


def problem4a():
    # Note: the "if clause" of a list comprehension is optional
    return [value for value in range(0,101,3)]
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# Problem 5
# Write a function that takes a sentence, splits it into individual words,
# removes any words that start with a vowel, and then re-joins the words
# into a single string where the words are separated by a dash instead of
# a space. Use the string split() and join() methods to accomplish this.
# ----------------------------------------------------------------------


def convert(sentence):
    sentence = sentence.split(' ')
    return [word for word in sentence if word[0] not in "a,e,i,o,u"]


# ----------------------------------------------------------------------
# Problem 6
# Question: Why are all functions that receive only tuples as
#           input arguments "pure functions"? Be as specific as you can.
#
# Answer:
#       Because tuples are not mutable. They do not change once they are created.
#
#
#
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# Problem 7
# Write a function that receives a tuple and returns the average, median,
# and standard deviation of the values in the tuple.
# Hints:
# 1) The average is the sum of the values divided by the number of values.
# 2) The standard deviation is found by taking the square root of the
#    average of the squared deviations of the values from their average value.
# 3) The median is found by sorting the values and selecting the middle
#    element. (Use the sorted() function.)
# ----------------------------------------------------------------------


def problem7(a_tuple):
    pass
# ----------------------------------------------------------------------


def main():
    print("Call your functions to verify their correctness.")

    print(convert("this is just a test."))

    # fred = problem1()
    # mary = problem1a()
    # if fred != mary:
    #     print("Problem 1 failed.")
    #     print(fred)
    #     print(mary)
    #
    # fred = problem2()
    # mary = problem2a()
    # if fred != mary:
    #     print("Problem 2 failed.")
    #     print(fred)
    #     print(mary)
    #
    # fred = problem3()
    # mary = problem3a()
    # if fred != mary:
    #     print("Problem 3 failed.")
    #     print(fred)
    #     print(mary)

    # fred = problem4()
    # mary = problem4a()
    # if fred != mary:
    #     print("Problem 4 failed.")
    #     print(fred)
    #     print(mary)

    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Tests for problem 5:
    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # test = "This is a test of the emergency broadcast system"
    # converted_test = convert(test)
    # if converted_test != "This-test-the-broadcast-system":
    #     print("convert function failed. The incorrect result was", converted_test)

    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Tests for problem 7:
    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Create a list of random integers between 0 and 100
    # values = (37, 12, 56, 23, 59, 86, 19, 8, 46)
    # print(values)
    # a, b, c = problem7(values)
    # # Expected results: average = 38.44 median = 37 standard deviation = 2.5185
    # print("average = {:.2f} median = {:d} standard deviation = {:.4f}"
    #       .format(a, b, c))
    # # Or
    # values = problem7(values)
    # print("average = {:.2f} median = {:d} standard deviation = {:.4f}"
    #       .format(values[0], values[1], values[2]))

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
