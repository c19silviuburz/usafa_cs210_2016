#!/usr/bin/env python

"""
Lab26 - Learn more about recursion in problem solving.

The following code will be used to build a solution to the
"Towers of Hanoi" problem.
"""
# =====================================================================

from turtle import TurtleScreen, RawTurtle, TK
import time

# Metadata
__author__ = "First Last"

# Disk definition, tuple: (COLOR, LENGTH)
COLOR = 0
LENGTH = 1
TURTLE = 2  # Added if the disks are animated

# Pole definition, tuple: (NAME, POSITION, DISKS)
NAME = 0
POSITION = 1
DISKS = 2

# Turtle Window Constants
CANVAS_WIDTH = 600
CANVAS_HEIGHT = int(CANVAS_WIDTH * 9 / 16)  # Produces the eye-pleasing 16:9 HD aspect ratio.
DISK_HEIGHT = 21

# Global variable to recognize when graphics need to be initialized
turtles_for_disks_exist = False


def initialize_graphics():
    # Create a Tkinter graphics window
    graphics_window = TK.Tk()
    graphics_window.title("Lab 5 Problems")

    # Create a "canvas" inside the graphics window to draw on
    my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
    my_canvas.pack()  # Organizes my_canvas inside the graphics window

    # Create a "canvas" made specifically for turtle graphics
    turtle_canvas = TurtleScreen(my_canvas)
    turtle_canvas.bgcolor("white")

    # Create a turtle to draw on the canvas
    t = RawTurtle(turtle_canvas)

    turtle_canvas.delay(0)
    t.hideturtle()
    t.speed("fastest")

    return t, turtle_canvas


def draw_rectangle(t, width, height, color):
    # Get to lower-left corner
    t.up()
    t.backward(width // 2)
    t.color(color)
    t.down()

    # Draw rectangle
    t.begin_fill()
    for side in range(2):
        t.forward(width)
        t.left(90)
        t.forward(height)
        t.left(90)
    t.end_fill()

    # Get back to starting point
    t.up()
    t.forward(width // 2)
    t.down()


def draw_pole(t, pole):
    t.up()
    t.goto(pole[POSITION][0], pole[POSITION][1])
    t.setheading(0)
    draw_rectangle(t, 6, 200, "black")


def animate_disk_move(from_pole, to_pole, extra_pole):
    global turtles_for_disks_exist

    # Initial setup on first call
    if not turtles_for_disks_exist:
        t, turtle_canvas = initialize_graphics()
        # Create a turtle for each disk. This assumes that all
        # the disks are initially on the "from_pole".
        n = 0
        for disk in from_pole[DISKS]:
            disk_turtle = RawTurtle(turtle_canvas)
            disk_turtle.up()
            disk_turtle.pensize(1)
            disk_turtle.shape("square")
            disk_turtle.resizemode('user')
            disk_turtle.turtlesize(1, disk[LENGTH], 1)
            disk_turtle.color(disk[COLOR])
            disk_turtle.showturtle()
            disk_turtle.setposition(from_pole[POSITION][0],
                                    from_pole[POSITION][1] + n * DISK_HEIGHT)
            n += 1
            disk.append(disk_turtle)

        # Draw the poles
        draw_pole(t, from_pole)
        draw_pole(t, to_pole)
        draw_pole(t, extra_pole)

        turtles_for_disks_exist = True

    # Get the top disk on the from_pole
    top_disk = from_pole[DISKS][-1]
    disk_turtle = top_disk[TURTLE]
    position = disk_turtle.position()
    x = position[0]
    y = position[1]
    frame_rate = 0.015

    # Animate the top disk on the "from_pole" moving to the top of its pole
    while y < 100:
        y += 4
        disk_turtle.setposition(x, y)
        time.sleep(frame_rate)

    # Animate the disk moving over to the "to_pole"
    if from_pole[POSITION][0] < to_pole[POSITION][0]:
        while x < to_pole[POSITION][0]:
            x += 4
            disk_turtle.setposition(x, y)
            time.sleep(frame_rate)
    else:
        while x > to_pole[POSITION][0]:
            x -= 4
            disk_turtle.setposition(x, y)
            time.sleep(frame_rate)

    # Animate the disk moving down on its "to_pole"
    limit = to_pole[POSITION][1] + len(to_pole[DISKS]) * DISK_HEIGHT
    while y > limit:
        y -= 4
        disk_turtle.setposition(x, y)
        time.sleep(frame_rate)
# =====================================================================


def towers_of_hanoi(num_disks_to_move, from_pole, to_pole, extra_pole):
    if num_disks_to_move == 1:
        animate_disk_move(from_pole, to_pole, extra_pole)
        disk = from_pole[DISKS].pop()
        to_pole[DISKS].append(disk)
        print("move ", disk[COLOR], " disk from ", from_pole[NAME], " to ", to_pole[NAME])
    else:
        towers_of_hanoi(num_disks_to_move-1, from_pole, extra_pole, to_pole)
        towers_of_hanoi(1,from_pole,to_pole,extra_pole)
        towers_of_hanoi(num_disks_to_move-1,extra_pole,to_pole,from_pole)

def main():
    """
    A solution to the "Towers of Hanoi" Problem.
    """

    # The disks are defined by their color and width, ordered from bottom to top
    #disks = [["orange", 3], ["yellow", 2], ["green", 1]]
    disks = [["red", 6], ["darkorange", 5], ["pink", 4],
             ["orange", 3], ["yellow", 2], ["green", 1]]

    # A pole is defined by its name, location (x,y), and the disks on it
    a = ("pole A", (-200,-100), disks)
    b = ("pole B", (   0,-100), [])
    c = ("pole C", ( 200,-100), [])

    towers_of_hanoi(len(disks), a, b, c)

    #TK.mainloop()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
