#!/usr/bin/env python

"""
Lab13 - Learn basic string manipulation.

        All of the problems in this lab exercise are formatted in the
        same way as GR I. That is, you will write a function to solve
        a specific problem. You will test the function by calling it
        from a main() function.

        Note: You can split the editor window vertically to see
        function code and the main code at the same time. ("Right-click
        on the editor tab and select "Split Horizontally".)
"""
# =====================================================================

# # Metadata
__author__ = "First Last"

# ----------------------------------------------------------------------
# Problem 1
# 1) Predict what the following statements will print and write down
#    your answers on a separate piece of paper.
# 2) Compare your written answers to a fellow cadet's answers and
#    resolve any differences of opinion.
# 3) Execute the function by calling it from main().
# 4) Verify that you understand each result. Discuss any wrong answers
#    with a fellow cadet or your instructor.
# ----------------------------------------------------------------------


def problem_1():
    print('Python'[1], type('Python'[1]))
    print("Strings are sequences of characters."[5],
          type("Strings are sequences of characters."[5]))
    print(len("wonderful"), type(len("wonderful")))
    print('p' in 'Pineapple', type('p' in 'Pineapple'))
    print('apple' in 'Pineapple', type('apple' in 'Pineapple'))
    print('pear' not in 'Pineapple', type('pear' not in 'Pineapple'))
    print('apple' > 'pineapple', type('apple' > 'pineapple'))
    print('pineapple' < 'Peach', type('pineapple' < 'Peach'))

# ----------------------------------------------------------------------
# Problem 2
# In Robert McCloskey’s book "Make Way for Ducklings", the names of the
# ducklings are Jack, Kack, Lack, Mack, Nack, Ouack, Pack, and Quack.
# The code in the problem_2() function below tries to output these names
# in order. Of course, that’s not quite right because Ouack and Quack
# are misspelled. Can you fix it?
# ----------------------------------------------------------------------


def problem_2():
    prefixes = "JKLMNOPQ"
    suffix = "ack"

    for p in prefixes:
        print(p + suffix)

# ----------------------------------------------------------------------
# Problem 3
# Implement a function called analyze_text that receives one input parameter,
# a single string. Then count the number of alphabetic characters
# (a through z, or A through Z) in the string of text and keep track
# of how many are the letter ‘e’. Your function should print an analysis
# of the text something like this:
#
#   Your text contains 243 alphabetic characters, of which 109 (44.8%) are 'e'.
#
# Hint: Use a loop to test each individual character in the the string.
#       Use the string function "isalpha()" on each character to determine
#       if it is an alphabetic character.
#
# In the main() function below create a triple-quoted string that
# contains a paragraph of text - perhaps a poem, a speech,
# instructions to bake a cake, some inspirational verses, etc. Then call
# your function with your string as the input parameter.
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 4
# Write a function called num_digits_in_integer() that will return the
# number of digits in an integer. It should receive one parameter, a
# single integer.
#
# Hint: You can change an integer into a string using the str() function.
#
# Call your function multiple times from the main() function to verify
# its correctness.
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 5
# Write a function called reverse_string() that reverses and returns its
# string argument. For example, if the function received "Wayne",
# it would return "enyaW"
#
# Hint: Start with an empty string and add each character from the
#       original string in reverse order to the new string. You can
#       use positive indexes or negative indexes.
#
# Call your function multiple times from the main() function to verify
# its correctness.
#  ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 6
# Write a function called mirror() that mirrors its string argument.
# For example, the mirror of "abcd" would be "abcddcba". Use the
# reverse_string() function you wrote in problem 5 to accomplish this task.
#
# Call your function multiple times from the main() function to verify
# its correctness.
#  ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 7
# Write a function called is_palindrome that recognizes palindromes.
# A palindrome is a string that is same whether is it read from left-to-
# right or right-to-left. A famous palindrome is
#      "A Man, A Plan, A Canal-Panama!"
# Use your "string_reverse" function from a previous problems to help
# solve this problem.
#
# Hint: Palindromes ignore capitalization, so convert your string to all
#       lower case. And palindromes skip all non-alphabetic characters,
#       so remove them from the original string. (Use a loop to build
#       a new string that contains only alphabetic characters.)
#
# Call your function multiple times from the main() function to verify
# its correctness.
#  ----------------------------------------------------------------------


# ----------------------------------------------------------------------


def main():
    # problem_1()
    # problem_2()
    my_string = """
Put a string of your choice here.
    """
    # analyze_text(my_string)

    # for n in range(0, 10001, 67):
    #     print(n, "has", num_digits_in_integer(n), "digits")

    # print(reverse_string("Wayne"))
    # print(reverse_string("Now is the time for all good men"))

    # print(mirror("Wayne"))
    # print(mirror("Now is the time for all good men"))

    # text = "A Man, A Plan, A Canal-Panama!"
    # print(text, " is a plaindrome?", is_palindrome(text))
    #
    # text = "Eva, Can I Stab Bats In A Cave?"
    # print(text, " is a plaindrome?", is_palindrome(text))
    #
    # text = "This is a test"
    # print(text, " is a plaindrome?", is_palindrome(text))

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
