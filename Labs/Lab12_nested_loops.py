#!/usr/bin/env python

"""
Lab12 - Learn how to create "nested loops" - loops inside other loops.

        All of the problems in this lab exercise are formatted in the
        same way as GR I. That is, you will write a function to solve
        a specific problem. You will test the function by calling it
        from a main() function.

        Note: You can split the editor window vertically to see
        function code and the main code at the same time. ("Right-click
        on the editor tab and select "Split Vertically".)
"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules

# # Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "Sep 14, 2016"

# ----------------------------------------------------------------------
# Problem 1
# Write a function that prints a multiplication table for the values
# between zero and ten. For an example, see:
#      http://www.prasinipriza.com/A/P/AA310040/30/MULTIPLICATION-TABLE.html
# Each entry in the table (except the column and row labels) must be
# calculated.
#
# Note the following:
#  1) Print a tab, '\t' between each value so that the numbers in
#     each column will line up vertically.
#  2) A normal print() command will print an entire line and cause
#     the print cursor to move to the next line. If you want the print
#     cursor to stay on the current line, include an "end" parameter
#     that is equal to an empty string, like this: end="". E.g.,
#         print(alpha, end="")
#     will print the value of alpha and leave the print cursor on
#     the same line as alpha.
# ----------------------------------------------------------------------

def printmultitables():
    for i in range(10):
        for j in range(10):
            print(i, "x", j ," = " ,i*j)

# ----------------------------------------------------------------------
# Problem 2
# Write a function that receives a list of integer values and prints a
# bar chart that visually represents the values in the list. For example,
# the list [ 42, 25, 75, 13, 37, 67, 23 ] would create output like this:
#
#   42 | ******************************************
#   25 | *************************
#   75 | ***************************************************************************
#   13 | *************
#   37 | *************************************
#   67 | *******************************************************************
#   88 | ***********************
#
# Note: As explained in the problem 1, use the "end" parameter to keep
#       the print cursor on the same line, when appropriate. For example,
#           print( "*", end="")
#       will print a single asterisk and leave the print cursor on the same line.
# ----------------------------------------------------------------------

def printvisualrep(list):
    for i in list:
        print(i, " | ",end="")
        for j in range(i):
            print("*", end="")
        print(" ")

# ----------------------------------------------------------------------
# Problem 3
# Setup: You will be performing image manipulation for the remaining
#        problems in this lab. You must do the following first:
#
#        1) Save the cImage.py file on the lab assignment page on Moodle
#           to your repository Labs folder.
#        2) Right-click on your Labs folder and select "Mark Directory As"
#           and then "Sources Root". This makes PyCharm look in the current
#           folder for import files.
#        3) Save the file us-air-force-academy-logo.jpg on the lab
#           assignment page on Moodle to your repository Labs folder.
#           This will be the image you manipulate.
#        4) Install a new image library called Pillow.
#             a) Open a CMD window.
#             b) Execute "easy_install Pillow"
#
# Study the following example program from the textbook, execute it,
# discuss it with a fellow cadet, and attempt to modify it. If you have
# any problems, please get help from your instructor.
# ----------------------------------------------------------------------


def example_image_manipulation_from_textbook():
    """
    Open and display an image, then create and display its "negative"
    :return: None
    """
    import cImage as image

    my_image = image.Image("us-air-force-academy-logo.jpg")
    my_window = image.ImageWin(my_image.width, my_image.height)
    my_image.draw(my_window)

    for row in range(my_image.height):
        for col in range(my_image.width):
            p = my_image.getPixel(col, row)

            new_red = 255 - p.getRed()
            new_green = 255 - p.getGreen()
            new_blue = 255 - p.getBlue()

            new_pixel = image.Pixel(new_red, new_green, new_blue)

            my_image.setPixel(col, row, new_pixel)

    my_image.draw(my_window)
    my_window.exitonclick()

# ----------------------------------------------------------------------
# Problem 4
# Make a copy of the example_image_manipulation_from_textbook()
# function and paste it below. Rename the copy to convert_to_gray_scale().
# Then modify the code to convert the color image to a gray-scale
# image where every pixel is a shade of gray. This means that each
# component of each pixel is the same intensity, such as (0.5, 0.5, 0.5)
# or (0.61, 0.61, 0.61). You can calculate the intensity of each pixel
# using a percentage of each color component, like this:
#    intensity = int( 0.299 * red + 0.587 * green + 0.114 * blue )
# (This is the formula used to convert color TV to black-and-white TV.)
# ----------------------------------------------------------------------


def convert_to_gray_scale():
    import cImage as image

    my_image = image.Image("us-air-force-academy-logo.jpg")
    my_window = image.ImageWin(my_image.width, my_image.height)
    my_image.draw(my_window)

    for row in range(my_image.height):
        for col in range(my_image.width):
            p = my_image.getPixel(col, row)
            intensity = int(0.299 * p.getRed() + 0.587 * p.getGreen() + 0.114 * p.getBlue())
            new_red = intensity
            new_green = intensity
            new_blue = intensity

            new_pixel = image.Pixel(new_red, new_green, new_blue)

            my_image.setPixel(col, row, new_pixel)

    my_image.draw(my_window)
    my_window.exitonclick()

# ----------------------------------------------------------------------
# Problem 5
# Create another copy of the image manipulation function
# example_image_manipulation_from_textbook() and call this function
# double_image_size(). This new function should make a copy of the USAFA
# logo image that is double the original image size and display it.
#  ----------------------------------------------------------------------


def double_image_size():
    import cImage as image

    my_image = image.Image("us-air-force-academy-logo.jpg")
    my_window = image.ImageWin(my_image.width, my_image.height)
    my_image.draw(my_window)

    for row in range(my_image.height):
        for col in range(my_image.width):
            p = my_image.getPixel(col, row)
            intensity = int(0.299 * p.getRed() + 0.587 * p.getGreen() + 0.114 * p.getBlue())
            new_red = intensity
            new_green = intensity
            new_blue = intensity

            new_pixel = image.Pixel(new_red, new_green, new_blue)

            my_image.setPixel(col, row, new_pixel)

    my_image.draw(my_window)
    my_window.exitonclick()

# ----------------------------------------------------------------------
# Challenge Problem 1
# Research how to calculate a Sobel Gradient image and create a function
# that creates a Sobel Gradient image. Note that such an image finds the
# edges in an image. You need to work with intensity values, not RGB
# values, so convert your image to gray-scale before performing the Sobel
# gradient calculations. And the result of your calculations must be
# restricted to an integer in the range [0, 255] for each color component.
#  ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def main():
    # print_multiplication_table()
    #print_bar_chart([42, 25, 75, 13, 37, 67, 23])
    #  example_image_manipulation_from_textbook()
    # convert_to_gray_scale()
    # double_image_size()
    # sobel_gradient_image()
    #printmultitables()
    #printvisualrep({12,41,51,53})
    #example_image_manipulation_from_textbook()
    convert_to_gray_scale()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()

