#!/usr/bin/env python

"""
Lab19 - Learn more about lists and list manipulation.
"""
# =====================================================================

import turtle

# Metadata
__author__ = "Silviu Burz"

# ---------------------------------------------------------------------
# Problem 1
# Write a function that creates a list called my_list with contains the
# following six items: 76, 92.3, “hello”, True, 4, and 76.
# Create the list by starting with an empty list and adding one item at
# a time. Do this twice, once using append and then using concatenation.
#
# Discuss with a fellow cadet which method is preferred and why.
# ----------------------------------------------------------------------


def problem1():
    my_list = []
    for i in range(76, 92.3, "hello", True, 4, 76):
        my_list.append(i)

    my_list = []
    for i in range(76, 92.3, "hello", True, 4, 76):
        my_list = my_list + [i]

# ----------------------------------------------------------------------
# Problem 2
# Python provides us with many list methods, some of which are:
#    .count(value)
#    in operator
#    .reverse()
#    .index(value)
#    .insert(position, item)
# It can be very instructive to think about how these functions are implemented.
# Implement the following Python functions that work like the built-in
# functions listed above:
#   count(a_list, value) - return the number of times a value occurs in a_list
#   is_in(a_list, value) - return True if the value is in a_list, False otherwise
#   reverse(a_list) - return a new list with the elements in the reverse order
#                     as a_list
#   index(a_list, value) - return the position of the first occurrence of a value
#                in a_list. If the value is not found, return -1.
#   insert(a_list, value, position) - modify a_list to insert the value at
#                 the position index.
# ----------------------------------------------------------------------


def count(a_list, value):
    count = 0
    for i in range(len(a_list)):
        if (value == a_list[i]):
            count += 1
    return count


def is_in(a_list, value):
    count = False
    for i in range(len(a_list)):
        if (value == a_list[i]):
            count = True
    return count


def reverse(a_list):
    newlist = a_list[:]
    for i in range(len(a_list)):
        newlist[i] = a_list[len(a_list)-i-1]
    return newlist


def index(a_list, value):
    for i in range(len(a_list)):
        if (value == a_list[i]):
            return i
    return -1  # the value was not found


def insert(a_list, value, position):
    newlist = []
    for i in range(position):
        newlist += [a_list[i]]
    newlist += [value]
    for i in range(len(a_list)-position):
        newlist += [a_list[i+position]]
    return newlist

# ----------------------------------------------------------------------
# Problem 3
# Pascal's triangle is a famous configuration of numbers that looks like
# this:
#                     1
#                  1     1
#               1     2     1
#            1     3     3     1
#         1     4     6     4     1
#
# Each value in the "triangle" is the sum of the two numbers above it.
# The first and last value on each row is always a 1.
#
# Write a function that takes one input, the number of rows of a Pascal
# triangle, and prints a Pascal Triangle up to the specified rows. Use
# a list to store the values on the previous row so that each term can
# be calculated from the previous row. Use the string .format function
# to center the numbers appropriately.
# A call to print_pascal_triangle(10) should produce this output:
#                                    1
#                                 1     1
#                              1     2     1
#                           1     3     3     1
#                        1     4     6     4     1
#                     1     5     10    10    5     1
#                  1     6     15    20    15    6     1
#               1     7     21    35    35    21    7     1
#            1     8     28    56    70    56    28    8     1
#         1     9     36    84   126   126    84    36    9     1
#
# Hints:
# 1) Send print() a named parameter, end='', to keep it from automatically
#    going to a new line.
# 2) Print the first row as a special case (not inside your loop).
# 3) You can print a variable number of spaces by repeating a single blank
#    character, such as print("{}".format(' ' * num_spaces)).
# 4) The above examples use a field size of 6 for each number.
# ----------------------------------------------------------------------


def print_pascal_triangle(number_rows):
    num_spaces = 5
    triangle = []
    triangle.extend([0]*number_rows)
    triangle[0][0] = [1]
    print(1)
    for i in range(number_rows-1):
        for j in range(i):
            if (j == 0) or (j == i-1):
                triangle[i+1][j] = [1]
                print("{}".format(1,""*num_spaces))
            else:
                triangle[i+1][j] = triangle[i][j-1] + triangle[i][j]
                print("{}".format(triangle[i+1][j], ""* num_spaces))

# ----------------------------------------------------------------------
# Challenge Problem
# As described in the textbook, an L-system can be modified to use a list
# to remember a turtle's position and orientation and then return to that
# position and orientation at a later time.
# The symbol "[" is a "marker" that stores the current position and orientation.
# The symbol "]" is a "marker" that says "go back to the last saved position
#   and orientation."
#
# The code below is copied from the textbook. Notice that the "draw_L_system"
# function implements these new commands using a list to save and return-to
# turtle locations and orientations.
#
# Study the code to understand how it works.
#
# Now modify the code to implement this L-System:
#   Axiom:  H
#   Rule 1: H --> HFX[+H][-H]
#   Rule 2: X --> X[-FFF][+FFF]FX
#           Use an angle of 25.7 degrees
#
# Here is another L-system to try: (this is the best one!)
#   Axiom:  F
#   Rule 1: F --> F[-F]F[+F]F
#           Use an angle of 25 degrees
#
# ----------------------------------------------------------------------


def create_L_system(number_iterations, axiom):
    pattern = axiom
    for iteration in range(number_iterations):
        pattern = process_string(pattern)

    return pattern


def process_string(original_string):
    new_string = ""
    for letter in original_string:
        new_string += apply_rules(letter)

    print(original_string, "-->", new_string)
    return new_string


def apply_rules(letter):
    if letter == 'X':
        new_string = 'F[-X]+X'  # Rule 1
    elif letter == 'F':
        new_string = 'FF'       # Rule 2
    else:
        new_string = letter     # no rules apply so keep the character

    return new_string


def draw_L_system(a_turtle, instructions, angle, distance):
    saved_turtle_state = []
    for cmd in instructions:
        if cmd == 'F':
            a_turtle.forward(distance)
        elif cmd == 'B':
            a_turtle.backward(distance)
        elif cmd == '+':
            a_turtle.right(angle)
        elif cmd == '-':
            a_turtle.left(angle)
        elif cmd == '[':
            print("Saving turtle state:")
            saved_turtle_state.append([a_turtle.heading(), a_turtle.xcor(), a_turtle.ycor()])
            print(saved_turtle_state)
        elif cmd == ']':
            print("Restoring turtle state:")
            state = saved_turtle_state.pop()
            print(state)
            a_turtle.up()
            a_turtle.setheading(state[0])
            a_turtle.setposition(state[1], state[2])
            a_turtle.down()


def challenge_problem():
    # Create a turtle and a window to draw into
    t = turtle.Turtle()
    wn = turtle.Screen()

    # Move the turtle to the left side of the screen. The pattern will
    # draw from left to right.
    t.up()
    # Backward 100 works if the iterations is 4. If you increase the
    # number of iterations you will need to increase this distance
    # (or decrease the segment length below.)
    t.left(90)
    t.back(100)
    t.down()
    t.speed(9)

    # Create the L-system
    number_of_times_to_apply_rules = 4
    inst = create_L_system(number_of_times_to_apply_rules, "X")

    # Draw the picture from the L-system
    segment_length = 30 / number_of_times_to_apply_rules
    turn_angle = 60
    draw_L_system(t, inst, turn_angle, segment_length)

    wn.exitonclick()
# ----------------------------------------------------------------------


def main():
    print("Call your functions to verify their correctness.")
    #problem1()

    print_pascal_triangle(3)
    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Tests for problem 2:
    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # create a list of random integers between 0 and 100
    # size = 10
    # my_list = [0] * size
    # for n in range(size):
    #     my_list[n] = random.randint(0, 20)
    #
    # for test in range(20):
    #     print("{} was found {} times in {}".
    #           format(test, count(my_list, test), my_list))
    #     print("{} is in {} = {}".
    #           format(test, my_list, is_in(my_list, test)))
    #     print("{} is at position {} in {}".
    #           format(test, index(my_list, test), my_list))
    #
    # backwards = reverse(my_list)
    # print("List reversed is", backwards)
    #
    # for value in range(10):
    #     new_position = random.randint(0, len(my_list)-1)
    #     insert(my_list, value, new_position)
    #     print("Inserted {} at position {}, result: {}"
    #           .format(value, new_position, my_list))

    # print_pascal_triangle(10)

    # challenge_problem()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
