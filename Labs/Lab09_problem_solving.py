#!/usr/bin/env python

"""
Lab09 - Work through the problem solving steps to design, build, and test
        the draw_US_flag() function that uses Turtle Graphics to display a
        scaled American flag drawn to the precise U.S. Code specifications.
"""
# =====================================================================

# Module imports
import math
from turtle import TurtleScreen, RawTurtle, TK

# Metadata
__author__ = "Silviu Burz"
__date__ = "Sept 6, 2016"

# Constants
CANVAS_WIDTH = 1200
CANVAS_HEIGHT = 675
WINDOW_TITLE = "Lab 9 - Problem Solving"
CANVAS_BACKGROUND_COLOR = "light gray"

def create_window():
    """
    Create a graphics window, a drawing canvas in the window, and a turtle
    to draw on the canvas.
    :return: a graphics window, a canvas, and a turtle
    """

    # Create a Tkinter graphics window
    graphics_window = TK.Tk()
    graphics_window.title(WINDOW_TITLE)

    # Create a "canvas" inside the graphics window to draw on
    my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
    my_canvas.pack()  # Organizes my_canvas inside the graphics window

    # Create a "canvas" made specifically for turtle graphics
    turtle_canvas = TurtleScreen(my_canvas)
    turtle_canvas.bgcolor(CANVAS_BACKGROUND_COLOR)

    # Create a turtle to draw on the canvas
    my_turtle = RawTurtle(turtle_canvas)

    # Make the turtle draw as fast as possible - comment these lines out for
    # slower rendering.
    # turtle_canvas.delay(0)
    # my_turtle.hideturtle()
    # my_turtle.speed("fastest")

    return graphics_window, my_canvas, my_turtle

# ----------------------------------------------------------------------
# Part 1 - Understand the Problem
# Read completely the Lab 9 handout and the US Code specifications of
# the American Flag as summarized on Wikipedia at:
#    https://en.wikipedia.org/wiki/Flag_of_the_United_States#Specifications, and
#    https://en.wikipedia.org/wiki/Flag_of_the_United_States#Colors.
#
# Part 2 - Design a Solution to the Problem
# Before writing any code, complete the design requirements on the back of
# the handout.  Time spent on developing a good design will save you time
# while coding.
#
# Part 3 - Build the Solution using Python below in this file
#   a. Incrementally develop and test functions used to simplify draw_usa_flag() below.
#   b. Incrementally develop and test the draw_usa_flag() function below.
# DO NOT CHANGE the function header (specification) of draw_usa_flag().
#
# Part 4 - Test and Improve Your Solution
# Write at least three calls in main() to test your draw_usa_flag() with different values.
#
# ----------------------------------------------------------------------

# Part 3.a - place your support function code here


# ----------------------------------------------------------------------

# Part 3.b - add code below to implement the provided specification of draw_usa_flag()

def draw_usa_flag(t, x, y, height):
    """
    Draw an American flag at (x,y) with the given height in pixels
    :param t: the turtle used to draw the flax
    :param x: x coordinate in pixels of the upper left corner of the flag
    :param y: y coordinate in pixels of the upper left corner of the flag
    :param height: height (hoist) of the flag in pixels
    :return: NONE
    """
    top_stripes(t, height)
    t.left(90)
    t.forward(height*3.75/5)
    t.right(90)
    bottom_stripes(t, height)
    t.forward(height-height/13)
    draw_blue_area(t,height)




def draw_star(turtle, scale):
    return

def draw_stars_on_flag(turtle, scale):
    return


def top_stripes(turtle, scale):
    draw_stripe(turtle,scale, "red", "short")
    turtle.forward(-scale/13)
    draw_stripe(turtle, scale, "white", "short")
    turtle.forward(-scale/13)
    draw_stripe(turtle, scale, "red", "short")
    turtle.forward(-scale/13)
    draw_stripe(turtle, scale, "white", "short")
    turtle.forward(-scale/13)
    draw_stripe(turtle, scale, "red", "short")
    turtle.forward(-scale / 13)
    draw_stripe(turtle, scale, "white", "short")
    turtle.forward(-scale/13)
    draw_stripe(turtle, scale, "red", "short")
    turtle.forward(-scale / 13)
    return

def bottom_stripes(turtle, scale):
    draw_stripe(turtle, scale, "white", "long")
    turtle.forward(-scale/13)
    draw_stripe(turtle, scale, "red", "long")
    turtle.forward(-scale/13)
    draw_stripe(turtle, scale, "white", "long")
    turtle.forward(-scale/13)
    draw_stripe(turtle, scale, "red", "long")
    turtle.forward(-scale/13)
    draw_stripe(turtle, scale, "white", "long")
    turtle.forward(-scale/13)
    draw_stripe(turtle, scale, "red", "long")
    return


def draw_blue_area(turtle, scale):
    width = scale * 1.9 * 2/5
    height = scale * 7/13
    turtle.fillcolor("blue")
    turtle.begin_fill()
    turtle.right(90)
    turtle.forward(width)
    turtle.right(90)
    turtle.forward(height)
    turtle.right(90)
    turtle.forward(width)
    turtle.right(90)
    turtle.forward(height)
    turtle.end_fill()
    return


def draw_stripe(turtle, scale, color, length):
    turtle.fillcolor(color)
    turtle.begin_fill()
    if (length == "short"):
        width = scale*1.9*3
        width = width/5
    if (length == "long"):
        width = scale*1.9

    turtle.right(90)
    turtle.forward(width)
    turtle.right(90)
    turtle.forward(scale/13)
    turtle.right(90)
    turtle.forward(width)
    turtle.right(90)
    turtle.forward(scale/13)
    turtle.end_fill()
    return

# ----------------------------------------------------------------------

# Part 4 - add code below for at least 3 tests cases for draw_US_flag()

def main():
    # Create a graphics window, a canvas to draw on, and a turtle
    graphics_window, my_canvas, sam = create_window()
    sam.up()
    sam.left(90)

    draw_usa_flag(sam,0,0,100)

    # Keep the graphics window open until the user closes it.
    TK.mainloop()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
