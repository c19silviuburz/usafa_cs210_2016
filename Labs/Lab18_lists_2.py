#!/usr/bin/env python

"""
Lab18 - Learn more about lists and list manipulation.

For this lab, you will work with a partner using the "pair programming"
technique. In the "pair programming" technique, two programmers work
together on one computer. One is designated as the "pilot" and writes
the code; the other is designated as the "navigator" and reviews each
line of code as it is typed. The two programmers switch roles frequently.
For this lab, the programmers should switch roles at least every ten
to twelve minutes. (Note: When you switch roles, email your code file
to your partner. When you finish, you should each have a copy of the
same file.)
"""
# =====================================================================

import random

# Metadata
__author__ = "First Last"

# ---------------------------------------------------------------------
# Problem 1
# (Repeat question from Lab17. I want you to think about this problem again!)
# Please do the following:
#   1) Study the following code.
#   2) Write down what you think the output will be.
#   3) Discuss your expected output with a fellow cadet and resolve any
#      discrepancies.
#   4) Now execute both functions and compare you answers. If you got any of
#      the outputs wrong, make sure you know why.
#
# Note: The exact value of an object's ID is not important and you can
#       never know its value before a program executes (because Python
#       assigns ID's to objects as they are created at run-time). But
#       your can compare ID's to know if two variables reference the same
#       object. In your output, compare the object ID's.
# ----------------------------------------------------------------------


def problem1a():
    a = [1, 2, 3]
    b = a[:]  # Remember, a slice always creates a copy of the original
    b[0] = 5

    # Compare the ID's. Are they the same or different?
    print("Problem 1a output:")
    print("id: {} list: {}".format(id(a), a))
    print("id: {} list: {}".format(id(b), b))


def problem1b():
    a = [1, 2, 3]
    b = a  # b is now an alias of list a
    b[0] = 5

    # Compare the ID's. Are they the same or different?
    print("Problem 1b output:")
    print("id: {} list: {}".format(id(a), a))
    print("id: {} list: {}".format(id(b), b))

# ----------------------------------------------------------------------
# Problem 2
# Investigate how Python stores references to object by studying the
# following code and then studying it's output. Please make sure you
# understand the following two big ideas:
#   1) Python optimizes the use of computer memory by storing a
#      value only once.
#   2) A list is always a sequence of object references.
# ----------------------------------------------------------------------


def problem2():
    a = ["sam", 34, "sam", 34, "mary", 12.1, "mary", 12.1]

    for index in range(len(a)):
        print("a[{}] has id {}. It's value is: {}".format(index, id(a[index]), a[index]))

# ----------------------------------------------------------------------
# Problem 3
# When you pass a list to a function, you are passing a reference to the
# list. If the function modifies the list it receives, the original list
# is also changed. You can see this behaviour by studying and experimenting
# with the following example code. Make sure you un-comment the corresponding
# 4 lines of code in the main() function.
# ----------------------------------------------------------------------


def increment_values(a_list):
    for index in range(len(a_list)):
        a_list[index] += 1

    print("a_list inside function : ", a_list)
    # Notice that nothing needs to be returned, because the function
    # changed the original data.

# ----------------------------------------------------------------------
# Problem 4
# Make a copy of the increment_values() function above and paste it below.
# Call the new function increment_values2()
# Then modify the function to make a copy of the original list, increment
# each value in the copy, and then return the new list.
#  ----------------------------------------------------------------------

def increment_values2(a_list):
    newlist = a_list[:]
    for index in range(len(newlist)):
        newlist[index] += 1

    print("a_list inside function : ", newlist)
    return newlist

# ----------------------------------------------------------------------
# Problem 5
# As the textbook describes, when you have a list that contains sub-lists,
# you have to be careful that the sub-lists are not copies of a single list.
# Study the code below and make sure you know why it produces the output
# it does. If the reason for the output is not clear, go back to the textbook
# and re-study the section on "repetition and references".
# http://interactivepython.org/runestone/static/usafa_cs210_2016/Lists/RepetitionandReferences.html
# ----------------------------------------------------------------------


def problem5():
    # Create a list of 10 zeros
    zeros = [0] * 10
    print("zeros: ", zeros)
    # Change one value in the list
    zeros[3] = 5
    print("zeros: ", zeros)

    # Create a list of 10 sub-lists, where each sub-list is the same list
    ones = [[1]] * 10
    print("ones: ", ones)
    # Change the value in the sub-list
    ones[3][0] = 5   # Note that the 3 could have been 0-9 with the same results
    print("ones: ", ones)

    # Create a list of 10 sub-lists, where each sub-list is unique
    two = [2]
    twos = [two] * 10  # each sub-list is the same
    # Make a copy of the sub-list for each element
    for index in range(10):
        twos[index] = two[:]
    print("twos: ", twos)
    # Change the value of one of the sub-lists
    twos[3][0] = 5
    print("twos: ", twos)


# ----------------------------------------------------------------------
# Problem 6
# Define a function will add an assignment grade to a list of grades
# related to a list of cadets. The function receives two lists:
#   grade_book is a list of cadet information. Each entry is a sub-list
#       that contains the cadet name and a list of their current grades.
#   new_scores is a list of cadet grades for a new assignment.
# The order of the grade_book and the new_scores is not necessary the same.
# Your function must match the information in the two lists and add the
# new_score to the cadet's list of scores. (See the main() function for
# two example lists to work with.)
#  ----------------------------------------------------------------------

def add_to_grade_book(grade_book, new_scores):
    for i in range(len(new_scores)):
        for name in range(len(new_scores)):
            if (grade_book[i][0] == new_scores[name][0]):
                grade_book[i][1] += [new_scores[name][1]]


# ----------------------------------------------------------------------


def main():
    print("Call your functions to verify their correctness.")
    #problem1a()
    # problem1b()
    #problem2()

    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Code for problem 3: (the next 4 statements)
    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # my_list = [37, 45, 23, 12, 0, -3, 18, -12]
    # print("my_list before the call: ", my_list)
    # increment_values(my_list)
    # print("my_list after  the call: ", my_list)

    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Code for problem 4: (the next 5 statements)
    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # my_list = [37, 45, 23, 12, 0, -3, 18, -12]
    # print("my_list before the call    : ", my_list)
    # updated_list = increment_values2(my_list)
    # print("my_list after  the call    : ", my_list)
    # print("updated_list after the call: ", updated_list)

    # problem5()

    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    # Code for problem 6: (the next 10 lines)
    #  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    grade_book = [["Sam", [100, 90, 83]],
                  ["Mary", [98, 100, 96]],
                  ["Sue", [90, 80, 95]],
                  ["Fred", [79, 92, 85]]
                 ]
    new_scores = [["Sue", 100], ["Mary", 80], ["Fred", 94], ["Sam", 85]]

    print("Original grade_book:", grade_book)
    add_to_grade_book(grade_book, new_scores)
    print("Updated grade_book :", grade_book)

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
