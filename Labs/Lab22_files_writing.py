#!/usr/bin/env python

"""
Lab21 - Learn how to write text files.
"""
# =====================================================================

# Metadata
__author__ = "Silviu Burz"

def average(list):
    sum = 0
    for i in range(len(list)):
        sum += list[i]
    return sum/len(list)

# ---------------------------------------------------------------------
# Problem 1
# Please make sure you have downloaded the data files from the Moodle server
# before starting this lab.
#
# Assume that you have opened a file for reading with this command:
#
#       file_object = open("example.txt", "r")
#
# Explain what each of the following methods on the file object would do.
# Describe the data type of the variable "data" and what it will contain.
# After you have described each call, discuss your answers with a fellow cadet.
#
#   data = file_object.read()
#   - data is ... read into a single string for the whole file
#
#   data = file_object.readlines()
#   - data is ... read into a list of strings where each item is a new line
#
#   data = file_object.readline()
#   - data is ... reads the next line of the file into a string
#
#   for data in file_object:
#   - data is ... iterates through each line in the file
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 2
# Write a function that reads the studentdata.txt file and creates a new
# text file that contains one line of output for each student in the file.
# Each line should contain the student's name and their average grade on
# all assignments. Note that some students have more grades than others.
#
# Make your output file look like this:
'''
joe        23.00
bill       20.00
sue        16.17
grace      23.67
john       35.20
'''
# ----------------------------------------------------------------------


def problem2():
    infile = open("C:\\Users\\C19Silviu.Burz\\Documents\\SourceCodeRepositories\\USAFA_CS210_2016\\Data\\studentdata.txt", "r")
    data = infile.readlines()  # Note the .readlines() method
    infile.close()

    file_object = open("newstudentdata.txt", "w")

    for i in range(len(data)):
        string = ""
        name = ""
        data[i] = data[i].split(" ")
        name = str(data[i][0])
        data[i] = data[i][1:]
        for j in range(len(data[i])):
            if (data[i][j].isdigit()):
                pass
            else:
                data[i][j] = data[i][j][:2]
            data[i][j] = int(data[i][j])
        string = str(average(data[i]))
        file_object.write("{} \t Average: {}".format(name, string))
        file_object.write("\n")


# ----------------------------------------------------------------------
# Problem 3
# The code in the multiplication_table() function below prints a
# multiplication table. Modify the code so that it saves the table to a
# file. Add a parameter to the function that specifies the file name
# of the output file.
# ----------------------------------------------------------------------


def multiplication_table(rows, columns):
    # Print the top heading
    file_object = open("newstudentdata.txt", "w")
    #print("  * |", end='')
    file_object.write("  * |", end='')
    for c in range(columns):
       #print("{:5d}".format(c), end='')
        file_object.write("{:5d}".format(c), end='')
    file_object.write("\n")
   # print()

    # Print a dividing line between the heading and the table.
    #print("    |", end='')
    file_object.write("    |", end='')
    #print("-----" * columns)
    file_object.write("-----" * columns)

    # Print the rows of the table
    for r in range(rows):
        #print("{:3d} |".format(r), end='')
        file_object.write("{:3d} |".format(r), end='')
        for c in range(columns):
            #print("{:5d}".format(r * c), end='')
            file_object.write("{:5d}".format(r * c), end='')

        #print()
        file_object.write("\n")

# ----------------------------------------------------------------------
# Problem 4
# Write a function that prints "I love Python" to an output file 1 million
# times.
#
# After you have a working program, speculate on how hard it would be to
# write a malicious program that filled up a person's hard drive.
#  ----------------------------------------------------------------------


def problem4():
    file_object = open("newstudentdata.txt", "w")
    data = "I love Python"
    for i in range(1000000):
        file_object.write(data)
        file_object.write("\n")

# ----------------------------------------------------------------------
# Problem 5
# If you have not read the PEX 3 assignment, read it now. Then write a
# function that will save an instance of a RACK-O game (as defined in the
# PEX 3 description) to a text file. A "RACK-O instance" is defined in the
# main() function below for your testing.
#  ----------------------------------------------------------------------


def save_game(game):
    file_object = open("newstudentdata.txt", "w")
    for i in range(len(game)):
        file_object.write(game[i])
        file_object.write("\n")


# ----------------------------------------------------------------------

# Note: File processing is really string processing because the contents
#       of a text file is always a string.


def main():
    print("Call your functions to verify their correctness.")

    problem2()
    # multiplication_table("test.txt", 10, 10)
    # problem4()

    # game = [0,  # DEALER
    #         1,  # ACTIVE_PLAYER
    #         [[1, 2, 3, 4, 5, 6, 7, 8, 9, 10],  # HANDS
    #          [11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
    #          [21, 22, 23, 24, 25, 26, 27, 28, 29, 30]],
    #         [31, 32, 33, 34, 35, 36, 37, 38, 39, 40,  # DRAW_PILE
    #          41, 42, 43, 44, 45, 46, 47, 48, 49, 50],
    #         [],  # DISCARD_PILE
    #         [0, 0, 0]  # PLAYER_SCORES
    #        ]
    #
    # save_game(game)

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
