#!/usr/bin/env python

"""
Lab16 - Learn more string manipulation.
"""
# =====================================================================

import turtle, math

# Metadata
__author__ = "Silviu Burz"

# ---------------------------------------------------------------------
# Problem 1
# Study the L-system example below, which is from the textbook, that draws
# a "Koch curve." Try different distances and angles to see how the drawing
# changes. If you have any questions about how this code works, make
# sure you discuss your questions with your instructor.
#
# The L-system rules for this example are:
#   Axiom:  F
#   Rule 1: F --> F-F++F-F
#
# Note that problem1() is executed from the main() function below. Comment
# out the call to problem1() after you are finished with this problem, but
# do not comment out these functions.
# ----------------------------------------------------------------------


# def create_L_system(number_iterations, axiom):
#     pattern = axiom
#     for iteration in range(number_iterations):
#         pattern = process_string(pattern)
#
#     return pattern
#
#
# def process_string(original_string):
#     new_string = ""
#     for letter in original_string:
#         new_string += apply_rules(letter)
#
#     print(original_string, "-->", new_string)
#     return new_string
#
#
# def apply_rules(letter):
#     if letter == 'F':
#         new_string = 'F-F++F-F'   # Rule 1
#     else:
#         new_string = letter    # no rules apply so keep the character
#
#     return new_string
#
#
# def draw_L_system(a_turtle, instructions, angle, distance):
#     for cmd in instructions:
#         if cmd == 'F':
#             a_turtle.forward(distance)
#         elif cmd == 'B':
#             a_turtle.backward(distance)
#         elif cmd == '+':
#             a_turtle.right(angle)
#         elif cmd == '-':
#             a_turtle.left(angle)

#
# def problem1():
#     # Create a turtle and a window to draw into
#     t = turtle.Turtle()
#     wn = turtle.Screen()
#
#     # Move the turtle to the left side of the screen. The pattern will
#     # draw from left to right.
#     t.up()
#     # Backward 200 works if the iterations is 4. If you increase the
#     # number of iterations you will need to increase this distance
#     # (or decrease the segment length below.)
#     t.back(200)
#     t.down()
#     t.speed(9)
#
#     # Create the L-system
#     inst = create_L_system(4, "F")
#
#     # Draw the picture from the L-system
#     segment_length = 10
#     turn_angle = 80
#     draw_L_system(t, inst, turn_angle, segment_length)
#
#     wn.exitonclick()

# ----------------------------------------------------------------------
# Problem 2
# Modify the functions in problem 1 to draw a different pattern that
# is created using these L-system rules:
#
#   Axiom:  L
#   Rule 1: L -> +RF-LFL-FR+
#   Rule 2: R -> -LF+RFR+FL-
#
# Hint: Do the following:
#       * Re-write the apply_rules function.
#       * Change the "axiom" in the call to create_L_system() to "L".
#       * Use a turn angle of 90 degrees.
#       All other functions can be used "as is".
#
# This L-system creates a "Hilbert curve," which is a "space filling"
# curve that "visits" every location on a grid without ever crossing
# itself or picking up its pen.
# ----------------------------------------------------------------------
# def create_L_system(number_iterations, axiom):
#     pattern = axiom
#     for iteration in range(number_iterations):
#         pattern = process_string(pattern)
#
#     return pattern
#
#
# def process_string(original_string):
#     new_string = ""
#     for letter in original_string:
#         new_string += apply_rules(letter)
#
#     print(original_string, "-->", new_string)
#     return new_string
#
#
# def apply_rules(letter):
#     if letter == 'R':
#         new_string = '-LF+RFR+FL-'
#     elif letter == 'L':
#         new_string = '+RF-LFL-FR+'
#     else:
#         new_string = letter    # no rules apply so keep the character
#
#     return new_string
#
#
# def draw_L_system(a_turtle, instructions, angle, distance):
#     for cmd in instructions:
#         if cmd == 'F':
#             a_turtle.forward(distance)
#         elif cmd == 'B':
#             a_turtle.backward(distance)
#         elif cmd == '+':
#             a_turtle.right(angle)
#         elif cmd == '-':
#             a_turtle.left(angle)
#
# def problem2():
# # Create a turtle and a window to draw into
#     t = turtle.Turtle()
#     wn = turtle.Screen()
#
#     # Move the turtle to the left side of the screen. The pattern will
#     # draw from left to right.
#     t.up()
#     # Backward 200 works if the iterations is 4. If you increase the
#     # number of iterations you will need to increase this distance
#     # (or decrease the segment length below.)
#     t.back(200)
#     t.down()
#     t.speed(9)
#
#     # Create the L-system
#     inst = create_L_system(4, "L")
#
#     # Draw the picture from the L-system
#     segment_length = 10
#     turn_angle = 90
#     draw_L_system(t, inst, turn_angle, segment_length)
#
#     wn.exitonclick()

# ----------------------------------------------------------------------
# Problem 3
# Modify the functions in problem 1 to draw a different pattern that
# is created using theses L-system rules:
#
#   Axiom:  FXF--FF--FF
#   Rule 1: F -> FF
#   Rule 2: X -> --FXF++FXF++FXF--
#
# Hint: Do the following:
#       * Re-write the apply_rules function.
#       * Change the "axiom" in the call to create_L_system() to "FXF--FF--FF".
#       * Use a turn angle of 60 degrees.
#       All other functions can be used "as is".
#
#  This L-system creates a "Sierpinski Triangle."
# ----------------------------------------------------------------------
def create_L_system(number_iterations, axiom):
    pattern = axiom
    for iteration in range(number_iterations):
        pattern = process_string(pattern)

    return pattern


def process_string(original_string):
    new_string = ""
    for letter in original_string:
        new_string += apply_rules(letter)

    print(original_string, "-->", new_string)
    return new_string


def apply_rules(letter):
    if letter == 'F':
        new_string = 'FF'
    elif letter == 'X':
        new_string = '--FXF++FXF++FXF--'
    else:
        new_string = letter    # no rules apply so keep the character

    return new_string


def draw_L_system(a_turtle, instructions, angle, distance):
    for cmd in instructions:
        if cmd == 'F':
            a_turtle.forward(distance)
        elif cmd == 'B':
            a_turtle.backward(distance)
        elif cmd == '+':
            a_turtle.right(angle)
        elif cmd == '-':
            a_turtle.left(angle)

def problem3():
# Create a turtle and a window to draw into
    t = turtle.Turtle()
    wn = turtle.Screen()

    # Move the turtle to the left side of the screen. The pattern will
    # draw from left to right.
    t.up()
    # Backward 200 works if the iterations is 4. If you increase the
    # number of iterations you will need to increase this distance
    # (or decrease the segment length below.)
    t.back(200)
    t.down()
    t.speed(9)

    # Create the L-system
    inst = create_L_system(4, "FXF--FF--FF")

    # Draw the picture from the L-system
    segment_length = 10
    turn_angle = 60
    draw_L_system(t, inst, turn_angle, segment_length)

    wn.exitonclick()

# ----------------------------------------------------------------------
# Problem 4
# Please do the following:
#   1) Study the following code.
#   2) Write down what you think the output will be.
#   3) Discuss your expected output with a fellow cadet and resolve any
#      discrepancies.
#   4) Now execute the code and compare you answers. If you got any of
#      the outputs wrong, make sure you know why.
# ----------------------------------------------------------------------

def problem4():
    string = "Trump vs. Clinton, what a mess!"
    print("1: ", string[3:8])
    print("2: ", string[3:])
    print("3: ", string[:5])
    print("4: ", string[:])
    print("5: ", string[-7:-3])
    print("6: ", string[-7:])
    print("7: ", string[:-20])
    print("8: ", string[3:20:6])  # This was not in the reading! Guess how it works.

# ----------------------------------------------------------------------
# Problem 5
# The exact definition of Python's print function is:
#
#     print(*objects, sep=' ', end='\n', file=sys.stdout, flush=False)
#
# Note that the last 4 parameters to the function have default values.
# This means that they are optional parameters and if they are not
# specified in a call statement, the parameter uses its default
# value. For this problem you will investigate only the "sep" and the "end"
# parameters. Note that the "sep" parameter specifies the string that is
# printed between each value in the printed output (i.e., the "seperator"),
# and the "end" parameter specifies the string that is printed at the
# end of the line. Note that the default separator is a single space
# and the default ending is a new-line (\n) character.
#
# Please do the following:
#   1) Study the following code.
#   2) Write down what you think the output will be.
#   3) Discuss your expected output with a fellow cadet and resolve any
#      discrepancies.
#   4) Now execute the code and compare you answers. If you got any of
#      the outputs wrong, make sure you know why.
# ----------------------------------------------------------------------


def problem5():
    a = 37
    b = 5.5
    c = "abc"
    print("1: ", a, b, c)
    print("2: ", a, b, c, sep='')
    print("3: ", a, b, c, sep='fred')
    print("4: ", a, b, c, end='')
    print("5: ", a, b, c, end='sam')
    print("6: ", a, b, c, end='\t end')
    print("7: ", a, b, c, sep='__', end="|\n")

# ----------------------------------------------------------------------
# Problem 6
# Make sure you have read the information on "formatted output" on the
# lab 16 page of the course web site.
#
# Please do the following:
#   1) Study the following code.
#   2) Write down what you think the output will be.
#   3) Discuss your expected output with a fellow cadet and resolve any
#      discrepancies.
#   4) Now execute the code and compare you answers. If you got any of
#      the outputs wrong, make sure you know why.
#
# Please ask your instructor for clarification if this is confusing.
# Note the following format specifiers:
#    f - floating point value
#    d - decimal value (integer)
#    s - string value
# ----------------------------------------------------------------------


def problem6():
    a = 37
    b = math.pi
    c = "Wayne"
    print("1: ", "{} is a {} of {}".format(a, b, c))
    print("2: ", "{} is a {} of {}".format(c, a, b))
    print("3: ", "{} is a {:12.4f} of {:12s}".format(a, b, c))
    print("4: ", "{:.4f}".format(b))
    print("5: ", "{:.2f} is a {:10d}".format(b, a))

# ----------------------------------------------------------------------
# Challenge Problem
# Define a function that, given a normal sentence in a string, it converts
# the sentence to pig Latin using the following two rules:
#   1) For words that begin with consonant sounds, all letters before
#      the initial vowel are placed at the end of the word sequence.
#      Then, "ay" is added, as in the following examples:
#
#         "pig" --> "igpay"
#         "banana" --> "ananabay"
#         "trash" --> "ashtray"
#         "happy" --> "appyhay"
#         "duck" --> "uckday"
#         "glove" --> "oveglay"
#
#   2) For words that begin with vowel sounds, one just adds "yay"
#      to the end. Examples are:
#
#         "eat" --> "eatyay"
#         "omelet" --> "omeletyay"
#         "are" --> "areyay"
#
# Hints:
# 1) Break your sentence into individual "words" using the string
#    split(" ") method which returns a list of strings. For example,
#    "This is a test".split(' ') returns ['This', 'is', 'a', 'test']
# 2) Process rule 2 first.
# ----------------------------------------------------------------------


def challenge(sentence):
    returnsentence = ""
    for i in sentence.split(' '):
        if i[0] in "a,e,i,o,u":
            returnsentence += i + "y"
        else:
            for j in range(len(i)):
                if chr(j) not in "a,e,i,o,u":
                    returnsentence += i[j]
                else:
                    for k in range(len(i)-j):
                        returnsentence += i[k]
        returnsentence += "ay "

    return returnsentence
# ----------------------------------------------------------------------


def main():
    #problem1()
    #problem2()
    # problem3()
    # problem4()
    # problem5()
    # problem6()

    # sentence = "Now is the time for all good men to come to the aid of their country"
    # print(to_pig_latin(sentence))


    print(challenge("This is just a test"))
# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
