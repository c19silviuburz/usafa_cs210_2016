#!/usr/bin/env python

"""
Lab30 - Learn more about how to create and use objects.
"""
# =====================================================================

# Metadata
__author__ = "Silviu Burz"

# ---------------------------------------------------------------------
# Problem 1:
# With a fellow cadet, discuss the Fraction Class that was introduced
# in the textbook, which is shown below. Make sure you understand every
# line of code in the Class definition. If anything is confusing,
# discuss it with your instructor.
#
# Why is the gcd() function not made a part of the Fraction class?
#
# In the problem1() function below, create several different objects
# from the Fraction Class and print them to verify each object.
# Modify the Class to make every Fraction object always print in its
# simplest form. (Think about modifying the __str__ method.)
# ---------------------------------------------------------------------


def gcd(m, n):
    """
    Find the greatest common divisor of two numbers: m and n
    :param m: an integer
    :param n: an integer
    :return: The greatest common divisor of m and n
    """
    while m % n != 0:
        m, n = n, m % n

    return n


class Fraction:
    """
    Store and manipulate a fraction
    """
    def __init__(self, top, bottom):
        self.num = top        # the numerator is on top
        self.den = bottom     # the denominator is on the bottom
        self.gcd1 = 1

    def __str__(self):
        gcd1 = gcd(self.num, self.den)
        return str(int(self.num/gcd1)) + "/" + str(int(self.den/gcd1))

    def simplify(self):
        common = gcd(self.num, self.den)

        self.num = self.num // common
        self.den = self.den // common


def problem1():
    a = Fraction(6,8);
    print(a);

# ---------------------------------------------------------------------
# Problem 2:
# Make sure you have read the lesson in the textbook on "sameness" and
# note the following:
#   "shallow equality" means two variables reference the same object.
#   "deep equality" means that two objects have the same "state".
#
# Note: Be cautious of using == for "deep equality". (It works in some
#       cases and not in others!)
#
# To test for "shallow equality," always use the "is" operator.
#
# The == operator sometimes does a "shallow equality" and sometimes
# a "deep equality" test. Always verify which one is performed on a
# specific type of object before using the == operator.
#
# In the problem2() function below, create come Fraction objects and
# experiment with the "is" operator and the == operator. Determine
# if the == operator does a shallow or deep comparison on Fractions.
#
# Result: == does a shallow equality on Fraction objects.
# =====================================================================


def problem2():
    a = Fraction(3,6);
    b = Fraction(6,13);
    c = Fraction(9, 19);
    d = Fraction(1,2);
    e = Fraction(2,4);

    print(a is b)
    print(d is a)
    print(a is e)
    print(c is e)

    print(a == e)
    print(c == e)


# ---------------------------------------------------------------------
# Problem 3:
# Remember that you created a __str__() function that determines how an
# object is converted to a string. Every object has a default __str__()
# function that it inherits from Python when it is defined.
# When you create a __str__() function for your Class, you are "overriding"
# the built-in version of __str__(). Look again at the definition of
# the Fraction Class above and notice that in the left margin there is
# a small, blue-circle icon with a red up-arrow. If you place your cursor
# over this icon it says "Overrides method in object." You can change the
# way that a Class works by overriding the default methods that all
# Python Classes automatically inherit from Python.
#
# If you implement a function called __eq__() then you can decide whether
# an object does "shallow equality" or "deep equality" when the == operator
# is used. (You should have determined in Problem 2 that by default the
# == operator does a "shallow equality".) Let's change how the ==
# operator works for Fractions. In the Fraction Class above, add a new
# method that has this function stub:
#
#     def __eq__(self, other_fraction):
#         pass
#
# Replace the "pass" statement, which is just a dummy statement that says
# "do nothing", with logic that returns True if the "self" object and the
# "other_fraction" object represent the same fraction. Remember that
# 1/2, 2/4, and 10/20 are all equal, so you need to simplify both
# fractions before you compare them.
#
# After you are finished with the __eq__() method, notice the same
# "override" icon appears to the left of the __eq__ method definition.
# Anytime that objects of the Fraction class are compared with an ==
# operator, the __eq__() function will automatically be called to get
# the answer. In this way you have total control over how standard
# operations on Fraction objects will behave.
#
# Here is a list of the other pre-defined comparison functions and
# the operators they override:
# Operator                   Expression	  Internally
# Less than
#      f1 < f2      f1.__lt__(f2)
# Less than or equal to      f1 <= f2     f1.__le__(f2)
# Equal to                   f1 == f2     f1.__eq__(f2)
# Not equal to               f1 != f2     f1.__ne__(f2)
# Greater than	             f1 > f2      f1.__gt__(f2)
# Greater than or equal to   f1 >= f2     f1.__ge__(f2)
#
# Implement several other override methods in the Fraction Class above
# to specify how the comparison operators work on two fractions.
# Test your override methods by using them in the problem3() function
# below.
# ---------------------------------------------------------------------


def problem3():
    pass

# ---------------------------------------------------------------------
# Problem 4:
# PEX 4 will be an implementation of a RACK-O game using objects.
# In the space below, stub out several Class definitions that could be
# used to implement a RACK-O game. In your "stubs", include an __init__()
# method that defines all of the "state" attributes of the class you
# are creating. Then stub out all of the methods the Class will have.
# As in the above problem, a method stub has a correct function heading,
# but no internal logic. (Functions must have at least one statement
# in them to be valid, so include a "pass" statement to make the stub
# valid.)
# ---------------------------------------------------------------------

# ---------------------------------------------------------------------


def main():
    problem1()
    problem2()
    problem3()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
