#!/usr/bin/env python

"""
Lab15 - Learn more string manipulation.
"""
# =====================================================================

# # Metadata
__author__ = "First Last"

# ----------------------------------------------------------------------
# Problem 1
# Write a function that removes all occurrences of a given letter from
# a string. It's input parameters should be a string and the letter to
# remove.
# ----------------------------------------------------------------------

def RemoveLetter(string, letter):
    newstring = ""
    for i in range(len(string)):
        if (string[i] != letter):
            newstring += string[i]
    return newstring

# ----------------------------------------------------------------------
# Problem 2
# Write a function that counts how many times a substring occurs in a
# string.
# ----------------------------------------------------------------------

def CheckSubString(string, substring):
    counter = 0
    valid = 1
    for i in range(len(string)):
        for j in range(len(substring)):
            if (valid == 1):
                if (string[i+j] != substring[j]):
                    valid = 0
        if (valid == 1):
            counter += 1
        valid = 1
    return counter

# ----------------------------------------------------------------------
# Problem 3
# Write a function that removes the first occurrence of a string from
# another string.
# ----------------------------------------------------------------------

def RemoveFirstOccurance(string, substring):
    valid = 1
    newstring = ""
    skipnext = 0
    boolfirstoccurance = 0
    for i in range(len(string)):
        if (skipnext ==0):
            for j in range(len(substring)):
                if (valid == 1):
                    if (string[i+j] != substring[j]):
                        valid = 0
            if (boolfirstoccurance == 1):
                valid = 0
            if (valid == 0):
                newstring += string[i]
                valid = 1
            else:
                skipnext = len(substring)-1
                boolfirstoccurance = 1
        else:
            skipnext -= 1
    return newstring
# ----------------------------------------------------------------------
# Problem 4
# Write a function that removes all occurrences of a string from another
# string.
# ----------------------------------------------------------------------

def RemoveAllOccurance(string, substring):
    valid = 1
    newstring = ""
    skipnext = 0
    for i in range(len(string)):
        if (skipnext ==0):
            for j in range(len(substring)):
                if (valid == 1):
                    if (string[i+j] != substring[j]):
                        valid = 0
            if (valid == 0):
                newstring += string[i]
                valid = 1
            else:
                skipnext = len(substring)-1
        else:
            skipnext -= 1
    return newstring

# ----------------------------------------------------------------------
# Problem 5
# Write a function that prints all of the separate "words" in a string.
# Print each word on a separate line. Consider a "word" to be characters
# surrounded by spaces. Use the following algorithm:
#
#    find the location of the first space
#    while there are 1 or more spaces in a string:
#         take all the characters before the space as a single word and print it
#         remove all of the characters up to and including the space
#         find the location of the next space
#
# ----------------------------------------------------------------------




# ----------------------------------------------------------------------
# Problem 6
# Write a function that implements a substitution cipher. In a substitution
# cipher one letter is substituted for another to garble the message.
# For example A -> Q, B -> T, C -> G etc.  Your function should take two
# parameters, the message you want to encrypt, and a string that represents
# the mapping of the 26 letters in the alphabet. Your function should
# return a string that is the encrypted version of the message.
#
# Note the following:
#  1) The mapping "MAWBCDFHYIJKLNOGPQERSTUVXZ" means that A -> M, B -> A,
#     C -> W, D -> B, etc.
#  2) Spaces should be ignored.
#  3) Only letters should be mapped to new letters. Ignore any character that
#     is not a letter of the alphabet (a-z). (Use the string method isalpha().)
#  4) You can determine the numerical value of a letter using the ord()
#     function. To find its offset in the alphabet, subtract it from the
#     ord("A"). (Hint: convert all characters in the input string to upper case.)
#
# For example, If your message is "Now is the time for all good men to come
# to the aid of their country.", and your substitution map is
# "MAWBCDFHYIJKLNOGPQERSTUVXZ", the encrypted string would be
# "NOUYERHCRYLCDOQMKKFOOBLCNROWOLCRORHCMYBODRHCYQWOSNRQX"
#  ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 7
# Write a function that decrypts the message from the previous problem.
# It should also take two parameters: the encrypted message, and the mixed
# up alphabet. The function should return a string that contains the
# original, unencrypted message, but without the spaces or any
# non-alphabetic characters.
#
# Hint: use the chr(n) function to convert a number into a character. You
# will need to offset the index of the letter by ord("A").
#  ----------------------------------------------------------------------


# ----------------------------------------------------------------------

def main():
    print("call the functions to verify their correctness.")
    print(RemoveAllOccurance("asdaddasdmin","asd"))

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()

