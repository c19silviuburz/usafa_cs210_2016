#!/usr/bin/env python

"""
Lab11 - Learn how to create "while loops".
        Compare and contrast "for loops" with "while loops".

        All of the problems in this lab exercise are written as function
        to give you additional practice in writing functions. As in previous
        labs, comment out your answers when you move on to the next problem,
        but you do not need to comment out the functions.
"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules

# # Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "Sep 14, 2016"

# ----------------------------------------------------------------------
# Problem 1
# Add a print statement to Newton’s sqrt function that prints out the
# value of "better" each time it is calculated. Call your modified
# function several times with various input values and print the returned
# result.
#
# The "Newton’s sqrt function" from the textbook is provided below for
# your convenience.
#
# Note: We discussed in the previous lesson that you have two general
#       options for solving problems:
#         1) Calculate an answer using a formula
#         2) Accumulate an answer using repeated calculations
#       Consider which type of solution this is.
# ----------------------------------------------------------------------


def newton_sqrt(n):
    approximate = 0.5 * n
    better = 0.5 * (approximate + n/approximate)
    while better != approximate:
        print(better)
        approximate = better
        better = 0.5 * (approximate + n/approximate)

    return approximate

# ----------------------------------------------------------------------
# Problem 2
# Write a function called print_triangular_numbers(n) that prints out the
# first n triangular numbers. A call to print_triangular_numbers(5) would
# produce the following output:
#    1       1
#    2       3
#    3       6
#    4       10
#    5       15
# Hint: use a web search to find out what a triangular number is.
#
# Implement the function using a "for loop" and then again using a "while
# loop". After you are finished, compare and contrast your 2 solutions.
# Which makes the code simpler and easier to understand -- a "for loop"
# or a "while loop"?  Why?
# ----------------------------------------------------------------------

def print_triangular_numbers(n):
    sum = 0
    for i in range(n):
        sum += i
        print(sum)

# ----------------------------------------------------------------------
# Problem 3
# Write a function, is_prime, that takes a single integer argument and
# returns True when the argument is a prime number and False otherwise.
# Note that a number is prime only if it is exactly divisible by itself
# and 1.
#
# Implement the function using a "for loop" and then again using a "while
# loop". After you are finished, compare and contrast your two solutions.
# Which makes the code simpler and easier to understand -- a "for loop"
# or a "while loop"?  Why?
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 4
# Modify the walking turtle program from the textbook so that rather than
# a 90 degree left or right turn, the angle of the turn changes as follows:
#    5% of the time the angle is in the range [-60, -50).
#   20% of the time the angle is in the range [-50, -20).
#   50% of the time the angle is in the range [-20, +20).
#   20% of the time the angle is in the range [+20, +50).
#    5% of the time the angle is in the range [+50, +60).
#
# Hint: Generate a random floating point value in the range [0,1) to get
#       a percentage. Use this random value to decide the range of the
#       angle. Then generate a second random value in the appropriate
#       range for the actual angle.
#
# The code from the textbook is provided below as a starting point.
# Note: The variable names have been made more descriptive so that the
#       code is easier to understand. And the code has been placed into
#       appropriate functions to make the code more modular.
#       You should completely understand the existing code before trying
#       to modify it. If you do not understand anything, please ask your
#       instructor for help.
# ----------------------------------------------------------------------

import random
import turtle


def turtle_is_inside_the_screen(window, my_turtle):
    left_boundary = - window.window_width() / 2
    right_boundary = window.window_width() / 2
    top_boundary = window.window_height() / 2
    bottom_boundary = - window.window_height() / 2

    turtle_x = my_turtle.xcor()
    turtle_y = my_turtle.ycor()

    return left_boundary <= turtle_x <= right_boundary and \
        bottom_boundary <= turtle_y <= top_boundary


def random_walking_turtle(window, my_turtle):

    my_turtle.shape('turtle')

    while turtle_is_inside_the_screen(window, my_turtle):
        coin = random.randrange(0, 2)
        if coin == 0:
            my_turtle.left(90)
        else:
            my_turtle.right(90)

        my_turtle.forward(50)


def problem_4():
    # Create a screen and a turtle
    window = turtle.Screen()
    sue = turtle.Turtle()

    random_walking_turtle(window, sue)

    # Keep the window open until the user closes the window.
    window.exitonclick()

# Execute problem 4
# problem_4()

# ----------------------------------------------------------------------
# Problem 5
# Modify the turtle random walk program so that you have two turtles,
# each with a random starting location. Keep the turtles moving until
# one of them leaves the screen.
#
# Use the function turtle_is_inside_the_screen() from the previous problem
# without modifying it. Write code for the two_random_walking_turtles()
# function stub below. (It is legal (and recommended) that you copy the
# code from the previous random_walking_turtle() function and then modify
# it appropriately. And create new, additional functions if they will help
# you organize and simplify your code.
# ----------------------------------------------------------------------


def two_random_walking_turtles(window, turtle_one, turtle_two):
    """
    Two turtles randomly walk around the screen until one of them
    goes off the screen.
    :param window: the window to draw on
    :param turtle_one: a turtle
    :param turtle_two: a turtle
    :return: None
    """


def problem_5():
    # Create a screen and two turtles
    window = turtle.Screen()
    sam = turtle.Turtle()
    sue = turtle.Turtle()

    two_random_walking_turtles(window, sam, sue)

    # Keep the window open until the user closes the window.
    window.exitonclick()

# Execute problem 5
# problem_5()

# ----------------------------------------------------------------------
# Challenge Problem 1
# Create a new version of the previous turtle walk program so that the
# two turtles turn around when they go past a boundary of the
# screen. This will implement an infinite loop, since there is no
# condition to make the turtles stop moving.
#
# Use the functions you have already created to help solve this problem.
#
# Note: If you create an infinite loop and then close your graphics
#       window, your program will generate a run-time error in the
#       console window. Typically, infinite loops should be avoided!
# ----------------------------------------------------------------------


def self_aware_turtles(window, turtle_one, turtle_two):
    """
    Two turtles randomly walk around the screen forever.
    :param window: the window to draw on
    :param turtle_one: a turtle
    :param turtle_two: a turtle
    :return: None
    """


def challenge_problem():
    # Create a screen and two turtles
    window = turtle.Screen()
    sam = turtle.Turtle()
    sue = turtle.Turtle()

    self_aware_turtles(window, sam, sue)

    # Keep the window open until the user closes the window.
    window.exitonclick()

# Execute the challenge problem
# challenge_problem()


def main():
    print_triangular_numbers(8)

if __name__ == "__main__":
    main()
