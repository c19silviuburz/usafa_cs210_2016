#!/usr/bin/env python

"""
Lab23 - Learn how to use dictionaries.
"""
# =====================================================================

import string

# Metadata
__author__ = "Silviu Burz"

# ----------------------------------------------------------------------
# Problem 1
# Given the function below, fill in commands to perform the actions
# described in the comments. Print the dictionary after each modification
# to verify that the action was performed correctly.
# ----------------------------------------------------------------------


def problem1():
    # Create the initial inventory.
    inventory = {'apples': 15, 'bananas': 35, 'grapes': 12}

    # 1) Print the names of the items in the inventory.
    #    (That is, print the keys in the dictionary.)

    for i in inventory:
        print(i)

    # 2) Add 'peaches" to the inventory and give it a value of 37.

    inventory.update({'peaches': 37})

    # 3) Print the number of bananas in the inventory.

    print(inventory.get("bananas"))

    # 4) Delete grapes from the inventory.

    del inventory["grapes"]

    # 5) Use a loop to print every item in the inventory that has a
    #    value greater than 20.

    for i in inventory:
        if (inventory.get(i)>20):
            print(i)

# ---------------------------------------------------------------------
# Problem 2
# Read the data file called update_inventory.txt which contains how many
# apples, bananas, and grapes have been removed and added to an food
# warehouse. As you read each line, update the inventory numbers for the
# appropriate type of fruit. (Negative numbers indicate removals from the
# warehouse. Positive numbers indicate deliveries to the warehouse.)
# After all lines in the file have been processed, display the totals
# for each fruit to the console window. Your final output should look
# like this:
#   grapes        412
#   bananas       225
#   apples         70
# ----------------------------------------------------------------------


def update_inventory(current_inventory):
    infile = open("C:\\Users\\C19Silviu.Burz\\Documents\\SourceCodeRepositories\\USAFA_CS210_2016\\Data\\update_inventory.txt", "r")
    data = infile.readlines()  # Note the .readlines() method
    infile.close()
    grapes = 0
    apples = 0
    bananas = 0
    for i in range(len(data)):
        stats = data[i].split()
        if (stats[0] == grapes):
            grapes += stats[1]
        elif (stats[0] == apples):
            apples += stats[1]
        else:
            bananas += stats[1]
    print ("grapes = \t", grapes, "\nbananas = \t", bananas, "\napples = \t", apples)



# ---------------------------------------------------------------------
# Problem 3
# Study the following code that counts the number of occurrences of each
# letter in a string. Then discuss the code with a fellow cadet. Then
# modify the code in the following ways and investigate what happens.
#  1) Comment out the "keys.sort()" line. Note that the order of the printed
#     list is unpredictable. (Dictionaries are naturally unordered.)
#  2) Notice the import of the string module in line 8 above. This brings
#     in several pre-defined values that are helpful for string processing.
#     Print out the value of string.ascii_lowercase to see what it contains.
#  3) The dictionary method .keys() returns a "view" of the keys in a
#     dictionary. You can always convert a "view" to a list using the list()
#     conversion function. See what happens when you remove the call to
#     list() in the statement: keys = list(letter_count.keys())
#  4) Modify the function to only print the letters that appeared in the
#     input string. That is, if the count of a letter is zero, don't display it.
# ----------------------------------------------------------------------


def count_occurrences(sentence):

    # Convert all alphabetic characters to lower case.
    sentence = sentence.lower()

    # Create a counter for every letter in the alphabet using a dictionary
    letter_count = {}
    for char in string.ascii_lowercase:
        letter_count[char] = 0

    # For every alphabetic character, increment its counter by 1
    for char in sentence:
        if char in string.ascii_lowercase: # ignore any punctuation, numbers, etc
            letter_count[char] += 1

    # Print the counters out in sorted order
    keys = list(letter_count.keys())
    keys.sort()
    for char in keys:
        if (letter_count[char] != 0):
            print("{} occurs {} times".format(char, letter_count[char]))

# ----------------------------------------------------------------------
# Problem 4
# Finish implementing the function called alice_words() below that
# creates a text file named alice_words.txt in your Data folder
# containing an alphabetical listing of all the 'words' in "Alice's
# Adventures in Wonderland". The text file "alice_in_wonderland.txt"
# contains the book's text. Include with each 'word' the number of
# times it occurs in the text. Note that for this problem you can
# ignore the problem of removing punctuation at the beginning and
# end of 'words'.
#
# The first 12 lines of your output file should be:
#   "'TIS                              1
#   "--SAID                            1
#   "Come                              1
#   "Coming                            1
#   "Defects,"                         1
#   "Edwin                             1
#   "French,                           1
#   "HOW                               1
#   "He's                              1
#   "How                               1
#   "I                                 7
#   "I'll                              2

# Hint: Read the entire file into a string. Then use .split() to get
#       a list of all the 'words' in the book.
#  ----------------------------------------------------------------------


def alice_words():
    infile = open("../Data/alice_in_wonderland.txt", "r", errors='replace')
    all_text = infile.read()
    infile.close()

    # Remove all non-printable characters from the data, but keep new-lines
    printable_text = ""
    for char in all_text:
        if char in string.printable or char == '\n':
            printable_text += char

    splittext = printable_text.split()
    count = 0
    for i in splittext:
        count = 0
        for j in splittext:
            if (i==j):
                count += 1
        print(i, "\t\t", count)
    # Finish the function by processing the printable_text ...

# ----------------------------------------------------------------------


def main():
    print("Call your functions to verify their correctness.")

    # ------------------------------------------------------------------
    # Problem 1 test:
    #problem1()

    # ------------------------------------------------------------------
    # Problem 2 test:
    # inventory = {'apples': 15, 'bananas': 35, 'grapes': 12}
    #update_inventory()

    # ------------------------------------------------------------------
    # Problem 3 test:
    # sentence = "This is a test sentence for counting characters in a string."
    # count_occurrences(sentence)

    # ------------------------------------------------------------------
    # Problem 4 test:
    alice_words()


# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
