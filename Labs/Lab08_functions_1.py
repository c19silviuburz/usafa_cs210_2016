#!/usr/bin/env python

"""
Lab08 - Learn how to create and invoke functions.

NOTE: Since functions are not executed unless invoked, as you work
      through these problems, only comment out code that is executed and
      leave the functions un-commented. You will reuse functions written
      for one problem in other problems.
"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules

# Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "Aug 31, 2016"

from turtle import TurtleScreen, RawTurtle, TK

# Constants
CANVAS_WIDTH = 600
CANVAS_HEIGHT = int(CANVAS_WIDTH * 9 / 16)  # Produces the eye-pleasing 16:9 HD aspect ratio.

# Create a Tkinter graphics window
graphics_window = TK.Tk()
graphics_window.title("Lab 8 - Functions")

# Create a "canvas" inside the graphics window to draw on
my_canvas = TK.Canvas(graphics_window, width=CANVAS_WIDTH, height=CANVAS_HEIGHT)
my_canvas.pack()  # Organizes my_canvas inside the graphics window

# Create a "canvas" made specifically for turtle graphics
turtle_canvas = TurtleScreen(my_canvas)
turtle_canvas.bgcolor("white")

# Create a turtle to draw on the canvas
phil = RawTurtle(turtle_canvas)

# Make the turtle draw as fast as possible - comment these lines out for
# slower rendering.
turtle_canvas.delay(0)
phil.hideturtle()
phil.speed("fastest")

# ----------------------------------------------------------------------
# Problem 1
# Use the drawsquare function we wrote in this chapter in a program to
# draw the example image for problem 1. Assume each side is 20 units.
# (Hint: notice that the turtle has already moved away from the ending
#  point of the last square when the program ends.)
# ----------------------------------------------------------------------
phil.penup()
# def DrawSquare(length):
#     phil.left(180)
#     phil.forward(length/2)
#     phil.right(90)
#     phil.forward(length/2)
#     phil.right(90)
#     phil.pendown()
#     for side in range(4):
#         phil.forward(length)
#         phil.right(90)
#     phil.penup()
#     phil.forward(length/2)
#     phil.right(90)
#     phil.forward(length/2)
#     phil.left(90)

# for square in range(5):
#     DrawSquare(20)
#     phil.forward(20*2)


# ----------------------------------------------------------------------
# Problem 2
# Write a program to draw the design in the image. Assume the innermost
# square is 20 units per side, and each successive square is 20 units
# bigger, per side, than the one inside it.
# ----------------------------------------------------------------------


# for i in range(20,140,20):
#     DrawSquare(i)

# ----------------------------------------------------------------------
# Problem 3
# Write a non-fruitful function drawPoly(a_turtle, num_sides, side_length)
# which makes a turtle draw a regular polygon. When called with
# drawPoly(tess, 8, 50), it draws a polygon with 8 sides, where each
# side has a length of 50.
# ----------------------------------------------------------------------

def drawPoly(a_turtle, num_sides, side_length):
    a_turtle.pendown()
    for i in range(num_sides):
        a_turtle.forward(side_length)
        a_turtle.right((180 - (360/num_sides)))
    a_turtle.penup()

# drawPoly(phil, 8, 50)


# ----------------------------------------------------------------------
# Problem 4
# Draw a pattern created by the superposition of squares rotated at
# successive 18 degree increments. Your solution must use a function
# to draw the square.
# ----------------------------------------------------------------------

def DrawSquare(length):
    phil.pendown()
    for side in range(4):
        phil.forward(length)
        phil.right(90)
    phil.penup()

# for i in range(20):
#     DrawSquare(20)
#     phil.right(18)

# ----------------------------------------------------------------------
# Problem 5
# Implement a function that draws a 4 sided spiral, where each side of
# the spiral grows longer by 2 units. The change in angle between each
# side of the spiral must be a parameter to the function. Then call the
# function twice to produce the demo images (using angles of 90 and 89).
# (Experiment with other angles besides the ones needed for the demo
# images -- if you have extra time.)
# ----------------------------------------------------------------------

def drawspiral(angle,length):
    phil.pendown()
    for i in range(length):
        phil.forward(length-i)
        phil.right(angle)
    phil.penup()


# drawspiral(89, 120)
# ----------------------------------------------------------------------
# Problem 6
# Write a non-fruitful function drawEquitriangle(a_turtle, side_length)
# which calls drawPoly from the previous question to have its turtle
# draw a equilateral triangle. Make 2 or more calls to this new function.
# ----------------------------------------------------------------------


def drawEquitriangle(a_turtle, side_length):
    drawPoly(a_turtle, 3, side_length)


drawEquitriangle(phil, 30)

# ----------------------------------------------------------------------
# Problem 7
# Write a fruitful function sumTo(n) that returns the sum of all integer
# numbers up to and including n. Use the equation (n * (n + 1)) // 2 to
# calculate your answer. Then implement another function that calculates
# the same sum, but this time using the "accumulator pattern." Call this
# function sumUp(n). Call the functions several times to verify that
# they get the correct answer. Note: The sum 1+2+3...+10 is 55.
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Problem 8
# Write a function areaOfCircle(radius) which returns the area of a
# circle. Use the math module in your solution appropriately. Call your
# function several times to verify its correctness.
# ----------------------------------------------------------------------


# Keep the graphics window open until the user closes it.
TK.mainloop()

