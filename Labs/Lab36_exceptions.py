#!/usr/bin/env python

"""
Lab36 - Exceptions
"""
# =====================================================================

import tkinter as tk
from tkinter import filedialog
import os  # to be able to get the current working directory; os.getcwd()

# Metadata
__author__ = "First Last"

# ---------------------------------------------------------------------
# Exercise 1:
# The code below will be used to demonstrate how exceptions modify the
# normal, sequential flow-of-control of a program. Follow your
# instructor's instructions as you experiment with this code.
# ---------------------------------------------------------------------


class MyException(Exception):
    """ This is a custom exception. """
    def __init__(self, description):
        self.description = description

    def __str__(self):
        return self.description


def exercise1_main_function():
    try:
        alpha()
        print("after call to alpha")
    except MyException:
        print("caught MyException")


def alpha():
    print("alpha called")
    beta()
    print("after call to beta")


def beta():
    try:
        print("beta called")
        gamma()
        print("after call to gamma")
    except MyException:
        print("Caught exception in beta")


def gamma():
    print("gamma called")
    delta()
    print("after call to delta")


def delta():
    print("delta called")
    raise MyException("ex")

# ---------------------------------------------------------------------
# Exercise 2:
# The function below demonstrates how to get a file name from a user
# using a tkinter filedialog box. It then opens the file selected by
# the user and writes ""I like Python!\n" to it 10 times. Study the
# exception handling code in the example.
# ---------------------------------------------------------------------


def catching_exceptions_for_file_io():
    application_window = tk.Tk()

    # Build a list of tuples for each file type the file dialog should display
    my_file_types = [('all files', '.*'), ('text files', '.txt')]

    # Ask the user to select a single file name.
    filename = filedialog.asksaveasfilename(parent=application_window,
                                            initialdir=os.getcwd(),
                                            title="Please select a file name for saving:",
                                            filetypes=my_file_types)
    if len(filename) > 0:
        try:
            f = open(filename, "w")
            try:
                for num in range(10):
                    f.write("I like Python!\n")
            except UnicodeEncodeError:
                print("writing failed")
            finally:
                f.close()
        except IOError:
            print("Error: {} does not exist or it can't be opened for output.".format(filename))


def main():
    #exercise1_main_function()
    catching_exceptions_for_file_io()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
