#!/usr/bin/env python

"""
Lab33 - Initial lab on Graphical User Interfaces (GUIs)
"""
# =====================================================================

import tkinter as tk
from tkinter import ttk

# Metadata
__author__ = "Silviu Burz"

'''
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
It is assumed that you have read the readings for lesson 33. If you have
not, please read them before attempting this lab exercise.
'''
# ---------------------------------------------------------------------
# Exercise 1:
# The reading on " Standard Dialog Boxes" provides a description of the
# various pre-defined gui interfaces built into tkinter. In PyCharm,
# open a "Python Console" and then experiment with the various dialog
# boxes. Note that you have to import the required tk module into the
# "Python Console" so that the appropriate objects are defined.
#
# Note: Some of the dialog boxes require an "application window" and
#       if one does not exist, it will automatically create one. For
#       this reason you will often see two windows appear on your screen.
#
#       If the application window is hidden from view, the dialog box
#       will be hidden too. If you execute a dialog box and it does
#       not show up on the screen, it is probably hidden behind other
#       windows. You can click on its icon in the task bar to bring it
#       to the front (which makes it visible.)
#
#       The default application window often gets "hung" because it has
#       no functionality and it does not know how to quit. You sometimes
#       need to kill the "Python Console" by hitting the red "X" in the
#       left tools panel of the "Python Console" and open a new console.
#
# After you have experimented, copy and paste a few of your tests into
# the test_gui_dialog() function below.
# ---------------------------------------------------------------------


def test_gui_dialogs():
    pass

# ---------------------------------------------------------------------
# Exercise 2:
# In each of the functions below, create a gui window that looks like
# the examples shown on the Lab 33 web page on the Moodle server. It is
# recommended that you start with the "Hello World" program in the "GUI
# Introduction" lesson as your starting point. To see examples of the
# various widgets, you can examine the code in the all_user_input_widgets.py
# and all_frame_widgets.py example programs from the readings.
#
# Note: You are only creating the "interface." Your GUI windows will
#       have no functionality. We will add the functionality behind
#       the widgets in future lessons.
# ---------------------------------------------------------------------

class Counter_program():
    def __init__(self):
        self.window = tk.Tk()
        self.window.title("tk Examples")
        self.create_widgets()

        self.radio_variable = tk.StringVar()
        self.combobox_value = tk.StringVar()

    def create_widgets(self):
        # Create some room around all the internal frames
        self.window['padx'] = 5
        self.window['pady'] = 5

        # - - - - - - - - - - - - - - - - - - - - -
        # The Commands frame
        # cmd_frame = ttk.LabelFrame(self.window, text="Commands", padx=5, pady=5, relief=tk.RIDGE)
        cmd_frame = ttk.LabelFrame(self.window, text="Commands", relief=tk.RIDGE)
        cmd_frame.grid(row=1, column=1, sticky=tk.E + tk.W + tk.N + tk.S)

        button_label = ttk.Label(cmd_frame, text="tk.Button")
        button_label.grid(row=1, column=1, sticky=tk.W, pady=3)

        button_label = ttk.Label(cmd_frame, text="ttk.Button")
        button_label.grid(row=2, column=1, sticky=tk.W, pady=3)

        my_button = tk.Button(cmd_frame, text="do something")
        my_button.grid(row=1, column=2)

        my_button = ttk.Button(cmd_frame, text="do something")
        my_button.grid(row=2, column=2)

        switch_frame = ttk.LabelFrame(self.window, text="Choices", relief=tk.RIDGE, padding=6)
        switch_frame.grid(row=2, column=2, padx=6, sticky=tk.E + tk.W + tk.N + tk.S)


        checkbutton1 = ttk.Checkbutton(switch_frame, text="On-off switch 1")
        checkbutton1.grid(row=1, column=2)
        checkbutton2 = ttk.Checkbutton(switch_frame, text="On-off switch 2")
        checkbutton2.grid(row=2, column=2)
        checkbutton3 = ttk.Checkbutton(switch_frame, text="On-off switch 3")
        checkbutton3.grid(row=3, column=2)

        self.radio_variable = tk.StringVar()
        self.radio_variable.set("0")

        radiobutton1 = ttk.Radiobutton(switch_frame, text="Choice One of three",
                                       variable=self.radio_variable, value="0")
        radiobutton2 = ttk.Radiobutton(switch_frame, text="Choice Two of three",
                                       variable=self.radio_variable, value="1")
        radiobutton3 = ttk.Radiobutton(switch_frame, text="Choice Three of three",
                                       variable=self.radio_variable, value="2")
        radiobutton1.grid(row=4, column=2, sticky=tk.W)
        radiobutton2.grid(row=5, column=2, sticky=tk.W)
        radiobutton3.grid(row=6, column=2, sticky=tk.W)

def interface1():
    program = Counter_program()
    program.window.mainloop()
    pass


def interface2():
    pass


def interface3():
    pass


def interface4():
    pass

# ---------------------------------------------------------------------
# If you have extra time, please work on PEX 4.
# ---------------------------------------------------------------------


def main():
    test_gui_dialogs()
    interface1()
    # interface2()
    # interface3()
    # interface4()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
