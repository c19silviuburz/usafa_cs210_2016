#!/usr/bin/env python

"""
Lab21 - Learn how to read text files and process the text from a file.
"""
# =====================================================================

import turtle

# Metadata
__author__ = "Silviu Burz"

# ---------------------------------------------------------------------
# Problems 1
# Please make sure you have downloaded the data files from the Moodle server
# before starting this lab.
#
# There are 3 standard ways to read data from a text file into a Python program:
#   1) Read the entire contents of a file into a single string.
#   2) Read the entire contents of a file into a list, where each element
#      of the list is a single line of the file.
#   3) Read the file one line at a time into a string variable.
# These three ways of reading data are demonstrated in the following three
# functions. Study these functions and compare and contrast what is similar
# and what is different about them. Discuss them with a fellow cadet. Then
# execute them from the main() function and and examine the output they
# produce. Make sure you understand the following concepts:
#   1) A file must be opened before it can be read from.
#   2) A file must be opened for reading, "r", before you can read from it.
#   3) A file should always be closed after its data has been read. This
#      conserves computer resources like RAM memory.
#   4) A file object has methods that can be called, e.g., .read(),
#      .readlines(), .close()
#   5) A file object is a collection! You can think of it as a collection of
#      lines (i.e., a list of strings).
#   6) A file object it iterable, meaning it can be used as the "list" of
#      things to process in a "for loop".
# ----------------------------------------------------------------------


def read_file_into_string():
    infile = open("qbdata.txt", "r")
    data = infile.read()  # Note the .read() method
    infile.close()

    return data
# ----------------------------------------------------------------------


def read_file_into_list():
    infile = open("qbdata.txt", "r")
    data = infile.readlines()  # Note the .readlines() method
    infile.close()

    return data
# ----------------------------------------------------------------------


def read_file_separate_lines():
    a_file = open("qbdata.txt", "r")
    all_lines = ""
    line_number = 1
    for one_line in a_file:  # Note the loop over the file variable
        all_lines += "{:3d} {}".format(line_number, one_line)
        line_number += 1

    a_file.close()

    return all_lines

# ----------------------------------------------------------------------
# Problem 2
# Write a function that reads the qbdata.txt file and prints the following
# statement for each quarterback that has a completion percentage over 60% and
# has attempted more than 3000 passes.
#
#     first last has a completion percentage of nn% after nn attempts!
#
# Hint: Use the string .split() method to break a line of text into individual
#       "words". Then convert the required strings into ints or floats
#       for comparisons.
# ----------------------------------------------------------------------


def problem2():
    infile = open("qbdata.txt", "r")
    data = infile.readlines()  # Note the .readlines() method
    infile.close()
    for item in range(len(data)):
        stats = data[item].split()
        percent = stats[9]
        percent = percent[:-1]
        if ((float(percent) > float(60)) & (int(stats[6]) > int(3000))):
            print(stats[0], stats[1])



# ----------------------------------------------------------------------
# Problem 3
# Write a function that reads the file named top40songs.txt in your Data
# folder of your source code repository and prints each line to the console.
# Number each line from 1 to 40.
#
# Hint: Since the file is not in the same folder as your python program,
#       you must use a "relative file path" to tell python how to find the file.
#
# The data in the file comes from http://www.ukpopcharts.co.uk/this_week's_chart.htm
# ----------------------------------------------------------------------


def problem3():
    infile = open("C:\\Users\\C19Silviu.Burz\\Documents\\SourceCodeRepositories\\USAFA_CS210_2016\\Data\\top40songs.txt", "r")
    data = infile.readlines()  # Note the .readlines() method
    infile.close()
    for item in range(len(data)):
        print(item, ": ", data[item])

# ----------------------------------------------------------------------
# Problem 4
# Write a modified version of your function from problem 4 that only
# prints the songs that did not change their ranking from the previous week.
# The previous week's ranking is the first number on each line in parenthesis.
#
# Hints: Use a string .split() method to break a line into its individual
#        data values. Split on a comma.
#        Use a string .strip() method to remove leading and trailing blanks
#
#  Your expected output should look like this:
#   6: (6),	Ariana Grande featuring Nicki Minaj - 'Side To Side' (Republic),	7,	(6)
#  13: (13),	Shawn Mendes - 'Treat You Better' (EMI),	16,	(23)
#  15: (15),	Lil Wayne, Wiz Khalifa & Imagine Dragons with Logic, Ty Dolla $ign & X Ambassadors - 'Sucker For Pain' (Atlantic),	27,	(30)
#  16: (16),	Jonas Blue featuring JP Cooper - 'Perfect Strangers' (Positiva),	23,	(21)
#  20: (20),	Calvin Harris // Rihanna - 'This Is What You Came For' (Columbia),	20,	(14)
#  21: (21),	Kungs vs Cookin' On 3 Burners - 'This Girl' (3Beat),	13,	(16)
#  22: (22),	Emeli SandÃ© - 'Hurts' (Virgin),	18,	(12)
#  24: (24),	Bob Marley featuring LVNDSCAPE + Bolier - 'Is This Love (Remix)' (Island),	19,	(18)
#  25: (25),	Digital Farm Animals & Cash Cash featuring Nelly - 'Millionaire' (Syco),	31,	(27)
#  30: (30),	SIA - 'Cheap Thrills' (Monkey Puzzle/RCA),	30,	(34)
# ----------------------------------------------------------------------


def problem4():
    infile = open("C:\\Users\\C19Silviu.Burz\\Documents\\SourceCodeRepositories\\USAFA_CS210_2016\\Data\\top40songs.txt", "r")
    data = infile.readlines()  # Note the .readlines() method
    infile.close()
    for item in range(len(data)):
        stats = data[item].split()
        rank = stats[0]
        rank = rank[:-2]
        rank = rank[1:]
        if (rank.isdigit()):
            if ((int(item+1) == int(rank))):
                stats = data[item].split(",")
                print(item, ": ", stats[1])

# ----------------------------------------------------------------------
# Problem 5
# Read a data file called "mystery.txt" that is stored in the Data folder
# of your source code repository. Use the data to draw a turtle graphics
# picture. If a line contains the word DOWN, drop the turtle's pen. If
# a line contains the word UP, raise the turtle's pen. Otherwise, assume
# the line contains 2 integer values and move the turtle to the specified
# (x,y) location. A context for a turtle graphics drawing is included
# below as a starting point.
#
# Hint: Remember that each line of the file ends in a new-line, '\n',
#       character. You need to strip off the new-line character before
#       you use the line. And remember that strings are immutable!
# ----------------------------------------------------------------------


def problem5():
    # Create a turtle and a window to draw into
    a_turtle = turtle.Turtle()
    wn = turtle.Screen()

    # Read and process the "mystery.txt" file
    infile = open("C:\\Users\\C19Silviu.Burz\\Documents\\SourceCodeRepositories\\USAFA_CS210_2016\\Data\\mystery.txt", "r")
    data = infile.readlines()  # Note the .readlines() method
    infile.close()
    for item in range(len(data)):
        if (data[item] == "DOWN"):
            a_turtle.down()
        elif (data[item] == "UP"):
            a_turtle.up()
        else:
            coords = data[item].split(" ")
            a_turtle.goto(coords[0],coords[1])

    wn.exitonclick()
# ----------------------------------------------------------------------

# Note: File processing is really string processing because the contents
#       of a text file is always a string.


def main():
    print("Call your functions to verify their correctness.")

    # # Problem 1: call the functions and print the value they return
    # quarter_backs = read_file_into_string()
    # print("The entire contents of the file as a string:\n", quarter_backs)
    #
    # football_players = read_file_into_list()
    # print("\nThe file as a list of strings, one line per element:\n", football_players)
    #
    # players = read_file_separate_lines()
    # print("\nThe file as a string, built one line at a time\n", players)

    #problem2()
    #problem3()
    #problem4()
    problem5()
# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()