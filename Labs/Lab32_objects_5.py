#!/usr/bin/env python

"""
Lab32 - Learn more about objects and data encapsulation.
"""
# =====================================================================

# Metadata
__author__ = "Silviu Burz"

'''
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

                Data Encapsulation - The Theory

There was no textbook reading for this lesson. Our goal for this lab
time is to learn about data encapsulation inside an object. Encapsulation
protects the data inside of an object. Why??? Two main reasons:

  1) Avoid errors:
     We would like an object's "state" to always be consistent and
     never allow invalid manipulation of an object.

  2) Extend functionality, fix bugs, increase efficiency:
     We would like the "idea an object represents" and the internal
     data representation used to store the object's "state" to be
     separate things. If an object's interface (it's methods) is
     separated from an object's attribute values, it might be
     possible in the future to make an object's code more efficient
     and/or add functionality to the object without breaking any
     existing code that already uses the object.

     One of the important ideas behind object-oriented programming is
     object re-use. That means we need a way to enhance and modify
     object definitions (Classes) over time without breaking the code
     that already uses the objects.

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

          Data Encapsulation - Python Programming Syntax

Fact 1:
Python allows all attributes of an object to be accessed using "dotted
notation." You can't hide object attributes from the rest of the program.

Fact 2:
By convention, if a name starts with a single underscore, this
indicates that the object does not want programmers to use that
data attribute (or method) in their code. It should be considered
"private" and only accessible inside the Class.

Fact 3:
By convention, if a name starts with a double underscore,
the variable or method is accessible only inside the Class. (This
is not the entire truth, but it is close enough for CS210.)

Fact 4:
By convention, if a name starts with a double underscore and ends
with a double underscore, the variable or method should not be
used by a programmer. It should only be called by Python's internal
software.

Summary:

Class Foo:
    def __init__(self):  # never call, it is called automatically by Python
        self.x = 5       # public variable
        self._y = 5      # private variable
        self.__z = 5     # accessible only by this Class

    def task1(self):     # a public method
        pass

    def _task2(self):    # a private method, never call it
        pass

    def __task3(self):   # a private method only accessible inside a Foo object
       pass
'''
# ---------------------------------------------------------------------
# Demo 1:
# Change the definition of the Faction class below to make the denominator
# a "private" attribute. Why? Because a Fraction can't have a denominator
# of zero. (Division by zero is mathematically undefined.) We don't want
# a programmer to create a Fraction object that has a denominator of zero.
# It would make almost every method of the object crash!
# ---------------------------------------------------------------------


def gcd(m, n):
    """
    Find the greatest common divisor of two numbers: m and n
    :param m: an integer
    :param n: an integer
    :return: The greatest common divisor of m and n
    """
    while m % n != 0:
        m, n = n, m % n

    return n


class Fraction:
    """
    Store and manipulate a fraction
    """
    def __init__(self, top, bottom=1):
        self._num = top        # set the numerator value
        self._den = 1     # set the denominator value
        self.den = bottom

    @property
    def den(self):
        return self._den

    @den.setter
    def den(self, new_value):
        if type(new_value) is float:
            new_value = int(new_value+0.5)
        if type(new_value) is str:
            if new_value.isdeciaml():
                new_value = int(new_value)
        if new_value != 0:
            self._den = new_value

    @property
    def num(self):
        return self._den

    @num.setter
    def num(self, new_value):
        if type(new_value) is float:
            new_value = int(new_value+0.5)
        if type(new_value) is str:
            if new_value.isdeciaml():
                new_value = int(new_value)


    def __str__(self):
        self.simplify()
        return str(self.num) + "/" + str(self._den)

    def simplify(self):
        common = gcd(self.num, self._den)

        self.num = self.num // common
        self._den = self._den // common

    def __eq__(self, other_fraction):
        self.simplify()
        other_fraction.simplify()
        return self.num == other_fraction.num and \
               self._den == other_fraction.den

    def __ne__(self, other_fraction):
        self.simplify()
        other_fraction.simplify()
        return self.num != other_fraction.num or \
               self._den != other_fraction.den

    def __lt__(self, other_fraction):
        # A common denominator is required, return which numerator is then less
        return self.num * other_fraction.den < \
               other_fraction.num * self._den

    def __le__(self, other_fraction):
        # A common denominator is required, return which numerator is then less than or equal to
        return self.num * other_fraction.den <= \
               other_fraction.num * self._den

    def __gt__(self, other_fraction):
        # A common denominator is required, return which numerator is then greater
        return self.num * other_fraction.den > \
               other_fraction.num * self._den

    def __ge__(self, other_fraction):
        # A common denominator is required, return which numerator is then greater than or equal to
        return self.num * other_fraction.den >= \
               other_fraction.num * self._den

    def __add__(self, other):
        if type(other) == int:
            other = Fraction(other * self._den, self._den)

        if type(other) == Fraction:
            newnum = self.num * other._den + self._den * other.num
            newden = self._den * other._den

            result = Fraction(newnum, newden)
            result.simplify()
        else:
            result = Fraction(0,1)
            print("Error in Fraction addition. The right operand must be a Fraction or a integer.")

        return result

    def __sub__(self, otherfraction):
        newnum = self.num * otherfraction.den - self._den * otherfraction.num
        newden = self._den * otherfraction.den

        result = Fraction(newnum, newden)
        result.simplify()

        return result

    def __mul__(self, otherfraction):
        newnum = self.num * otherfraction.num
        newden = self._den * otherfraction.den

        result = Fraction(newnum, newden)
        result.simplify()

        return result


def demo1():
    f1 = Fraction(1, 2)
    print("f1 = ", f1)

    f1._den = 4
    print("f1 = ", f1)

    print(f1._den)

    # f1.num = 5
    # print("f1 = ", f1)
    #
    # f1.num = 7.8
    # print("f1 = ", f1)
    #
    # f1.num = "3"
    # print("f1 = ", f1)


# ---------------------------------------------------------------------
'''
             Summary - Protecting Object Attributes

Principle 1:
------------
If you don't need to protect an attribute from invalid "state" values:
    * DO NOT implement a "getter" and a "setter."
    * Use normal "dotted notation" to get and set the object's attribute
      value.

Principle 2:
------------
If values for an attribute need to be checked for correct "state",
implement a "getter" and a "setter" and perform checks on every new
value before changing an attribute. The syntax conventions are:
    a) Use one underscore before the attribute name.
    b) Create the attribute in __init__() and give it a default value.
    c) Create a "getter" with the same name as the attribute,
         but without the underscore. Put the "decorator" @property
         above the function.
    d) Create a "setter" with the same name as the attribute,
         but without the underscore. Put the "decorator" @name.setter
         above the function.
    e) Assign the attribute its real initial value in the __init__()
         function using an assignment statement without the underscore.
         This forces the "setter" to be called which will perform the
         appropriate verification checks on the assignment value
         before it is actually assigned to the attribute.
'''
# ---------------------------------------------------------------------

# ---------------------------------------------------------------------
# Please spend the remainder of your time working on PEX 4.
# ---------------------------------------------------------------------


def main():
    demo1()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
