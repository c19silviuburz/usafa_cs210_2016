#!/usr/bin/env python

"""
Lab04 - Practice debugging a program to find ParseErrors, TypeErrors,
        NameErrors, and ValueErrors.

Documentation Statement: (none)

"""
# =====================================================================

# Modules used for this program; built-in modules first, followed by
# third-party modules, followed by any changes to the path and your own modules
import math

# Metadata
__author__ = "Silviu Burz"
__email__ = "C19Silviu.Burz@usafa.edu"
__date__ = "Aug 18, 2016"

# Constants
N = 3

# Get three numbers from the user
n1 = float(input("First number: ")) #extra space, need float for all of them otherwise its a string
n2 = float(input("Second number: "))
n3 = float(input("Third number: ")) #third not fourth

# Calculate properties of the three numbers
sum = n1 + n2 + n3 #missing space before n2
average = sum / N #dont want int division here, replace with N
variance = (average - n1) ** 2 + (average - n2) ** 2 + (average - n3) ** 2 #need expon instead of multi
standard_deviation = math.sqrt(abs(variance)) #need abs for all the sqrts to prevent sqrt of neg number
population_standard_deviation = math.sqrt(abs(variance / N)) #missplled vaiance
sample_standard_deviation = math.sqrt(abs(variance / N-1))
minimum = min(n1, n2, n3) #n3 instead of n4
maximum = max(n1, n2, n3)

# Display the results
print("The inputs were: ", n1, n2, n3)
print("Their average is", average)
print("Their standard deviation is", standard_deviation)
print("Their population standard deviation is", population_standard_deviation)
print("Their sample standard deviation is", sample_standard_deviation)
print("Their minimum value is", minimum)
print("Their maximum value is", maximum)

# Make a list of the errors you found.
# Include the line number, the type of error, and how you fixed it.

#25 semantic error, include spaces properly
#25-28 semantic/runtime error, include float for all of the inputs
#27 semantic error, fourth instead of third
#30 syntax error, missing whitespace
#31 semantic error, don't want int division for an avg, also replace 3 with the variable N
#32 semantic error, need exponent instead of *
#33 runtime error, need abs before all the sqrts to prevent a negative under the sqrt
#34 syntax error, mispelled variance variable
#36 semantic/syntax error, replace n4 with the proper n3



