#!/usr/bin/env python

"""
Lab24 - Learn how to use recursion in problem solving.
"""
# =====================================================================

import random

# Metadata
__author__ = "First Last"

# ----------------------------------------------------------------------
# Problem 1
# The text book describes a recursive function that sums a list of numbers.
# Below is a series of functions that all produce the same "answer" to this
# problem. Study each version and discuss them with a fellow cadet. Which
# version is the best solution? Which version(s) will not crash if sent an
# empty list?
#
# After you have studied the functions with another cadet, write answers
# to these questions.
'''
1) What is the "base case" for these recursive functions?

2) How is the parameter to the recursive call changed to make it closer
   to the base case?


'''
# ----------------------------------------------------------------------


def sum_list1(my_list):
    """
    Sum the numbers in a list - non-recursive version
    :param my_list: a list of numbers
    :return: the sum of the numbers in the list
    """
    total = 0
    for item in my_list:
        total += item

    return total


def sum_list2(my_list):
    if len(my_list) == 1:  # Base case
        return my_list[0]
    else:
        return my_list[0] + sum_list2(my_list[1:])


def sum_list3(my_list):
    if len(my_list) == 1:  # Base case
        total = my_list[0]
    else:
        total = my_list[0] + sum_list3(my_list[1:])

    return total


def sum_list4(my_list):
    if len(my_list) <= 0:  # Base case
        return 0

    return my_list[0] + sum_list4(my_list[1:])


def sum_list5(my_list):
    if len(my_list) <= 0:  # Base case
        return 0

    return my_list[-1] + sum_list5(my_list[:-1])


def sum_list6(my_list):
    if len(my_list) <= 0:  # Base case
        return 0

    middle = len(my_list) // 2

    return sum_list6(my_list[:middle]) + my_list[middle] + sum_list6(my_list[middle+1:])


# ---------------------------------------------------------------------
# Problem 2
# Below is a non-recursive function that calculates n! (factorial).
# The factorial of n is equal to 1*2*3*4*5*6* ... *(n-1)*n
# Write at least two equivalent recursive functions that do the same
# thing, similar to variations in problem 1.
# (You can't use any built-in Python methods to solve this problem.)
# Use the example recursive functions from problem 1 as your guide.
#
# Note: You can assume that your function will always receive a positive
# integer as an input argument.
# ----------------------------------------------------------------------


def factorial(n):
    """
    Calculate n!, a non-recursive version
    :param n: The size of the factorial
    :return: n!
    """
    answer = 1
    for factor in range(2, n+1):
        answer = answer * factor

    return answer


def factorial1(n):
    pass


def factorial2(n):
    pass


# ---------------------------------------------------------------------
# Problem 3
# Below is a non-recursive function that calculates a Fibonacci number.
# The Fibonacci sequence is defined as the series 1, 1, 2, 3, 5, 8, 13, ...
# where each successive number is the sum of the previous two numbers.
# The nth term in the sequence can be defined by the relationship
# fib(n) = fib(n-1) + fib(n-2).
# Write at least two equivalent recursive functions that do the same
# thing, similar to variations in problem 1.
# (You can't use any built-in Python methods to solve this problem.)
#
# Assume that Fib(1) is equal to 1 and Fib(2) is equal to 1.
#
# Note: You can assume that your function will always receive a positive
# integer as an input argument.
# ----------------------------------------------------------------------


def fibonacci(n):
    """
    Calculate the nth term of a Fibonacci sequence
    :param n: The desired term
    :return: fib(n)
    """
    term0 = 1
    term1 = 1
    next_term = 1
    for term in range(3, n+1):
        next_term = term0 + term1
        term0 = term1
        term1 = next_term

    return next_term


def fibonacci1(n):
    pass


def fibonacci2(n):
    pass

# ---------------------------------------------------------------------
# Problem 4
# Below is a non-recursive function that returns the largest value in a list.
# Write a recursive functions that does the same thing.
# (You can't use any built-in Python methods to solve this problem.)
#
# Note: The non-recursive function will crash with an error if you
# send it an empty list. You can assume that this function will only
# be called with an input list containing one or more elements. You can
# assume the same thing for your recursive functions.
# ----------------------------------------------------------------------


def maximum(a_list):
    """
    Search a list and return its maximum value
    :param a_list: The list of values
    :return: The maximum value from the list
    """
    the_max = a_list[0]
    for item in a_list[1:]:
        if item > the_max:
            the_max = item

    return the_max


def maximum1(a_list):
    pass

# ---------------------------------------------------------------------
# Challenge Problem
# Below is a non-recursive function that searches for a value in a
# list and returns the location of its first occurrence. If the value is
# not found, it returns -1. Write a recursive version of this function.
# ----------------------------------------------------------------------


def find(a_list, a_value):
    """
    Search for a_value in the a_list and return its position if found.
    :param a_list: The list to search
    :param a_value: The value to search for
    :return: If a_value is found in a_list, return its first occurrence position,
             otherwise return -1.
    """
    for index in range(len(a_list)):
        if a_list[index] == a_value:
            return index

    # The value was not found, so return an invalid list position.
    return -1


def find1(a_list, a_value):
    pass

# ----------------------------------------------------------------------


def main():
    print("Call your functions to verify their correctness.")

    # ------------------------------------------------------------------
    # Problem 1 tests:
    a = [1, 3, 5, 7, 9, 11]
    print("from sum_list1()", sum_list1(a))
    print("from sum_list2()", sum_list2(a))
    print("from sum_list3()", sum_list3(a))
    print("from sum_list4()", sum_list4(a))
    print("from sum_list5()", sum_list5(a))
    print("from sum_list6()", sum_list6(a))

    # ------------------------------------------------------------------
    # Problem 2 tests:
    # n = 10
    # print(factorial(n))
    # print(factorial1(n))
    # print(factorial2(n))

    # ------------------------------------------------------------------
    # Problem 3 tests:
    # print("Print the first 10 terms of a Fibonacci sequence")
    # for n in range(1,11):
    #     print(fibonacci(n), fibonacci1(n), fibonacci2(n))

    # ------------------------------------------------------------------
    # Problem 4 tests:
    # for test in range(10):
    #     a_list = [random.randint(0,100) for j in range(10)]
    #     print("List to find max: ", a_list)
    #     print("maximum value is ", maximum(a_list), maximum1(a_list))

    # ------------------------------------------------------------------
    # Challenge Problem tests:
    # for test in range(10):
    #     a_list = [random.randint(0,10) for j in range(20)]
    #     value = random.randint(0,10)
    #     print("Find {} in the list {}: ".format(value, a_list))
    #     print("Position is ", find(a_list, value), find1(a_list, value))

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
