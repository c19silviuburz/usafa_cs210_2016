#!/usr/bin/env python

"""
Lab17 - Learn the basics of lists and list manipulation.
"""
# =====================================================================

import random

# Metadata
__author__ = "Silviu Burz"

# ---------------------------------------------------------------------
# Problem 1
# Please do the following:
#   1) Study the following code.
#   2) Write down what you think the output will be.
#   3) Discuss your expected output with a fellow cadet and resolve any
#      discrepancies.
#   4) Now execute both functions and compare you answers. If you got any of
#      the outputs wrong, make sure you know why.
#
# Note: The exact value of an object's ID is not important and you can
#       never know its value before a program executes (because Python
#       assigns ID's to objects as they are created at run-time). But
#       your can compare ID's to know if two variables reference the same
#       object. In your output, compare the object ID's.
# ----------------------------------------------------------------------


def problem1a():
    a = [1, 2, 3]
    b = a[:]
    b[0] = 5

    print("Problem 1a output:")
    print("id: {} list: {}".format(id(a), a))
    print("id: {} list: {}".format(id(b), b))


def problem1b():
    a = [1, 2, 3]
    b = a
    b[0] = 5

    print("Problem 1b output:")
    print("id: {} list: {}".format(id(a), a))
    print("id: {} list: {}".format(id(b), b))

# ----------------------------------------------------------------------
# Problem 2
# Define a function called problem2 that creates a list that contains
# the following values:  57, 36, "sam", 45.2, and False (in the order
# specified). Then print the list and perform the following modifications
# on the list. Print the modified list after each change.
#   a) Append “apple” and 76 to the end of the list. (Use list concatenation.)
#   b) Change the value of the 2nd element (i.e., the 36) to "mary"
#   c) Insert the value 12 at position 4 (between the values 45.2 and False).
#   d) Insert the value 99 at the start of the list.
#   e) Remove elements at positions 3, 4 and 5 totally from the list.
#   f) Find the index of the element “apple”. (Hint: use the index() method.)
# ----------------------------------------------------------------------

def problem2():
    alist = [57,36,"sam",45.2,False]
    print(alist)
    alist += ["apple", 57]
    print(alist)
    alist[1] = "mary"
    print(alist)
    alist = alist[:4] + [12] + alist[4:]
    print(alist)
    alist = [99] + alist
    print(alist)
    alist = alist[:3] + alist[6:]
    print(alist)
    print(alist.index("apple"))
# ----------------------------------------------------------------------
# Problem 3
# Define a function called create_random_values() that receives a single
# integer value that represents the size of a list. The function must
# create a list of the appropriate size and fill it with random numbers
# in the range [0,10]. The function must return the list that was created.

# Hint: There are two ways to do this:
#    1) Create the list and then assign each element a random value, or
#    2) Generate random values and add them to a list.
# Try to implement two different functions according to these two methods.
# ----------------------------------------------------------------------


def create_random_values(size):
    alist = []
    for i in range(size):
        alist += [random.randint(0,10)]
    print(alist)

# ----------------------------------------------------------------------
# Problem 4
# Define a function called count_occurrences() that receives a list and a
# value and returns the number of times the value occurs in the list.
# ----------------------------------------------------------------------

def count_occurrences(alist, value):
    counter = 0
    for i in range(len(alist)):
        if alist[i] == value:
            counter += 1
    return counter

# ----------------------------------------------------------------------
# Problem 5
# Define a function called rotate() that receives a list and
# returns a new list where the first element in the original list is now
# at the end of the new list. Return the new list. For example,
# rotate([1,2,3,4]) should return the list [2,3,4,1]
# ----------------------------------------------------------------------

def rotate(alist):
    return alist[1:] + [alist[0]]


# ----------------------------------------------------------------------
# Problem 6
# Define a function called randomize() that receives a list and
# rearranges the elements in the list using this algorithm:
#      For 100 times:
#          let j be a random integer between 0 and len(list)-1
#          let k be a random integer between 0 and len(list)-1
#          swap the values of the jth and kth elements
#
# PLEASE NOTE: The is similar to the shuffle algorithm for PEX 2, but
#              PEX 2 requires that you to use a different algorithm.
#              Please make sure you implement your PEX 2 code using the
#              "Fisher–Yates shuffle."
# ----------------------------------------------------------------------

def randomize(alist):
    for i in range(100):
        j = random.randint(0, len(alist)-1)
        k = random.randint(0, len(alist)-1)
        valuek = alist[k]
        valuej = alist[j]
        alist[j] = valuek
        alist[k] = valuej
    return alist

# ----------------------------------------------------------------------


def main():
    print("Call your functions to verify their correctness.")
    # problem1a()
    # problem1b()
    #problem2()

    #create_random_values(6)
    alist = ["this", "is", "a", "longer", "test"]
    #print(count_occurrences(alist, "test"))

    #print(rotate(alist))
    print(randomize(alist))
    # for j in range(10, 20):
    #     print("Size:", j, "  List:", create_random_values(j))
    #     print("Size:", j, "  List:", create_random_values2(j))
    #
    # my_list = create_random_values(10)
    # for j in range(11):
    #     print(my_list, " ", j, "occurs", count_occurrences(my_list, j))
    #
    # my_list = create_random_values(10)
    # print(my_list, "rotated is", rotate(my_list))

    # my_list = create_random_values(10)
    # print(my_list)
    # randomize(my_list)
    # print(my_list)

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
