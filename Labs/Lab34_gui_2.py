#!/usr/bin/env python

"""
Lab34 - Processing events in Graphical User Interface (GUI) programs.
"""
# =====================================================================

import tkinter as tk
from tkinter import ttk

# Metadata
__author__ = "Silviu Burz"

'''
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
It is assumed that you have read the readings for lesson 34. If you have
not, please read them before attempting this lab exercise.

You are provided with the GUI "interface" for these exercises because we
want you to concentrate on the event handling aspects of these lab
exercises. However, please examine the interface code that is provided
and if there is anything you don't understand, please discuss your
questions with your instructor.
'''
# ---------------------------------------------------------------------
# Exercise 1:
# Below is the example program from the readings that increments a
# "counter" each time a user hits a button. Modify the program
# to add a third button with the text "Subtract 1 from counter" and
# add a callback function to decrement the counter (which is actually
# a Label widget that contains a text string.)
# ---------------------------------------------------------------------


class CounterProgram:

    def __init__(self):
        self.window = tk.Tk()
        self.my_counter = None  # All attributes should be created in init
        self.create_widgets()

    def create_widgets(self):
        self.my_counter = ttk.Label(self.window, text="0")
        self.my_counter.grid(row=0, column=0)

        increment_button = ttk.Button(self.window, text="Add 1 to counter")
        increment_button.grid(row=1, column=0)
        increment_button['command'] = self.increment_counter

        decrement_button = ttk.Button(self.window, text="Subtract one from counter")
        decrement_button.grid(row=2, column=0)
        decrement_button['command'] = self.decrement_counter

        quit_button = ttk.Button(self.window, text="Quit")
        quit_button.grid(row=3, column=0)
        quit_button['command'] = self.window.destroy

    def increment_counter(self):
        self.my_counter['text'] = str(int(self.my_counter['text']) + 1)

    def decrement_counter(self):
        self.my_counter['text'] = str(int(self.my_counter['text']) - 1)

def exercise1():
    # Create the entire GUI program
    program = CounterProgram()

    # Start the GUI event loop
    program.window.mainloop()

# ---------------------------------------------------------------------
# Exercise 2:
# The GrowWindow class below defines a GUI program that contains 2
# buttons. Implement a callback function for the grow_button that will
# increase the size of the window each time it is clicked.
#
# Notes: You can't just modify the width and height of the window. The
#        window's size is controlled by the layout manager that sized
#        and positioned the buttons. You have to change the attributes
#        of the grid to make the cell that contains the grow_button
#        bigger. Try modifying the padx and pady attributes of the
#        grow_button's grid arguments.
#
#        After you get the program to work, try modifying the ipadx
#        and ipady attributes. This should help you better understand
#        the difference between padx and ipadx.
# ---------------------------------------------------------------------


class GrowWindow:

    def __init__(self):
        self.window = tk.Tk()
        self.grow_button = None
        self.create_widgets()
        self.window['padx'] = 5
        self.window['pady'] = 5

    def create_widgets(self):
        self.grow_button = ttk.Button(self.window, text="Make the window bigger")
        self.grow_button.grid(row=1, column=0)
        self.grow_button['command'] = self.grow_button

        quit_button = ttk.Button(self.window, text="Quit")
        quit_button.grid(row=2, column=0)
        quit_button['command'] = self.window.destroy


    def grow_button(self):
        self.window['padx'] += 1
        self.window['pady'] += 1

def exercise2():
    # Create the entire GUI program
    program = GrowWindow()

    # Start the GUI event loop
    program.window.mainloop()

# ---------------------------------------------------------------------
# Exercise 3:
# Below is an interface for creating a color editor. The idea is that
# when a user drags one of the scale widgets, the color of the frame on
# the right side of the window displays the color defined by the three
# values that come from the sliders. Implement a callback function (also
# called an event handler function) for each of the scale widgets. The
# callback functions should create a color from the current state of
# the three scale widgets and then update the color of the "color_frame".
#
# Notes: The callback function for a tk.Scale widget receives one
#        parameter, which is the new value of the scaler. This value
#        is passed to the callback function as a string!
#
#        Tk defines a set of colors by name, such as 'red' and 'cyan',
#        but to specify any arbitrary RGB color value you must use
#        the standard web notation, which is a string that starts with a
#        '#' and contains 2 hexadecimal digits for each color component.
#        For example, "#FF0000" represents red, "#00FF00" represents
#        green, and "#0000FF" represents blue. You can easily create this
#        color notation using this string:
#          "#{:02X}{:02X}{:02X}".format(self.red, self.green, self.blue)
#        The format specifier {:02X} means that you print a value
#        using 2 spaces in hexadecimal (base 16) notation. If the value
#        can be represented with a single digit, the leading 0 (before
#        the 2) makes any leading spaces filled with 0's.
# ---------------------------------------------------------------------


class ColorEditor:

    def __init__(self):
        self.window = tk.Tk()
        self.window.wm_title("Color Editor")
        self.red = 0
        self.green = 0
        self.blue = 0
        self.color_frame = None
        self.create_widgets()

    def create_widgets(self):
        slider_frame = tk.Frame(self.window)
        slider_frame.grid(row=1, column=1)

        red_label = tk.Label(slider_frame, text="Red")
        red_label.grid(row=1, column=1, sticky=tk.S)

        red_slider = tk.Scale(slider_frame, from_=0, to=255, orient=tk.HORIZONTAL,
                              width=8, length=200)
        red_slider.grid(row=1, column=2)
        red_slider['command'] = self.red_slider()

        green_label = tk.Label(slider_frame, text="Green")
        green_label.grid(row=2, column=1, sticky=tk.S)

        green_slider = tk.Scale(slider_frame, from_=0, to=255, orient=tk.HORIZONTAL,
                                width=8, length=200)
        green_slider.grid(row=2, column=2)
        green_slider['command'] = self.green_slider()

        blue_label = tk.Label(slider_frame, text="Blue")
        blue_label.grid(row=3, column=1, sticky=tk.S)

        blue_slider = tk.Scale(slider_frame, from_=0, to=255, orient=tk.HORIZONTAL,
                               width=8, length=200)
        blue_slider.grid(row=3, column=2)
        blue_slider['command'] = self.blue_slider()

        self.color_frame = tk.Frame(self.window, width=100, height=100)
        self.color_frame.grid(row=1, column=2, padx=4, pady=4)

    def red_slider(self):
        pass

    def green_slider(self):
        pass

    def blue_slider(self):
        pass



def exercise3():
    # Create the entire GUI program
    program = ColorEditor()

    # Start the GUI event loop
    program.window.mainloop()

# ---------------------------------------------------------------------
# Exercise 4:
# To experiment with bind events, try to modify your program in exercise
# 3 to do the following:
#
# Make the shape of the cursor change whenever it is over the color_frame.
# Add a callback for "<Enter>" events on the color_frame. Also add a callback
# for a "<Leave>" event on the color_frame. Change the 'cursor'
# attribute of the color_frame appropriately in each callback. The possible
# values for the 'cursor' attribute can be found at
#    http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/cursors.html
# Remember that a callback defined by a bind command receives one argument,
# which is an event_object.
#
# Bind a right button click on the color_frame to a callback that resets
# all of the scale widgets to zeros (which should make the color black.)
# You can change the value of a scale widget by calling its .set(new_value)
# method. (You will have to make the scale widgets attributes of the
# ColorEditor class so that you can access them in the callback function.
#
# Think up some other event that you would like to add to your program.
# Try to add it.
# If you have problems, get help from your instructor.
# ---------------------------------------------------------------------


# ---------------------------------------------------------------------
# If you have extra time, please work on PEX 4.
# ---------------------------------------------------------------------


def main():
    #exercise1()
    #exercise2()
    exercise3()

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
