#!/usr/bin/env python

"""
GR 2 - The second Graded Review in CS210, Fall 2016
"""
# =====================================================================

# # Metadata
__author__ = "Silviu Burz"
__date__ = "1 Nov 2016"

# ----------------------------------------------------------------------
# Problem 1 (25 pts)
# ----------------------------------------------------------------------

def get_data():
    datafile = open("CS210cadets.txt", "r")
    data = datafile.readlines()
    data = data[1:]
    datafile.close()
    for i in range(len(data)):
        data[i] = data[i].split(",")
        for j in range(len(data[i])):
            data[i][j] = data[i][j].strip()
    return data

# ----------------------------------------------------------------------
# Problem 2 (25 pts) (+5 pts for a docstring)
# ----------------------------------------------------------------------

def save_emails(list):
    """
    This function receives a list of cadet info and takes the emails (4th element) and
    writes them to a new file (just_emails.txt), all aligned by the "@" symbol
    :param list: the list of cadet info, assuming 4th element is emails
    :return: None
    """
    newfile = open("just_emails.txt", "w")
    for i in range(len(list)):
        length = list[i][4].split("@")
        length = len(length[0])
        newfile.write(" "*(24-length))
        newfile.write(list[i][4])
        newfile.write("\n")

# ----------------------------------------------------------------------
# Problem 3 (25 pts)
# ----------------------------------------------------------------------

def print_class_year(list, yearlist):
    for i in range(len(yearlist)):
        print(str(list[yearlist[i]][1] +", "+ list[yearlist[i]][0]))

def by_class_year(list):
    firsties = []
    twodigs = []
    threedigs =[]
    masterlist = [firsties, twodigs, threedigs]
    titles = ["Firsties", "2-degrees", "3-degrees"]
    for i in range(len(list)):
        if (int(list[i][4][1:3]) == 19):
            threedigs.append(i)
        elif (int(list[i][4][1:3]) == 18):
            twodigs.append(i)
        elif (int(list[i][4][1:3]) == 17):
            firsties.append(i)

    for i in range(3):
        print(titles[i])
        print_class_year(list, masterlist[i])
        print("\n")

# ----------------------------------------------------------------------
# Problem 4 (25 pts)
# ----------------------------------------------------------------------

def print_students(list, students):
    for i in range(len(students)):
        print(str(list[students[i]][1] + ", " + list[students[i]][0]))

def organize_by_section(list):
    m1a = []
    m3a = []
    t1a = []
    t3a = []
    t6a = []
    sectionslist = ["M1A", "M3A", "T1A", "T3A", "T6A"]
    sections = {'M1A': m1a, 'M3A': m3a, 'T1A': t1a, 'T3A': t3a, 'T6A': t6a}

    for i in range(len(list)):
        if (str(list[i][3]) == "M1A"):
            m1a.append(i)
        elif (str(list[i][3]) == "M3A"):
            m3a.append(i)
        elif (str(list[i][3]) == "T1A"):
            t1a.append(i)
        elif (str(list[i][3]) == "T3A"):
            t3a.append(i)
        elif (str(list[i][3]) == "T6A"):
            t6a.append(i)

    for i in range(len(sections)):
        print(sectionslist[i])
        print_students(list, sections[sectionslist[i]])
        print("\n")

# ----------------------------------------------------------------------
# Problem 5 (20 pts) (recursive function)
#  ----------------------------------------------------------------------

def recursive_function(a, b):
    if (b == 0): # odd initial case where b might be zero, otherwise it enters an endless loop
        return 0
    if (b != 1): # if this is set to 0 instead of 1, it always returns 1 more than it should
        a += recursive_function(a, b - 1)
        return a
    else:
        return a

# ----------------------------------------------------------------------


def main():
    print(recursive_function(5,3))


# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()
