#!/usr/bin/env python

"""
Final Exam - version 1, CS210, Fall 2016
"""
# =====================================================================

# # Metadata
__author__ = "Silviu Burz"
__date__ = "Dec 13, 2016"

import random

# ======================================================================
# Part 1: Software development                            _____ / 10 pts
# ======================================================================
def software_development_answers():
    """
    Type the correct answer for each software development question
    into the appropriate string.
    """
    question_1 = "B"
    question_2 = "B"
    question_3 = "A"
    question_4 = "C"
    question_5 = "D"

    return question_1, question_2, question_3, question_4, question_5


# ======================================================================
# Problem 1                                               _____ / 30 pts
# ======================================================================

def nautical_miles_to_feet(nmi):
    if ((nmi >= 117) & (395 >= nmi)):
        return float(nmi * 6067.12)
    else:
        return 0

# ======================================================================
# Problem 2                                              ______ / 30 pts
# ======================================================================

def max_hours_drive_time_permitted(dutyday, drivers, departurehr):
    hours = 0
    if ((type(dutyday) != bool) | (drivers <= 0) | (departurehr < 0) | (departurehr > 23)):
        pass
    elif (dutyday == True):
        if (drivers == 1):
            hours = 8
        else:
            hours = 12
        hours = min(hours, (24-departurehr))
    else:
        if (drivers == 1):
            hours = 8
        elif (drivers == 2):
            hours = 12
        else:
            hours = 16
    return hours

# ======================================================================
# Problem 3                                              ______ / 30 pts
# ======================================================================

def sparse_percentage(matrix):
    zeros = 0
    size = (len(matrix) * len(matrix[0]))
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if (matrix[i][j] == 0):
                zeros += 1
    return str(int(round(100*zeros/size,0))) + "%"


# ======================================================================
# Problem 4                                              ______ / 40 pts
# ======================================================================


def get_artist(stats):
    current = ""
    artist = ""
    i = 1
    while(current != "-"):
        artist += current
        current = stats[i]
        artist += " "
        i += 1
    return artist[:-1] + "."

def top_40_songs():
    file = open("top40songs.txt", "r")
    data = file.readlines()
    file.close()
    list = []
    for i in range(len(data)):
        stats = data[i].split()
        rank = stats[0]
        rank = rank[:-2]
        rank = rank[1:]
        try:
            rank = int(rank)
        except ValueError:
            rank = int(99)

        artist = get_artist(stats)
        # if (len(list) == 0):
        #     list.append(str(rank) + str(artist))
        # for i in range(len(list)):
        #     templist = list[i].split()
        #     if (int(templist[0]) <= rank):
        #         list.insert(i, str(rank) + str(artist))
        #         break
        templist = [rank, artist]
        list.append(templist)
    list = sorted(list)
    newlist = []
    for i in range(len(list)):
        newlist.append("")
        newlist[i] += "| "
        if (list[i][0] < 10):
            newlist[i] += " "
        newlist[i] += str(list[i][0]) + " | " + list[i][1]
        print(newlist[i])

# ======================================================================
# Problem 5                                              ______ / 40 pts
# ======================================================================

def create_maze(filename, rows, columns):
    file = open(filename, "w")
    file.write("_"*(columns+2))
    for i in range(rows):
        file.write("\n")
        file.write("|")


        string = ""
        for j in range(columns):
            rand = random.random()
            rand = int(rand*10)
            if (rand == 0):
                string += "_"
            elif (rand == 1):
                string += "|"
            else:
                string += " "
        file.write(string)


        file.write("|")
    file.write("\n")
    file.write("|")
    file.write("_" * (columns))
    file.write("|")

# ======================================================================
# Problem 6                                              ______ / 40 pts
# ======================================================================
# Docstring for problem 6                                ______ / 10 pts
# ======================================================================

class PieChart:
    """
    The PieChart class handles the data associated with a pie chart.
    It accepts and manipulates categories and values at a one to one ratio
    in the same manner that a pie chart would need them.

    """
    def __init__(self, categories = [], values = []):
        """
        The init function accepts two lists that are assigned as the categories and values of the chart.
        The values list is appropriately appended or truncated if it does not match the size of the
        categories list.

        :param categories: a list of strings representing categories of the pie chart
        :param values:  a list of values corresponding 1 to 1 with the categories of the pie chart
        """
        if (len(categories) > len (values)):
            for i in range(len(categories) - len(values)):
                values.append(0)
        elif (len(categories) < len (values)):
            for i in range(len(values) - len(categories)):
                values.pop()
        self.__categories = categories
        self.__values = values

    def add(self, new_category, new_value):
        """
        Adds a new category and its corresponding new value to the pie chart.

        :param new_category: new category to be added to the chart
        :param new_value: new value to be added to correspond
        :return: None
        """
        self.__categories.append(new_category)
        self.__values.append((new_value))

    def remove(self, category_name):
        """
        Removes the inputted category and its corresponding value from the pie chart.
        Does nothing if the category name does not exist in the chart.

        :param category_name: literal name of the category in the chart to be removed
        :return: None
        """
        try:
            i =self.__categories.index(category_name)
            self.__categories.pop(i)
            self.__values.pop(i)
        except ValueError:
            pass

    def __str__(self):
        """
        Creates, formats, and returns a string that represents the categories and corresponding values of the pie chart

        :return: string with that information
        """
        string = ""
        for i in range(len(self.__categories)):
            string += self.__categories[i] + ": " + str(self.__values[i]) + "%\n"
        return string

    def draw(self):
        """
        Produces a rendering of the pie chart

        :return: None
        """
        pass

def create_pie_charts():
    charts = []
    charts.append(PieChart())
    charts[0].add("Added category", 12)
    charts.append(PieChart(["test1"], [1]))
    charts[1].add("Another added category", 64)
    charts.append(PieChart(["Apple", "Orange", "Watermellon", "Grape"], [4, 43, 12, 54]))
    charts[2].remove("Watermellon")
    for i in range(len(charts)):
        print(charts[i])
        charts[i].draw()

# ======================================================================
# Problem 7                                              ______ / 20 pts
# ======================================================================

def remove_repeats(dna):
    string = ""

    if (len(dna) > 1):
        if (dna[0] == dna[1]):
            dna = dna[1:]
        else:
            string = dna[0]
            dna = dna[1:]
        string += remove_repeats(dna)
    else:
        return dna[0]
    return string

# ----------------------------------------------------------------------
def main():
    print("Function tests; call functions as needed to validate their correctness.")

    # Problem 1 tests
    # print(nautical_miles_to_feet(1))
    # print(nautical_miles_to_feet(200))
    # print(nautical_miles_to_feet(6000))

    # Problem 2 tests
    # print(max_hours_drive_time_permitted(True, 2, 12))
    # print(max_hours_drive_time_permitted(False, 3, 6))
    # print(max_hours_drive_time_permitted(True, 1, 22))
    # print(max_hours_drive_time_permitted(True, -3, 22))
    # print(max_hours_drive_time_permitted(4, -3, 22))
    # print(max_hours_drive_time_permitted(True, 2, 24))
    # print(max_hours_drive_time_permitted(True, 2, -1))
    # Problem 3 tests
    m1 = [[0, 0, 0],
          [0, 0, 0],
          [0, 0, 0]]

    m2 = [[4, 0, 1, 0, 2],
          [0, 2, 0, 0, 0],
          [0, 0, 5, 0, 0]]

    m3 = [[0, 1],
          [5, 0],
          [6, 0],
          [0, 3],
          [8, 0]]

    m4 = [[1, 1],
          [5, 0],
          [6, 3],
          [3, 3],
          [8, 0]]

    # print(sparse_percentage(m1))
    # print(sparse_percentage(m2))
    # print(sparse_percentage(m3))
    # print(sparse_percentage(m4))



    # Problem 4 tests

    # top_40_songs()

    # Problem 5 tests

    # create_maze("test.txt", 50, 100)

    # Problem 6 tests

    # create_pie_charts()

    # Problem 7 tests

    print(remove_repeats("ATCG"))
    print(remove_repeats("AATTTTTCCGG"))
    print(remove_repeats("ATCGG"))
    print(remove_repeats("ATCGATTGAGCTCTAGG"))

# ----------------------------------------------------------------------

# If this file is executed, call the main() function
if __name__ == "__main__":
    main()

